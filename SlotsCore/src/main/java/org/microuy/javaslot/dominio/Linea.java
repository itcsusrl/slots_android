package org.microuy.javaslot.dominio;

public class Linea {

	private int id;
	private int[][] forma;

	public Linea(int id, int[][] forma) {
		this.id = id;
		this.forma = forma;
	}
	
	public int[][] getForma() {
		return forma;
	}

	public void setForma(int[][] forma) {
		this.forma = forma;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}
}