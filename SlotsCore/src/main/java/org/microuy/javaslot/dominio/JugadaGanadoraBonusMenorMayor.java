package org.microuy.javaslot.dominio;

import java.util.List;

import org.microuy.javaslot.constante.JugadaTipo;

public class JugadaGanadoraBonusMenorMayor extends JugadaGanadora {

	/**
	 * Cuando se crea la configuracion
	 * de la maquina se setea el valor
	 * de este campo que representa 
	 * cuantas figuras JACKPOT deben de
	 * salir para ganar el premio
	 */
	private static int CANTIDAD_FIGURAS_PREMIO = 5;
	
	/**
	 * Monto asociado al bonus 'menor mayor'
	 */
	public float monto;
	
	public JugadaGanadoraBonusMenorMayor() {
		super(0, JugadaTipo.BONUS_MENOR_MAYOR);
	}
	
	/**
	 * La jugada formateada son los id's de 
	 * las figuras separado por coma
	 * 
	 * @param jugadaFormateada
	 * @param figurasJugada
	 * @param multiplicador
	 * @param probabilidadJugador
	 * @return
	 */
	public static JugadaGanadoraBonusMenorMayor configurarJugadaGanadoraBonusMenorMayor(Maquina maquina) {

		JugadaGanadoraBonusMenorMayor jugadaGanadoraBonusMenorMayor = null;
		
		//obtenemos la figura 'menor mayor'
		Figura figuraMenorMayor = maquina.configuracion.obtenerFiguraBonusMenorMayor();

		if(figuraMenorMayor != null) {
			
			//instanciamos la jugada ganadora
			jugadaGanadoraBonusMenorMayor = new JugadaGanadoraBonusMenorMayor();
			jugadaGanadoraBonusMenorMayor.maquina = maquina;
			
			//la cantidad de figuras del premio
			//es equivalente a la cantidad de 
			//rodillos
			for(int i=0; i<CANTIDAD_FIGURAS_PREMIO; i++) {
				jugadaGanadoraBonusMenorMayor.figuras.add(figuraMenorMayor);
			}
			
			//creamos el array de ids de figura
			//para la jugada ganadora
			int[] figurasIds = new int[CANTIDAD_FIGURAS_PREMIO];
			List<Figura> figurasJugadaGanadora = jugadaGanadoraBonusMenorMayor.figuras;
			for(int i=0; i<figurasJugadaGanadora.size(); i++) {
				figurasIds[i] = figurasJugadaGanadora.get(i).getId();
			}
			jugadaGanadoraBonusMenorMayor.figurasIds = figurasIds;
		}
		
		return jugadaGanadoraBonusMenorMayor;
	}
	
	public String toString() {
		return obtenerFormateado() + " --> BONUS MENOR o MAYOR";
	}
}