package org.microuy.javaslot.dominio.probabilidad;

import java.util.Comparator;

public class ProbabilidadJugadaGanadoraPorcentajePagoComparator implements Comparator<ProbabilidadJugadaGanadora> {

	public int compare(ProbabilidadJugadaGanadora arg0, ProbabilidadJugadaGanadora arg1) {
		
		float mult1 = arg0.porcentajePagoEntreGanadoras;
		float mult2 = arg1.porcentajePagoEntreGanadoras;
		
		return (int)(mult2-mult1);
	}
}