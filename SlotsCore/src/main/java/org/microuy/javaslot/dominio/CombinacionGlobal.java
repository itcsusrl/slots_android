package org.microuy.javaslot.dominio;

/**
 * Representa una combinacion global usando 
 * un array bidimensional como soporte 
 * 
 * @author Pablo Caviglia
 *
 */
public class CombinacionGlobal {

	public int[][] combinacion;
	
}
