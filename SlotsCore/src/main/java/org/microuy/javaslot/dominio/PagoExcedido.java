package org.microuy.javaslot.dominio;

import java.sql.Timestamp;

public class PagoExcedido {

	public int id;
	public Timestamp fecha;
	public float monto;
	public boolean pagado;

	public PagoExcedido(int id, Timestamp fecha, float monto, boolean pagado) {
		this.id = id;
		this.fecha = fecha;
		this.monto = monto;
		this.pagado = pagado;
	}
}