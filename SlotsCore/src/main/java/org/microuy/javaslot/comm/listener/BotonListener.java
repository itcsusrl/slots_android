package org.microuy.javaslot.comm.listener;

public interface BotonListener {

	public void procesarBotonPresionado(byte botonId);
	
}
