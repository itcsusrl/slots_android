package org.microuy.javaslot.comm.listener;


public class ComandoEvent {

	private int comandoId;
	private Object generico;
	private Object genericoHelper;
	
	public int getComandoId() {
		return comandoId;
	}
	public void setComandoId(int comandoId) {
		this.comandoId = comandoId;
	}
	public Object getGenerico() {
		return generico;
	}
	public void setGenerico(Object generico) {
		this.generico = generico;
	}
	public Object getGenericoHelper() {
		return genericoHelper;
	}
	public void setGenericoHelper(Object genericoHelper) {
		this.genericoHelper = genericoHelper;
	}
}