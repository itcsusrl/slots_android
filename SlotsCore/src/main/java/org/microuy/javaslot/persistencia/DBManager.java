package org.microuy.javaslot.persistencia;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import org.microuy.javaslot.persistencia.dao.ConfiguracionDAO;
import org.microuy.javaslot.persistencia.dao.DistribuidorPremiosDAO;
import org.microuy.javaslot.persistencia.dao.EstadisticaDAO;
import org.microuy.javaslot.persistencia.dao.GenericDAO;
import org.microuy.javaslot.persistencia.dao.PagoExcedidoDAO;
import org.microuy.javaslot.persistencia.dao.ResumenCobranzaDAO;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by pablo on 10/09/14.
 */
public class DBManager {

    private static List<GenericDAO> daos = new ArrayList<GenericDAO>();

    private static final int DATABASE_VERSION = 4;
    public static SQLiteDatabase db = null;

    public DBManager(Context context) {

        //inicializamos los DAO's
        daos.add(new ConfiguracionDAO());
        daos.add(new ResumenCobranzaDAO());
        daos.add(new PagoExcedidoDAO());
        daos.add(new DistribuidorPremiosDAO());
        daos.add(new EstadisticaDAO());

        //init the db
        SQLiteOpenHelper o = new PersistenceOpenHelper(context, "slots" + context.getPackageName() + ".db");
        DBManager.db = o.getWritableDatabase();
    }

    private class PersistenceOpenHelper extends SQLiteOpenHelper {

        PersistenceOpenHelper(Context context, String databaseName) {
            super(context, databaseName, null, DATABASE_VERSION);
        }

        @Override
        public void onCreate(SQLiteDatabase db) {
            for(GenericDAO genericDao : daos) {

                //drop old table
                db.execSQL("drop table if exists " + genericDao.TABLA_NOMBRE);

                //create news table and data
                genericDao.crearTabla(db);
                genericDao.crearData(db);
            }
        }

        @Override
        public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
            onCreate(db);
        }
    }
}