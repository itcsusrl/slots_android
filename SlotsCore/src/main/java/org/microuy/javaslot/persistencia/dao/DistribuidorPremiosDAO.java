package org.microuy.javaslot.persistencia.dao;

import android.database.sqlite.SQLiteDatabase;

import org.microuy.javaslot.dominio.DistribucionPremio;
import org.microuy.javaslot.persistencia.DBManager;

import java.util.ArrayList;
import java.util.List;

public class DistribuidorPremiosDAO extends GenericDAO {

	public void inicializar() {
		TABLA_NOMBRE = "distribuidor_premios";
	}

	public void crearTabla(SQLiteDatabase db) {
        db.execSQL("CREATE TABLE " + TABLA_NOMBRE + " " +
                "(id INTEGER PRIMARY KEY, " +
                "forma_jugada VARCHAR(100) NOT NULL, " +
                "acumulado FLOAT NOT NULL)");
	}

	public void crearData(SQLiteDatabase db) {
		
	}
	
	/**
	 * Crea un nuevo registro de distribucion 
	 * de jugadas en la persistencia
	 * 
	 * @param formaJugada
	 * @throws java.sql.SQLException
	 */
	public void crearDistribucion(String formaJugada) {
		
//		String sql = "INSERT INTO " + TABLA_NOMBRE + "(forma_jugada, acumulado) VALUES (?, ?)";
//
//		PreparedStatement ps = obtenerConexion().prepareStatement(sql);
//		ps.setString(1, formaJugada);
//		ps.setFloat(2, 0f);
//		ps.executeUpdate();
//
//		ps.close();
	}
	
	/**
	 * Actualiza el monto acumulable de 
	 * la jugada ganadora recibida por
	 * parametro
	 * @param formaJugada
	 * @throws java.sql.SQLException
	 */
	public void actualizarAcumulable(String formaJugada, float acumulado) {
		
//		Statement stmt = obtenerConexion().createStatement();
//		String sql = "UPDATE " + TABLA_NOMBRE + " " +
//		 			 "SET " +
//		 			 "acumulado = " + acumulado + " " +
//		 			 "WHERE forma_jugada='" + formaJugada + "'";
//
//		stmt.executeUpdate(sql);
//		stmt.close();
	}
	
	/**
	 * Obtiene todos los registros de la tabla 
	 * 
	 * @return
	 * @throws java.sql.SQLException
	 */
	public List<DistribucionPremio> obtenerDistribuciones() {
		
		List<DistribucionPremio> lista = new ArrayList<DistribucionPremio>();
		
//		//creamos el sql
//		String sql = "SELECT * FROM " + TABLA_NOMBRE;
//
//		Statement stmt = obtenerConexion().createStatement();
//		ResultSet rs = stmt.executeQuery(sql);
//
//		//recorremos la lista de
//		//distribuciones obtenidas
//		while(rs.next()) {
//
//			//obtenemos el id
//			int id = rs.getInt("id");
//			String formaJugada = rs.getString("forma_jugada");
//			float acumulado = rs.getFloat("acumulado");
//
//			DistribucionPremio distribucionPremio = new DistribucionPremio(id, formaJugada, acumulado);
//			lista.add(distribucionPremio);
//
//		}
//
//		//cerramos recursos
//		rs.close();
//		stmt.close();

		return lista;
	}
	
	/**
	 * Borra todos los registros de la tabla
	 */
	public void borrarTodasDistribuciones() {
		
//		//creamos el sql
//		String sql = "DELETE FROM " + TABLA_NOMBRE;
//
//		//creamos el statement
//		Statement stmt = obtenerConexion().createStatement();
//
//		//seteamos sql y lo ejecutamos
//		stmt.executeUpdate(sql);
		
	}
}