package org.microuy.javaslot.persistencia.dao;

import android.database.sqlite.SQLiteDatabase;

import org.microuy.javaslot.dominio.PagoExcedido;
import org.microuy.javaslot.persistencia.DBManager;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

public class PagoExcedidoDAO extends GenericDAO {

	public void inicializar() {
		TABLA_NOMBRE = "pago_excedido";
	}

	public void crearTabla(SQLiteDatabase db) {
        db.execSQL("CREATE TABLE " + TABLA_NOMBRE + " " +
                "(id INTEGER PRIMARY KEY, " +
                "fecha TIMESTAMP NOT NULL, " +
                "monto FLOAT NOT NULL, " +
                "pagado BOOLEAN NOT NULL)");
	}

	public void crearData(SQLiteDatabase db) {
	
	}
	
	public List<PagoExcedido> obtenerUltimosPagosExcedidos() {
		
		List<PagoExcedido> lista = new ArrayList<PagoExcedido>();
		
//		//creamos el sql con un limite
//		//de registros a obtener
//		String sql = "SELECT * FROM " + TABLA_NOMBRE + " ORDER BY fecha desc FETCH FIRST 10 ROWS ONLY";
//
//		Statement stmt = obtenerConexion().createStatement();
//		ResultSet rs = stmt.executeQuery(sql);
//
//		//recorremos la lista de
//		//resumenes obtenidos
//		while(rs.next()) {
//
//			//obtenemos el id
//			int id = rs.getInt("id");
//			float monto = rs.getFloat("monto");
//			Timestamp fecha = rs.getTimestamp("fecha");
//			boolean pagado = rs.getBoolean("pagado");
//
//			PagoExcedido pagoExcedido = new PagoExcedido(id, fecha, monto, pagado);
//			lista.add(pagoExcedido);
//
//		}
//
//		//cerramos recursos
//		rs.close();
//		stmt.close();
		
		return lista;
	}
	
	/**
	 * Devuelve el id del ultimo
	 * pago excedido insertado en 
	 * la tabla
	 * @return
	 * @throws java.sql.SQLException
	 */
	public PagoExcedido obtenerUltimoResumen() {

        //el objeto a devolver
        PagoExcedido pagoExcedido = null;

//        Statement stmt = obtenerConexion().createStatement();
//		String sql = "SELECT * FROM " + TABLA_NOMBRE + " ORDER BY id desc FETCH FIRST ROW ONLY";
//		ResultSet rs = stmt.executeQuery(sql);
//
//		//vamos al primer registro
//		boolean existeRegistro = rs.next();
//
//		if(existeRegistro) {
//
//			//obtenemos el id
//			int id = rs.getInt("id");
//			float monto = rs.getFloat("monto");
//			Timestamp fecha = rs.getTimestamp("fecha");
//			boolean pagado = rs.getBoolean("pagado");
//
//			pagoExcedido = new PagoExcedido(id, fecha, monto, pagado);
//
//		}
//
//		//cerramos recursos
//		rs.close();
//		stmt.close();
		
		return pagoExcedido;
	}
	
	public void crearPagoExcedido(float monto, Timestamp fecha, boolean pagado) {
		
//		String sql = "INSERT INTO " + TABLA_NOMBRE + "(monto, fecha, pagado) VALUES (?, ?, ?)";
//
//		PreparedStatement ps = obtenerConexion().prepareStatement(sql);
//		ps.setFloat(1, monto);
//		ps.setTimestamp(2, fecha);
//		ps.setBoolean(3, pagado);
//		ps.executeUpdate();
//
//		ps.close();
	}
	
	public void cambiarEstadoPagoExcedido(int id, boolean estado) {
		
//		String sql = "UPDATE " + TABLA_NOMBRE + " " +
//		 			 "SET pagado = " + estado + " " +
//		 			 "WHERE id=" + id;
//
//		Connection conexion = obtenerConexion();
//		PreparedStatement ps = conexion.prepareStatement(sql);
//		ps.executeUpdate();
//		ps.close();
//		conexion.commit();

	}
}