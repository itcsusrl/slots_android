package org.microuy.javaslot.persistencia.dao;

import android.database.sqlite.SQLiteDatabase;

import org.microuy.javaslot.persistencia.DBManager;

import java.sql.SQLException;


public abstract class GenericDAO {

	public String TABLA_NOMBRE;
	
	public abstract void inicializar();
	public abstract void crearTabla(SQLiteDatabase db);
	public abstract void crearData(SQLiteDatabase db);

	public GenericDAO() {
		inicializar();
	}
}