package org.microuy.javaslot.excepcion;

public class FiguraInexistenteException extends ConfiguracionInvalidaException {

	public FiguraInexistenteException(int id) {
		super("La figura id=" + id + " no existe");
	}
}
