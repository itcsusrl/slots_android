package org.microuy.javaslot.excepcion;

public class JugadorVirtualException extends Exception {

	public JugadorVirtualException(String mensaje) {
		super(mensaje);
	}
}