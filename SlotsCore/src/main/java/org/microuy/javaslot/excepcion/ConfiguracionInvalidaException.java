package org.microuy.javaslot.excepcion;

public class ConfiguracionInvalidaException extends RuntimeException {

	public ConfiguracionInvalidaException(String message) {
		super(message);
	}
}
