package org.microuy.javaslot.excepcion;

public class PorcentajeMaquinaMenorQueAsignadoException extends SystemException {
	
	public PorcentajeMaquinaMenorQueAsignadoException(String mensaje) {
		super(mensaje);
	}
}
