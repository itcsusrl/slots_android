package org.microuy.javaslot.excepcion;

public class WebAppException extends RuntimeException {

	public WebAppException(String message) {
		super(message);
	}
}
