package org.microuy.javaslot.presentacion.pantalla.configuracion;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;

import org.microuy.javaslot.constante.Boton;
import org.microuy.javaslot.constante.Sistema;
import org.microuy.javaslot.constante.SistemaFrontend;
import org.microuy.javaslot.persistencia.dao.ConfiguracionDAO;
import org.microuy.javaslot.presentacion.Principal;
import org.microuy.javaslot.presentacion.constante.Pantalla;
import org.microuy.javaslot.presentacion.dominio.EstadoBoton;
import org.microuy.javaslot.presentacion.modulo.ui.EnfocableIf;
import org.microuy.javaslot.presentacion.modulo.ui.SelectorUI;
import org.microuy.javaslot.presentacion.modulo.ui.TextSelectorUI;
import org.microuy.javaslot.presentacion.modulo.ui.listener.SelectorUIListener;
import org.microuy.javaslot.presentacion.pantalla.PantallaGenerica;
import org.microuy.javaslot.presentacion.pantalla.listener.PantallaListener;
import org.microuy.javaslot.sonido.ReproductorSonido;
import org.newdawn.slick.Color;
import org.newdawn.slick.Graphics;

/**
 * Menu con configuracion basica del juego, como montos minimos de apuesta,
 * apuesta maxima, incremento del valor de apuesta, porcentaje de la maquina y
 * el jugador, etc.
 * 
 * @author Pablo Caviglia
 * 
 */
public class MenuConfiguracionGeneral extends PantallaGenerica implements PantallaListener, SelectorUIListener {

	private Color colorFondo = Color.lightGray;
	private TextSelectorUI apuestaAdmitidaTextSelectorUI;
	private TextSelectorUI incrementoApuestaTextSelectorUI;
	private TextSelectorUI apuestaMaximaPorLineaTextSelectorUI;
	private TextSelectorUI factorConversionMonedaTextSelectorUI;
//	private TextSelectorUI montoApuestaGratuitaTextSelectorUI;
	//private TextSelectorUI cantidadLineasApuestaGratuitaTextSelectorUI;
	private TextSelectorUI apuestaWildTextSelectorUI;
	private TextSelectorUI porcentajeJugadorTextSelectorUI;
	private TextSelectorUI audioTextSelectorUI;
	
	private DecimalFormat integerFormat = new DecimalFormat("#");
	
	/**
	 * Lista contenedora de elementos
	 * que pueden ser enfocables
	 */
	private List<EnfocableIf> enfocables = new ArrayList<EnfocableIf>();
	
	public MenuConfiguracionGeneral(Principal principal) {
		super(Pantalla.PANTALLA_MENU_CONFIGURACION_BASICA,principal);
	}

	public void entraEnPantalla() {

	}

	public void saleDePantalla() {

	}
	
	private void presionaFlechaAbajo() {

		//buscamos el elemento
		//enfocado actualmente
		int proximoEnfocado = 0;
		for(int i=0; i<enfocables.size(); i++) {
			EnfocableIf enfocableIf = enfocables.get(i);
			if(enfocableIf.isEnfocado()) {
				proximoEnfocado = i+1;
				if(proximoEnfocado > (enfocables.size()-1)) {
					proximoEnfocado = 0;
				}
				enfocableIf.setEnfocado(false);
				break;
			}
		}
		
		//enfocamos el proximo elemento
		EnfocableIf enfocableIf = enfocables.get(proximoEnfocado);
		enfocableIf.setEnfocado(true);
		
		if(enfocableIf instanceof TextSelectorUI) {
			//reprocesamos el texto 
			//de la descripcion
			reprocesarTextoDescripcion(((TextSelectorUI)enfocableIf).descripcion);
		}
	}
	
	private void presionaFlechaArriba() {

		//buscamos el elemento
		//enfocado actualmente
		int anteriorEnfocado = 0;
		for(int i=0; i<enfocables.size(); i++) {
			EnfocableIf enfocableIf = enfocables.get(i);
			if(enfocableIf.isEnfocado()) {
				anteriorEnfocado = i-1;
				if(anteriorEnfocado < 0) {
					anteriorEnfocado = enfocables.size()-1;
				}
				enfocableIf.setEnfocado(false);
				break;
			}
		}
		
		//enfocamos el proximo elemento
		EnfocableIf enfocableIf = enfocables.get(anteriorEnfocado);
		enfocableIf.setEnfocado(true);
		
		if(enfocableIf instanceof TextSelectorUI) {
			//reprocesamos el texto 
			//de la descripcion
			reprocesarTextoDescripcion(((TextSelectorUI)enfocableIf).descripcion);
		}
	}

	private void presionaFlechaIzquierda() {
		
		for(EnfocableIf enfocableIf : enfocables) {
			if(enfocableIf.isEnfocado()) {
				if(enfocableIf instanceof TextSelectorUI) {
					TextSelectorUI textSelectorUI = (TextSelectorUI)enfocableIf;
					textSelectorUI.proximoElemento();
					break;
				}
			}
		}
	}

	private void presionaFlechaDerecha() {
		
		for(EnfocableIf enfocableIf : enfocables) {
			if(enfocableIf.isEnfocado()) {
				if(enfocableIf instanceof TextSelectorUI) {
					TextSelectorUI textSelectorUI = (TextSelectorUI)enfocableIf;
					textSelectorUI.previoElemento();
					break;
				}
			}
		}
	}

	protected void inicializarRecursos() {
		
		//definimos un titulo para 
		//esta pantalla
		titulo = "CONFIGURACION GENERAL";
		
		//creamos los componentes
		apuestaAdmitidaTextSelectorUI = new TextSelectorUI(this, 130, 150, 550,  "Apuesta admitida", ConfiguracionDAO.APUESTA_ADMITIDA, true);
		apuestaAdmitidaTextSelectorUI.agregarSelectorUIListener(this);
		apuestaAdmitidaTextSelectorUI.agregarElemento(1, "1");
		apuestaAdmitidaTextSelectorUI.agregarElemento(2, "2");
		apuestaAdmitidaTextSelectorUI.agregarElemento(5, "5");
		apuestaAdmitidaTextSelectorUI.agregarElemento(10, "10");
		apuestaAdmitidaTextSelectorUI.mostrarClavePorDefecto(Sistema.APUESTA_ADMITIDA);

		//cargamos la descripcion del  
		//componente desde la base de datos
		apuestaAdmitidaTextSelectorUI.descripcion = principal.logicaMaquina.obtenerDescripcionConfiguracion(apuestaAdmitidaTextSelectorUI.selectorUI.codigo);
		
		incrementoApuestaTextSelectorUI = new TextSelectorUI(this, 130, 190, 550,  "Incremento Apuesta", ConfiguracionDAO.INCREMENTO_APUESTA, false);
		incrementoApuestaTextSelectorUI.agregarSelectorUIListener(this);
		incrementoApuestaTextSelectorUI.agregarElemento(0.1, "0.1");
		incrementoApuestaTextSelectorUI.agregarElemento(0.2, "0.2");
		incrementoApuestaTextSelectorUI.agregarElemento(0.3, "0.3");
		incrementoApuestaTextSelectorUI.agregarElemento(0.4, "0.4");
		incrementoApuestaTextSelectorUI.agregarElemento(0.5, "0.5");
		incrementoApuestaTextSelectorUI.agregarElemento(0.6, "0.6");
		incrementoApuestaTextSelectorUI.agregarElemento(0.7, "0.7");
		incrementoApuestaTextSelectorUI.agregarElemento(0.8, "0.8");
		incrementoApuestaTextSelectorUI.agregarElemento(0.9, "0.9");
		incrementoApuestaTextSelectorUI.agregarElemento(1, "1");
		incrementoApuestaTextSelectorUI.agregarElemento(2, "2");
		incrementoApuestaTextSelectorUI.agregarElemento(5, "5");
		incrementoApuestaTextSelectorUI.agregarElemento(10, "10");
		incrementoApuestaTextSelectorUI.mostrarClavePorDefecto(Sistema.INCREMENTO_APUESTA);
		//cargamos la descripcion del  
		//componente desde la base de datos
		incrementoApuestaTextSelectorUI.descripcion = principal.logicaMaquina.obtenerDescripcionConfiguracion(incrementoApuestaTextSelectorUI.selectorUI.codigo);
		
		apuestaMaximaPorLineaTextSelectorUI = new TextSelectorUI(this, 130, 230, 550,  "Apuesta Max. por Linea", ConfiguracionDAO.APUESTA_MAXIMA_POR_LINEA, false);
		apuestaMaximaPorLineaTextSelectorUI.agregarSelectorUIListener(this);
		apuestaMaximaPorLineaTextSelectorUI.agregarElemento(0.1f, "0.1");
		apuestaMaximaPorLineaTextSelectorUI.agregarElemento(0.25f, "0.25");
		apuestaMaximaPorLineaTextSelectorUI.agregarElemento(0.3f, "0.3");
		apuestaMaximaPorLineaTextSelectorUI.agregarElemento(0.4f, "0.4");
		apuestaMaximaPorLineaTextSelectorUI.agregarElemento(0.5f, "0.5");
		apuestaMaximaPorLineaTextSelectorUI.agregarElemento(0.6f, "0.6");
		apuestaMaximaPorLineaTextSelectorUI.agregarElemento(0.75f, "0.75");
		apuestaMaximaPorLineaTextSelectorUI.agregarElemento(0.8f, "0.8");
		apuestaMaximaPorLineaTextSelectorUI.agregarElemento(0.9f, "0.9");
		apuestaMaximaPorLineaTextSelectorUI.agregarElemento(1f, "1");
		apuestaMaximaPorLineaTextSelectorUI.agregarElemento(2f, "2");
		apuestaMaximaPorLineaTextSelectorUI.agregarElemento(5f, "5");
		apuestaMaximaPorLineaTextSelectorUI.agregarElemento(10f, "10");
		apuestaMaximaPorLineaTextSelectorUI.mostrarClavePorDefecto(Sistema.APUESTA_MAXIMA_POR_LINEA);
		//cargamos la descripcion del  
		//componente desde la base de datos
		apuestaMaximaPorLineaTextSelectorUI.descripcion = principal.logicaMaquina.obtenerDescripcionConfiguracion(apuestaMaximaPorLineaTextSelectorUI.selectorUI.codigo);
		

		factorConversionMonedaTextSelectorUI = new TextSelectorUI(this, 130, 270, 550,  "Conversion moneda", ConfiguracionDAO.FACTOR_CONVERSION_MONEDA, false);
		factorConversionMonedaTextSelectorUI.agregarSelectorUIListener(this);
		factorConversionMonedaTextSelectorUI.agregarElemento(1, "x 1");
		factorConversionMonedaTextSelectorUI.agregarElemento(2, "x 2");
		factorConversionMonedaTextSelectorUI.agregarElemento(5, "x 5");
		factorConversionMonedaTextSelectorUI.agregarElemento(10, "x 10");
		factorConversionMonedaTextSelectorUI.agregarElemento(50, "x 50");
		factorConversionMonedaTextSelectorUI.agregarElemento(100, "x 100");
		factorConversionMonedaTextSelectorUI.agregarElemento(500, "x 500");
		factorConversionMonedaTextSelectorUI.agregarElemento(1000, "x 1000");
		factorConversionMonedaTextSelectorUI.mostrarClavePorDefecto(Sistema.FACTOR_CONVERSION_MONEDA);
		//cargamos la descripcion del  
		//componente desde la base de datos
		factorConversionMonedaTextSelectorUI.descripcion = principal.logicaMaquina.obtenerDescripcionConfiguracion(factorConversionMonedaTextSelectorUI.selectorUI.codigo);
		

//		montoApuestaGratuitaTextSelectorUI = new TextSelectorUI(this, 130, 310, 550,  "Monto Bonus", ConfiguracionDAO.MONTO_APUESTA_GRATUITA, false);
//		montoApuestaGratuitaTextSelectorUI.agregarSelectorUIListener(this);
//		montoApuestaGratuitaTextSelectorUI.agregarElemento(0.1, "0.1");
//		montoApuestaGratuitaTextSelectorUI.agregarElemento(0.2, "0.2");
//		montoApuestaGratuitaTextSelectorUI.agregarElemento(0.3, "0.3");
//		montoApuestaGratuitaTextSelectorUI.agregarElemento(0.4, "0.4");
//		montoApuestaGratuitaTextSelectorUI.agregarElemento(0.5, "0.5");
//		montoApuestaGratuitaTextSelectorUI.agregarElemento(0.6, "0.6");
//		montoApuestaGratuitaTextSelectorUI.agregarElemento(0.7, "0.7");
//		montoApuestaGratuitaTextSelectorUI.agregarElemento(0.8, "0.8");
//		montoApuestaGratuitaTextSelectorUI.agregarElemento(0.9, "0.9");
//		montoApuestaGratuitaTextSelectorUI.agregarElemento(1, "1");
//		montoApuestaGratuitaTextSelectorUI.agregarElemento(2, "2");
//		montoApuestaGratuitaTextSelectorUI.agregarElemento(5, "5");
//		montoApuestaGratuitaTextSelectorUI.agregarElemento(10, "10");
//		montoApuestaGratuitaTextSelectorUI.mostrarClavePorDefecto(Sistema.MONTO_APUESTA_GRATUITA);
//		//cargamos la descripcion del  
//		//componente desde la base de datos
//		montoApuestaGratuitaTextSelectorUI.descripcion = principal.logicaMaquina.obtenerDescripcionConfiguracion(montoApuestaGratuitaTextSelectorUI.selectorUI.codigo);
		
		apuestaWildTextSelectorUI = new TextSelectorUI(this, 130, 350, 550,  "Premios Wild", ConfiguracionDAO.APUESTA_WILD, false);
		apuestaWildTextSelectorUI.agregarSelectorUIListener(this);
		apuestaWildTextSelectorUI.agregarElemento(0, "Deshabilitado");
		apuestaWildTextSelectorUI.agregarElemento(1, "Habilitado");
		apuestaWildTextSelectorUI.descripcion = principal.logicaMaquina.obtenerDescripcionConfiguracion(apuestaWildTextSelectorUI.selectorUI.codigo);
		apuestaWildTextSelectorUI.mostrarClavePorDefecto(Sistema.APUESTA_WILD);

//		cantidadLineasApuestaGratuitaTextSelectorUI = new TextSelectorUI(this, 130, 350, 550,  "Cantidad Lineas Bonus", ConfiguracionDAO.CANTIDAD_LINEAS_APUESTA_GRATUITA, false);
//		cantidadLineasApuestaGratuitaTextSelectorUI.agregarSelectorUIListener(this);
//		cantidadLineasApuestaGratuitaTextSelectorUI.agregarElemento(1, "1");
//		cantidadLineasApuestaGratuitaTextSelectorUI.agregarElemento(2, "2");
//		cantidadLineasApuestaGratuitaTextSelectorUI.agregarElemento(3, "3");
//		cantidadLineasApuestaGratuitaTextSelectorUI.agregarElemento(4, "4");
//		cantidadLineasApuestaGratuitaTextSelectorUI.agregarElemento(5, "5");
//		cantidadLineasApuestaGratuitaTextSelectorUI.agregarElemento(6, "6");
//		cantidadLineasApuestaGratuitaTextSelectorUI.agregarElemento(7, "7");
//		cantidadLineasApuestaGratuitaTextSelectorUI.agregarElemento(8, "8");
//		cantidadLineasApuestaGratuitaTextSelectorUI.agregarElemento(9, "9");
//		cantidadLineasApuestaGratuitaTextSelectorUI.agregarElemento(10, "10");	
//		cantidadLineasApuestaGratuitaTextSelectorUI.mostrarClavePorDefecto(Sistema.CANTIDAD_LINEAS_APUESTA_GRATUITA);
//		//cargamos la descripcion del  
//		//componente desde la base de datos
//		cantidadLineasApuestaGratuitaTextSelectorUI.descripcion = principal.logicaMaquina.obtenerDescripcionConfiguracion(cantidadLineasApuestaGratuitaTextSelectorUI.selectorUI.codigo);

		
		porcentajeJugadorTextSelectorUI = new TextSelectorUI(this, 130, 390, 550,  "Porcentaje Jugador", ConfiguracionDAO.PORCENTAJE_JUGADOR, false);
		porcentajeJugadorTextSelectorUI.agregarSelectorUIListener(this);
		cargarPorcentajesJugador(porcentajeJugadorTextSelectorUI);
		porcentajeJugadorTextSelectorUI.mostrarClavePorDefecto(Sistema.PORCENTAJE_JUGADOR);
		//cargamos la descripcion del  
		//componente desde la base de datos
		porcentajeJugadorTextSelectorUI.descripcion = principal.logicaMaquina.obtenerDescripcionConfiguracion(porcentajeJugadorTextSelectorUI.selectorUI.codigo);
		

		audioTextSelectorUI = new TextSelectorUI(this, 130, 430, 550,  "Audio", ConfiguracionDAO.AUDIO, false);
		audioTextSelectorUI.agregarSelectorUIListener(this);
		audioTextSelectorUI.agregarElemento(true, "Activado");
		audioTextSelectorUI.agregarElemento(false, "Desactivado");
		audioTextSelectorUI.mostrarClavePorDefecto(SistemaFrontend.AUDIO_FX_ENABLED);
		//cargamos la descripcion del  
		//componente desde la base de datos
		audioTextSelectorUI.descripcion = principal.logicaMaquina.obtenerDescripcionConfiguracion(audioTextSelectorUI.selectorUI.codigo);
		

		//agregamos a la lista de
		//elementos enfocables
		enfocables.add(apuestaAdmitidaTextSelectorUI);
		enfocables.add(incrementoApuestaTextSelectorUI);
		enfocables.add(apuestaMaximaPorLineaTextSelectorUI);
		enfocables.add(factorConversionMonedaTextSelectorUI);
//		enfocables.add(montoApuestaGratuitaTextSelectorUI);
//		enfocables.add(cantidadLineasApuestaGratuitaTextSelectorUI);
		enfocables.add(apuestaWildTextSelectorUI);
		enfocables.add(porcentajeJugadorTextSelectorUI);
		enfocables.add(audioTextSelectorUI);
		
		//configuramos el primer texto
		reprocesarTextoDescripcion(apuestaAdmitidaTextSelectorUI.descripcion);
		
	}

	private void cargarPorcentajesJugador(TextSelectorUI textSelectorUI) {
		
		//cargamos valores de 30 a 95
		for(int i=30; i<=95; i++) {
			textSelectorUI.agregarElemento(i, "" + i);
		}
	}
	
	protected void renderizar(Graphics g) {
		
		//pintamos el fondo
		Color colorOriginal = g.getColor();
		g.setColor(colorFondo);
		g.fillRect(0, 0, principal.getWidth(), principal.getHeight());
		g.setColor(colorOriginal);
		
		//renderizamos cada componente
		apuestaAdmitidaTextSelectorUI.renderizar(g);
		incrementoApuestaTextSelectorUI.renderizar(g);
		apuestaMaximaPorLineaTextSelectorUI.renderizar(g);
		factorConversionMonedaTextSelectorUI.renderizar(g);
//		montoApuestaGratuitaTextSelectorUI.renderizar(g);
		apuestaWildTextSelectorUI.renderizar(g);
//		cantidadLineasApuestaGratuitaTextSelectorUI.renderizar(g);
		porcentajeJugadorTextSelectorUI.renderizar(g);
		audioTextSelectorUI.renderizar(g);
		
	}

	protected void actualizar(long elapsedTime) {
	
		apuestaAdmitidaTextSelectorUI.actualizar(elapsedTime);
		incrementoApuestaTextSelectorUI.actualizar(elapsedTime);
		apuestaMaximaPorLineaTextSelectorUI.actualizar(elapsedTime);
		factorConversionMonedaTextSelectorUI.actualizar(elapsedTime);
//		montoApuestaGratuitaTextSelectorUI.actualizar(elapsedTime);
//		cantidadLineasApuestaGratuitaTextSelectorUI.actualizar(elapsedTime);
		apuestaWildTextSelectorUI.actualizar(elapsedTime);
		porcentajeJugadorTextSelectorUI.actualizar(elapsedTime);
		audioTextSelectorUI.actualizar(elapsedTime);
		
	}

	protected void botonPresionado(byte botonId) {

		if(botonId == Boton.BOTON1) {
			principal.cambiarPantalla(Pantalla.PANTALLA_MENU_CONFIGURACION_AVANZADA);
		}
		else if(botonId == Boton.BOTON3) {
			presionaFlechaArriba();
		}
		else if(botonId == Boton.BOTON4) {
			presionaFlechaAbajo();
		}
		else if(botonId == Boton.BOTON5) {
			presionaFlechaDerecha(); 
		}
		else if(botonId == Boton.BOTON6) {
			presionaFlechaIzquierda();
		}
	}

	/**
	 * Llamado cuando cambia algun valor 
	 * de alguno de los combo selectores
	 * de la pantalla
	 */
	public void cambiaValorSelectorUI(SelectorUI selectorUI) {

		//ejecutamos el cambio 
		//en la persistencia
		principal.logicaMaquina.actualizarValorConfiguracion(selectorUI.codigo, selectorUI.elementoActualClave.toString());

		//si el combo de audio fue cambiado
		//evaluamos si es necesario detener
		//inmediatamente el sonido
		if(selectorUI.codigo.equals(ConfiguracionDAO.AUDIO)) {
			
			//obtenemos el valor elegido
			boolean estadoAudio = (Boolean)selectorUI.elementoActualClave;
			
			//si se elige detenerlo, lo
			//hacemos inmediatamente
			if(!estadoAudio) {
				ReproductorSonido.detenerAudio();
			}
		}
	}
}