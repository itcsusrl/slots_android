package org.microuy.javaslot.presentacion.pantalla.configuracion;

import org.microuy.javaslot.comm.listener.ComandoEvent;
import org.microuy.javaslot.comm.listener.ComandoListener;
import org.microuy.javaslot.constante.Boton;
import org.microuy.javaslot.constante.Comando;
import org.microuy.javaslot.constante.ModoExpulsionMoneda;
import org.microuy.javaslot.constante.Sistema;
import org.microuy.javaslot.dominio.ResumenCobranza;
import org.microuy.javaslot.dominio.estadistica.EstadisticaMaquina;
import org.microuy.javaslot.presentacion.Principal;
import org.microuy.javaslot.presentacion.constante.Pantalla;
import org.microuy.javaslot.presentacion.dominio.EstadoBoton;
import org.microuy.javaslot.presentacion.modulo.InicializacionBaseResumenCobranza;
import org.microuy.javaslot.presentacion.modulo.ui.ButtonUI;
import org.microuy.javaslot.presentacion.modulo.ui.EnfocableIf;
import org.microuy.javaslot.presentacion.pantalla.PantallaGenerica;
import org.microuy.javaslot.presentacion.pantalla.listener.PantallaListener;
import org.newdawn.slick.Color;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.UnicodeFont;

import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

public class MenuResumenCobranza extends PantallaGenerica implements PantallaListener, ComandoListener {

	//lista contenedora de los
	//ultimos resumenes de cobranza
	private List<ResumenCobranza> resumenesCobranzaList;
	
	//referencia al resumen que se
	//esta mostrando actualmente
	public ResumenCobranza resumenCobranzaActual;
	private String resumenCobranzaFechaDesdeFormateada = "";
	private String resumenCobranzaFechaHastaFormateada = "";
	
	//posicion en la lista del
	//resumen siendo visto actualmente
	private int posicionResumenActual;
	private int posicionResumenAnterior;
	
	private Color colorFondo = Color.lightGray;
	private ButtonUI finalizarResumenButtonUI;
	
//	//fuentes a ser usadas
//	private UnicodeFont resumenCobranzaFont = Principal.mediumWhiteOutlineGameFont;
//	private UnicodeFont titulosFont = Principal.bigWhiteOutlineGameFont;
//	private UnicodeFont estadisticasFont = Principal.extraSmallBlueSkyOutlineGameFont;
	
	//formateadeor de fechas
	private SimpleDateFormat dateFormatter = new SimpleDateFormat("dd/MM/yy");	
	private DecimalFormat integerFormat = new DecimalFormat("#");
	
	//modulo que muestra en pantalla
	//los controles necesarios para
	//inicializar el monto base del
	//resumen de cobranza
	public InicializacionBaseResumenCobranza inicializacionBaseResumenCobranza;
	
	//helper para contar monedas
	//en el resumen de cobranza
	private int contabilizacionMonedasHelper;
	
	/**
	 * Lista contenedora de elementos
	 * que pueden ser enfocables
	 */
	private List<EnfocableIf> enfocables = new ArrayList<EnfocableIf>();
	
	public MenuResumenCobranza(Principal principal) {

		super(Pantalla.PANTALLA_MENU_RESUMEN_COBRANZA, principal);
		
		//creamos el modulo de inicializacion
		//del monto base del resumen de cobranza
		inicializacionBaseResumenCobranza = new InicializacionBaseResumenCobranza(this);
		

	}

	public void entraEnPantalla() {

		//obtenemos la lista con los
		//ultimos resumenes de cobranza
		obtenerUltimosResumenes();
		
		//si todavia no se ha creado
		//ningun resumen de cobranza
		if(resumenesCobranzaList.size() == 0) {
			
			//crea un nuevo resumen de cobranza
			principal.logicaMaquina.crearResumenCobranza();
			
			//obtenemos nuevamente los
			//resumenes para obtener la
			//referencia al que acabamos
			//de crear
			obtenerUltimosResumenes();
			
			//mostramos el modulo de 
			//inicializacion de la base
			inicializarBase();
			
			//configuramos el primer texto
			reprocesarTextoDescripcion("La base del resumen de cobranza es la cantidad de dinero con la que se inicia la maquina luego de que se ha finalizado el resumen actual.");
		}
		else {
			//configuramos el primer texto
			reprocesarTextoDescripcion(finalizarResumenButtonUI.descripcion);
		}
		
		//fix para que cambie el
		//estado del elemento actual
		//cuando se entra a la pantalla	
		posicionResumenAnterior = -1;
		
	}
	
	private void inicializarBase() {
		
		//mostramos el modulo de 
		//inicializacion de la base
		inicializacionBaseResumenCobranza.inicializarBase();
		
	}
	
	private void obtenerUltimosResumenes() {
		
		resumenesCobranzaList = principal.logicaMaquina.obtenerUltimosResumenes();
		if(resumenesCobranzaList.size() > 0) {
			posicionResumenAnterior = posicionResumenActual;
			posicionResumenActual = resumenesCobranzaList.size() - 1;		
		}
	}
	
	public void saleDePantalla() {
		
	}
	
	/**
	 * Obtiene el componente enfocado actualmente
	 * @return
	 */
	private EnfocableIf obtenerEnfocado() {
		
		EnfocableIf enfocableIf = null;
		
		for(EnfocableIf enIf : enfocables) {
			if(enIf.isEnfocado()) {
				enfocableIf = enIf;
				break;
			}
		}
		
		return enfocableIf;
	}
	
	protected void inicializarRecursos() {
		
		//definimos un titulo para 
		//esta pantalla
		titulo = "RESUMEN COBRANZA";
		
		//creamos el boton
		finalizarResumenButtonUI = new ButtonUI(this, 360, 430, "Finalizar Resumen", "Finaliza el resumen de cobranza actual y crea uno nuevo para comenzar una nueva contabilizacion de pago.", true);
		
		//agregamos a la lista de
		//elementos enfocables
		enfocables.add(finalizarResumenButtonUI);
		
	}

	protected void renderizar(Graphics g) {
		
		//pintamos el fondo
		Color colorOriginal = g.getColor();
		g.setColor(colorFondo);
		g.fillRect(0, 0, principal.getWidth(), principal.getHeight());
		g.setColor(colorOriginal);
		
		EstadisticaMaquina estadisticaMaquina = principal.maquina.estadisticaMaquina;
		int porcentajeGanado = 0;
		if(estadisticaMaquina.dineroApostado > 0) {
			porcentajeGanado = (int)((estadisticaMaquina.dineroGanado * 100f) / estadisticaMaquina.dineroApostado);
		}
		String datosEstadisticos = "E" +  integerFormat.format(estadisticaMaquina.dineroApostado) + "   S" + integerFormat.format(estadisticaMaquina.dineroGanado) + "   P" + porcentajeGanado;
//		estadisticasFont.drawString(50, 70, datosEstadisticos);
		
		if(resumenCobranzaActual != null) {

			//si la fecha es nula significa que
			//el resumen no ha sido finalizado
			if(resumenCobranzaActual.fechaHasta == null) {
				
				//renderizamos cada componente
				finalizarResumenButtonUI.renderizar(g);
			}
			else {
				//si la fechaHasta no es nula significa
				//que el resumen ya ha sido finalizado
//				titulosFont.drawString(320, 430, "RESUMEN FINALIZADO");
			}
			
			//renderizamo el helper de
			//contabilizacion de monedas
//			resumenCobranzaFont.drawString(950, 30, String.valueOf(contabilizacionMonedasHelper));
			
		}
		
		//renderizamos la informacion
		//del resumen de cobranza
		if(resumenCobranzaActual != null) {
			
			/**
			 * Resumen #id - (fechaDesde -> fechaHasta)
			 */
//			resumenCobranzaFont.drawString(300, 180, "Resumen #" + resumenCobranzaActual.id + " - (" + resumenCobranzaFechaDesdeFormateada + " --- " + resumenCobranzaFechaHastaFormateada + ")");
			
			//calculamos cuantas monedas base fueron
			//ingresadas cuando se hizo el resumen
			int monedasBase = (int)(resumenCobranzaActual.base / Sistema.APUESTA_ADMITIDA);
			int monedasIngresadasTotal = resumenCobranzaActual.entradaMonedasPA + resumenCobranzaActual.entradaMonedasPC;
			int monedasEntregadasTotal = resumenCobranzaActual.salidaMonedasPA + resumenCobranzaActual.salidaMonedasPC;
			int monedasTotal = monedasBase + monedasIngresadasTotal - monedasEntregadasTotal;
//
//			//base
//			resumenCobranzaFont.drawString(370, 250, "Base:");
//			resumenCobranzaFont.drawString(500, 250, monedasBase + " monedas");
//
//			//monedas ingresadas
//			resumenCobranzaFont.drawString(370, 290, "Ingreso:");
//			resumenCobranzaFont.drawString(500, 290, monedasIngresadasTotal + " monedas");
//			resumenCobranzaFont.drawString(690, 290, "(" + resumenCobranzaActual.entradaMonedasPA + ")");
//
//			//monedas entregadas
//			resumenCobranzaFont.drawString(370, 330, "Egreso:");
//			resumenCobranzaFont.drawString(500, 330, monedasEntregadasTotal + " monedas");
//			resumenCobranzaFont.drawString(690, 330, "(" + resumenCobranzaActual.salidaMonedasPA + ")");
//
//			//Total Monedas
//			resumenCobranzaFont.drawString(370, 370, "Total:");
//			resumenCobranzaFont.drawString(500, 370, monedasTotal + " monedas");
//
//			resumenCobranzaFont.drawString(50, 490, "Los valores entre parentesis son las monedas que entraron o salieron de la");
//			resumenCobranzaFont.drawString(50, 520, "maquina estando la puerta abierta. El monto 'TOTAL' incluye esas monedas.");
			
			
//			//entrada, salida y saldo
//			resumenCobranzaFont.drawString(550, 250, "Entrada:");
//			resumenCobranzaFont.drawString(550, 290, "Salida:");
//			resumenCobranzaFont.drawString(550, 330, "Saldo:");			
//			resumenCobranzaFont.drawString(650, 250, "$ " + integerFormat.format(resumenCobranzaActual.entrada));
//			resumenCobranzaFont.drawString(650, 290, "$ " + integerFormat.format(resumenCobranzaActual.salida));
//			resumenCobranzaFont.drawString(650, 330, "$ " + integerFormat.format(resumenCobranzaActual.saldo));			
			
		}
		
		//renderizamos el modulo
		inicializacionBaseResumenCobranza.render(g);
		
	}

	protected void actualizar(long elapsedTime) {
	
		//si existe un resumen para desplegar
		if(resumenCobranzaActual != null) {

			//si la fecha es nula significa que
			//el resumen no ha sido finalizado
			if(resumenCobranzaActual.fechaHasta == null) {
				finalizarResumenButtonUI.actualizar(elapsedTime);	
			}
		}
		
		//si hay al menos un 
		//resumen disponible
		if(resumenesCobranzaList != null && !resumenesCobranzaList.isEmpty()) {
			
			//logica para noe ejecutar 
			//innecesariamente el codigo
			if(posicionResumenActual != posicionResumenAnterior) {
				
				//seteamos flag para no 
				//seguir entrando
				posicionResumenAnterior = posicionResumenActual;
				
				//obtenemos la referencia al
				//nuevo resumen de cobranza
				resumenCobranzaActual = resumenesCobranzaList.get(posicionResumenActual);
				
				//formateamos las fechas
				resumenCobranzaFechaDesdeFormateada = dateFormatter.format(resumenCobranzaActual.fechaDesde.getTime());
				
				//si no es nulo significa que ya
				//se encuentra finalizado el resumen
				if(resumenCobranzaActual.fechaHasta != null) {
					resumenCobranzaFechaHastaFormateada = dateFormatter.format(resumenCobranzaActual.fechaHasta.getTime());	
				}
				else {
					
					//en caso de que sea null significa
					//que el resumen no ha sido terminado,
					//por ende se refiere a la fecha actual
					resumenCobranzaFechaHastaFormateada = dateFormatter.format(System.currentTimeMillis());
				}
			}
		}
		
		//actualizamos la logica del modulo
		inicializacionBaseResumenCobranza.update(elapsedTime);

	}

	protected void botonPresionado(byte botonId) {

		//si el modulo de inicializacion de 
		//la base se encuentra habilitado 
		//ejecutamos la logica de botones de
		//ese modulo
		if(inicializacionBaseResumenCobranza.moduloHabilitado) {
			inicializacionBaseResumenCobranza.botonPresionado(botonId);
		}
		else {
			
			if(botonId == Boton.BOTON1) {
				principal.cambiarPantalla(Pantalla.PANTALLA_MENU_PRINCIPAL);
			}
			else if(botonId == Boton.BOTON3) {
				//resumen anterior
				if((posicionResumenActual-1) >= 0) {
					posicionResumenAnterior = posicionResumenActual;
					posicionResumenActual--;
				}
			}
			else if(botonId == Boton.BOTON4) {
				//resumen siguiente
				if((posicionResumenActual+1) <= (resumenesCobranzaList.size()-1)) {
					posicionResumenAnterior = posicionResumenActual;
					posicionResumenActual++;
				}
			}
			else if(botonId == Boton.BOTON5) {
			
				//cambiamos el estado del motor
				principal.logicaMaquina.cambiarEstadoMotor(!inicializacionBaseResumenCobranza.motorPrendido);
			
				//obtenemos el estado del motor
				//para que se ejecute un callback
				//desde el PIC y se configure el 
				//estado del primer boton que es
				//el que indica el estado del motor
				principal.logicaMaquina.obtenerEstadoMotor();

			}
			
			/**
			 * Grabamos el resumen actual y
			 * ejecutamos la logica para 
			 * crear uno nuevo
			 */
			if(obtenerEnfocado() == finalizarResumenButtonUI) {
				
				if(botonId == Boton.BOTON6) {

					//finalizamos el recurso actual 
					//para dar lugar a uno nuevo 
					principal.logicaMaquina.finalizarResumenActual();
					
					//cargamos nuevamente los resumenes
					obtenerUltimosResumenes();
					
					//mostramos el menu de 
					//inicializacion de base
					inicializarBase();

				}
			}
		}
	}

	public void procesarEventoComando(ComandoEvent comandoEvent) {
		
		if(comandoEvent.getComandoId() == Comando.COMANDO_CODIGO_ESTADO_MOTOR) {
			
			//obtenemos el estado del
			//motor desde el comando
			boolean prendido = ((Byte)comandoEvent.getGenerico()) == 1;
				
			//actualiza el estado del motor
			inicializacionBaseResumenCobranza.actualizarEstadoMotor(prendido);

			//si el motor se prendio
			if(prendido) {
				//reiniciamos el helper de 
				//contabilizacion de monedas
				contabilizacionMonedasHelper = 0;
			}
		}
		else if(comandoEvent.getComandoId() == Comando.COMANDO_VERIFICAR_PAGO_MONEDA) {
			
			//obtenemos el modo de expulsion de 
			//moneda que genera este comando
			byte modoExpulsionMoneda = (Byte)comandoEvent.getGenerico();
			
			if(modoExpulsionMoneda == ModoExpulsionMoneda.MODO_EXPULSION_MONEDA_ADMINISTRADOR) {
				
				if(inicializacionBaseResumenCobranza.moduloHabilitado) {
					inicializacionBaseResumenCobranza.incrementarMoneda();	
				}
				else {
					//incrementamos el helper
					//de contabilizacion de monedas
					contabilizacionMonedasHelper++;
				}
			}
		}
	}
	
	/**
	 * Llamado por el modulo de inicializacion
	 * de la base cuando se termina de editar
	 * la base
	 */
	public void finalizaEdicionBaseCallback(int cantidadMonedas) {

		//actualizamos en la persistencia
		//el monto base del ultimo resumen
		principal.logicaMaquina.actualizarUltimoResumenCobranzaBase((cantidadMonedas * Sistema.APUESTA_ADMITIDA));
		
		//configuramos el nuevo monto
		//base tambien en memoria
		resumenesCobranzaList.get(resumenesCobranzaList.size()-1).base = (cantidadMonedas * Sistema.APUESTA_ADMITIDA);
		
	}
}