package org.microuy.javaslot.presentacion.modulo;

import org.newdawn.slick.Graphics;

public interface ComponenteEscritorio {

	public int getX();
	public int getY();
	
	public void setX(int x);
	public void setY(int y);
	
	public void render(Graphics g);
	public void update(long elapsedTime);
	public void inicializarRecursos();
	
}
