package org.microuy.javaslot.presentacion.modulo.ui;

import org.microuy.javaslot.presentacion.Principal;
import org.microuy.javaslot.presentacion.modulo.ModuloGenerico;
import org.microuy.javaslot.presentacion.modulo.ui.listener.SelectorUIListener;
import org.microuy.javaslot.presentacion.pantalla.PantallaGenerica;
import org.newdawn.slick.Color;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.UnicodeFont;

public class TextSelectorUI extends ModuloGenerico implements EnfocableIf {

	public boolean enfocado;
	private Color colorFuente = Color.white;
	private Color colorFuenteEnfocado = Color.red;
	public SelectorUI selectorUI;
	public String texto;
	public String descripcion;
	
//	private static UnicodeFont fuente = Principal.bigWhiteOutlineGameFont;
	
	public TextSelectorUI(PantallaGenerica pantallaGenerica, int x, int y, int x2, String texto, String codigo, boolean enfocado) {
//		super(pantallaGenerica, x, y, 0, fuente.getHeight("A")+4);
        super(pantallaGenerica, x, y, 0, 15+4);
		this.enfocado = enfocado;
		this.texto = texto;
		selectorUI = new SelectorUI(pantallaGenerica, x2, y, codigo, enfocado);
	}

	public TextSelectorUI(PantallaGenerica pantallaGenerica, int x, int y, String texto, String codigo, boolean enfocado) {
//		this(pantallaGenerica, x, y, x + 25 + fuente.getWidth(texto), texto, codigo, enfocado);
        this(pantallaGenerica, x, y, x + 25 + 50, texto, codigo, enfocado);
	}
	
	public void agregarSelectorUIListener(SelectorUIListener listener) {
		selectorUI.agregarSelectorUIListener(listener);
	}
	
	public void agregarElemento(Object clave, String valor) {
		selectorUI.agregarElemento(clave, valor);
	}
	
	public void borrarElementos() {
		selectorUI.borrarElementos();
	}
	
	public void setTexto(String texto) {
		this.texto = texto;
	}

	/**
	 * Se mueve al proximo elemento
	 * de la lista
	 */
	public void proximoElemento() {
		selectorUI.proximoElemento();
	}
	
	public void previoElemento() {
		selectorUI.previoElemento();
	}
	
	public void setColorFuente(Color colorFuente) {
		this.colorFuente = colorFuente;
	}

	public void render(Graphics g) {

//		fuente.drawString(x, y, texto, enfocado ? colorFuenteEnfocado: colorFuente);
		selectorUI.render(g);
	}

	public void update(long elapsedTime) {

		selectorUI.update(elapsedTime);
	}

	public void inicializarRecursos() {
		
	}

	public boolean isEnfocado() {
		return enfocado;
	}

	public void setEnfocado(boolean enfocado) {
		this.enfocado = enfocado;
		selectorUI.enfocado = enfocado;
	}
	
	public void mostrarClavePorDefecto(Object clave) {
		selectorUI.mostrarClavePorDefecto(clave);
	}
}