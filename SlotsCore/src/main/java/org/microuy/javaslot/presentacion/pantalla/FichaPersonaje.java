package org.microuy.javaslot.presentacion.pantalla;

import org.microuy.javaslot.comm.listener.ComandoEvent;
import org.microuy.javaslot.comm.listener.ComandoListener;
import org.microuy.javaslot.constante.Comando;
import org.microuy.javaslot.excepcion.SystemException;
import org.microuy.javaslot.presentacion.Principal;
import org.microuy.javaslot.presentacion.constante.Pantalla;
import org.microuy.javaslot.presentacion.pantalla.listener.BotonSinEstadoListener;
import org.microuy.javaslot.util.SlotsRandomGenerator;
import org.microuy.javaslot.util.Timer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.Image;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.state.transition.FadeInTransition;
import org.newdawn.slick.state.transition.FadeOutTransition;

public class FichaPersonaje extends PantallaGenerica implements BotonSinEstadoListener, ComandoListener {

	//flag para finalizar los
	//recursos antes de salir
	private boolean finalizar;
	
//	private Timer timerPantalla = new Timer(2000);
	private Timer timerPantalla = new Timer(25000);
	
	//cargamos la ficha
	private Image fichaActual;
	
	public FichaPersonaje(Principal principal) throws SlickException {
		super(Pantalla.PANTALLA_FICHA_PERSONAJE, principal);
		addBotonSinEstadoListener(this);
	}

	protected void renderizar(Graphics g) {
		
		//si hay una ficha, la dibujamos
		if(fichaActual != null) {
			fichaActual.draw();
		}
	}
	
	protected void actualizar(long elapsedTime) {
		
		if(timerPantalla.action(elapsedTime)) {
			finalizarYSalir();
		}
		
		if(finalizar) {
			finalizar = false;
			saleDePantalla();
		}
	}

	protected void inicializarRecursos() {
		
	}
	
	public void botonPresionado(byte botonId) {

	}

	private void finalizarYSalir() {

		timerPantalla.refresh();
		finalizar = true;
	}
	
	public void entraEnPantalla() {
		
		//obtiene una ficha aleatoria
		fichaActual = obtenerFichaAleatoria();
	}
	
	private Image obtenerFichaAleatoria() {
		
		Image fichaImage = null;
		
		//obtenemos un numero aleatorio
		//del 1 al 12 (cada figura)
		int aleatorio = SlotsRandomGenerator.getInstance().nextInt(12) + 1;

		try {
			
			switch(aleatorio) {
			case 1:
				fichaImage = new Image("resources/fichas/fichas-01.jpg");
				break;
			case 2:
				fichaImage = new Image("resources/fichas/fichas-02.jpg");
				break;
			case 3:
				fichaImage = new Image("resources/fichas/fichas-03.jpg");
				break;
			case 4:
				fichaImage = new Image("resources/fichas/fichas-04.jpg");
				break;
			case 5:
				fichaImage = new Image("resources/fichas/fichas-05.jpg");
				break;
			case 6:
				fichaImage = new Image("resources/fichas/fichas-06.jpg");
				break;
			case 7:
				fichaImage = new Image("resources/fichas/fichas-07.jpg");
				break;
			case 8:
				fichaImage = new Image("resources/fichas/fichas-08.jpg");
				break;
			case 9:
				fichaImage = new Image("resources/fichas/fichas-09.jpg");
				break;
			case 10:
				fichaImage = new Image("resources/fichas/fichas-10.jpg");
				break;
			case 11:
				fichaImage = new Image("resources/fichas/fichas-11.jpg");
				break;
			case 12:
				fichaImage = new Image("resources/fichas/fichas-12.jpg");
				break;
			}
		} 
		catch (SlickException e) {
			throw new SystemException(e.getMessage());
		}
		
		return fichaImage;
	}
	
	public void saleDePantalla() {
		
		try {
			timerPantalla.refresh();
			fichaActual.destroy();
			fichaActual = null;
		} 
		catch (SlickException e) {
			e.printStackTrace();
		}

		//cambiamos de pantalla
		principal.cambiarPantalla(Pantalla.PANTALLA_MENU_PRESENTACION, new FadeOutTransition(), new FadeInTransition());
	
	}

	public void botonPresionadoSinEstado(byte botonId) {
		botonPresionado(botonId);
	}

	public void procesarEventoComando(ComandoEvent comandoEvent) {
		
		if(principal.getCurrentStateID() == id) {
			
			if(comandoEvent.getComandoId() == Comando.COMANDO_DINERO_INGRESADO) {

				//finaliza los recursos 
				//y sale al juego
				finalizarYSalir();
			}
		}
	}
}