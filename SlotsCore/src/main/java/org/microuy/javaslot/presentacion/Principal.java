package org.microuy.javaslot.presentacion;

import org.microuy.javaslot.comm.listener.ComandoEvent;
import org.microuy.javaslot.comm.listener.ComandoListener;
import org.microuy.javaslot.constante.Comando;
import org.microuy.javaslot.constante.EstadoMaquina;
import org.microuy.javaslot.constante.Sistema;
import org.microuy.javaslot.dominio.Maquina;
import org.microuy.javaslot.excepcion.SystemException;
import org.microuy.javaslot.logica.LogicaMaquina;
import org.microuy.javaslot.persistencia.dao.ConfiguracionDAO;
import org.microuy.javaslot.presentacion.constante.Pantalla;
import org.microuy.javaslot.presentacion.modulo.ConfirmacionCapacidadPagoExcedida;
import org.microuy.javaslot.presentacion.modulo.GeneralInfoModule;
import org.microuy.javaslot.presentacion.modulo.ManejadorEscritorio;
import org.microuy.javaslot.presentacion.pantalla.AyudaDescriptiva;
import org.microuy.javaslot.presentacion.pantalla.AyudaPremiosNormales;
import org.microuy.javaslot.presentacion.pantalla.FichaPersonaje;
import org.microuy.javaslot.presentacion.pantalla.Juego;
import org.microuy.javaslot.presentacion.pantalla.MenuPresentacion;
import org.microuy.javaslot.presentacion.pantalla.PantallaGenerica;
import org.microuy.javaslot.presentacion.pantalla.PantallaTest;
import org.microuy.javaslot.presentacion.pantalla.TablaPago;
import org.microuy.javaslot.presentacion.pantalla.configuracion.MenuConfiguracionAvanzada;
import org.microuy.javaslot.presentacion.pantalla.configuracion.MenuConfiguracionGeneral;
import org.microuy.javaslot.presentacion.pantalla.configuracion.MenuJackpot;
import org.microuy.javaslot.presentacion.pantalla.configuracion.MenuMenorMayor;
import org.microuy.javaslot.presentacion.pantalla.configuracion.MenuPagoExcedido;
import org.microuy.javaslot.presentacion.pantalla.configuracion.MenuPrincipal;
import org.microuy.javaslot.presentacion.pantalla.configuracion.MenuRecuperacionDinero;
import org.microuy.javaslot.presentacion.pantalla.configuracion.MenuResumenCobranza;
import org.microuy.javaslot.presentacion.pantalla.listener.PantallaListener;
import org.microuy.javaslot.sonido.ReproductorSonido;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.state.StateBasedGame;
import org.newdawn.slick.state.transition.EmptyTransition;
import org.newdawn.slick.state.transition.FadeInTransition;
import org.newdawn.slick.state.transition.FadeOutTransition;
import org.newdawn.slick.state.transition.Transition;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;

public class Principal extends StateBasedGame implements ComandoListener {

	private GameContainer gameContainer;

	//instancia de la maquina
	public Maquina maquina;
	public LogicaMaquina logicaMaquina;
	
	/**
	 * Indica si la aplicacion se encuentra en
	 * modo configuracion o en modo juego
	 */
	public static boolean modoConfiguracion;
	
	//estado del juego
	public byte estado = EstadoMaquina.EN_ESPERA;
	
//	//la fuente principal del juego
//	public static UnicodeFont superDuperBigShadowOutlineGameFont;
//	public static UnicodeFont superBigShadowOutlineGameFont;
//	public static UnicodeFont extraBigShadowOutlineGameFont;
//	public static UnicodeFont extraBigYellowBlueGradientShadowOutlineGameFont;
//	public static UnicodeFont bigWhiteOutlineGameFont;
//	public static UnicodeFont bigBlueskyOutlineGameFont;
//	public static UnicodeFont mediumLargeWhiteOutlineGameFont;
//	public static UnicodeFont mediumLargeYellowOutlineGameFont;
//	public static UnicodeFont mediumWhiteOutlineGameFont;
//	public static UnicodeFont smallWhiteOutlineGameFont;
//	public static UnicodeFont smallBlueskyOutlineGameFont;
//	public static UnicodeFont bigYellowOutlineGameFont;
//	public static UnicodeFont extraSmallBlueSkyOutlineGameFont;
//	public static UnicodeFont botonVirtualFont;
//	public static UnicodeFont botonBiggerVirtualFont;
	
	//formateador de numeros enteros
	public DecimalFormat integerFormat = new DecimalFormat("#");
	
	public GeneralInfoModule generalInfoModule;

	//listeners de las pantallas
	private List<PantallaListener> pantallaListeners;
	
	/**
	 * Monto acumulado en el JACKPOT
	 */
	public float montoJackpotUI;

	//confirmacion de que la 
	//capacidad de pago de la
	//maquina ha sido excedida
	public ConfirmacionCapacidadPagoExcedida confirmacionCapacidadPagoExcedida;

	/**
	 * Indica si la maquina se
	 * encuentra en modo demo 
	 * o en modo real
	 */
	public boolean demo = true;

	public Principal(Maquina maquina) {
		super("");
		try {
			//obtengo la instancia de la
			//maquina del juego
			this.maquina = maquina;
			logicaMaquina = maquina.logicaMaquina;
			
			//inicializo los listeners de pantalla
			pantallaListeners = new ArrayList<PantallaListener>();

		} catch (Exception e) {
			e.printStackTrace();
			throw new SystemException("Error inicializando Principal");
		}
	}
	
	public void initStatesList(GameContainer gameContainer) throws SlickException {

        //asignamos instancia
        this.gameContainer = gameContainer;

        //cargamos las fuentes del sistema
		cargarFuentes();
		
		//cargamos el monto 
		//acumulado del jackpot
		montoJackpotUI = logicaMaquina.distribuidorPremios.montoDistribuidoJackpot;
		
		//instanciamos el modulo que
		//despliega informacion arriba
		generalInfoModule = new GeneralInfoModule(this, 0, 0, getWidth(), 70);
		
		/**
		 * Cada pantalla del juego es un
		 * estado diferente. De aqui en mas
		 * la maquina define cada pantalla en
		 * una maquina de estados
		 */
		MenuPresentacion menuPresentacion = new MenuPresentacion(this);
		Juego juego = new Juego(this);
		TablaPago tablaPago = new TablaPago(this);
		MenuConfiguracionGeneral menuConfiguracionBasica = new MenuConfiguracionGeneral(this); 
		MenuPrincipal menuPrincipal = new MenuPrincipal(this);
		PantallaTest pantallaTest = new PantallaTest(this);
		MenuJackpot menuJackpot = new MenuJackpot(this);
		MenuConfiguracionAvanzada menuConfiguracionAvanzada = new MenuConfiguracionAvanzada(this);
		MenuResumenCobranza menuResumenCobranza = new MenuResumenCobranza(this);
		MenuRecuperacionDinero menuRecuperacionDinero = new MenuRecuperacionDinero(this);
		MenuMenorMayor menuMenorMayor = new MenuMenorMayor(this);
		AyudaPremiosNormales ayudaPremiosNormales = new AyudaPremiosNormales(this);
		AyudaDescriptiva ayudaDescriptiva = new AyudaDescriptiva(this);
		FichaPersonaje fichaPersonaje = new FichaPersonaje(this);
		MenuPagoExcedido menuPagoExcedido = new MenuPagoExcedido(this);
		
		//agrego los estados al sistema
		addState(menuPresentacion);
		addState(menuPrincipal);
		addState(juego);
		addState(tablaPago);
		addState(menuConfiguracionBasica);
		addState(pantallaTest);
		addState(menuJackpot);
		addState(menuConfiguracionAvanzada);
		addState(menuResumenCobranza);
		addState(menuRecuperacionDinero);
		addState(menuMenorMayor);
		addState(ayudaPremiosNormales);
		addState(ayudaDescriptiva);
		addState(fichaPersonaje);
		addState(menuPagoExcedido);
		
		//asigno los listeners de pantalla 
		pantallaListeners.add(menuPresentacion);
		pantallaListeners.add(juego);
		pantallaListeners.add(tablaPago);
		pantallaListeners.add(menuConfiguracionBasica);
		pantallaListeners.add(menuPrincipal);
		pantallaListeners.add(pantallaTest);
		pantallaListeners.add(menuJackpot);
		pantallaListeners.add(menuConfiguracionAvanzada);
		pantallaListeners.add(menuResumenCobranza);
		pantallaListeners.add(menuRecuperacionDinero);
		pantallaListeners.add(menuMenorMayor);
		pantallaListeners.add(ayudaPremiosNormales);
		pantallaListeners.add(ayudaDescriptiva);
		pantallaListeners.add(menuPagoExcedido);

		//modulo que despliega el mensaje que
		//indica que la capacidad de pago fue
		//excedida
		confirmacionCapacidadPagoExcedida = new ConfirmacionCapacidadPagoExcedida(this);

		//chequeamos si existe un monto
		//previo que haya quedado de una
		//partida no finaliza anterior
		float montoJugador = Float.parseFloat(logicaMaquina.obtenerValorConfiguracion(ConfiguracionDAO.MONTO_JUGADOR).trim());
		
		//mostramos en la barra 
		//superior la info de jackpot
		mostrarInfoJackpot();
		
		//cargamos la musica 
		ReproductorSonido.cargarMusica(new int[]{ReproductorSonido.MUSICA_PRESENTACION, ReproductorSonido.MUSICA_MINIJUEGO});
		ReproductorSonido.cargarSonidos(new int[]{ReproductorSonido.ENTRADA_MONEDA});
		
		//chequeamos la existencia de un
		//resumen de cobranza, en caso de 
		//que no exista ninguno creamos el
		//primero
//		ResumenCobranza resumenCobranza = logicaMaquina.obtenerUltimoResumen();
//		if(resumenCobranza == null) {
//			//entramos al menu del resumen
//			//de cobranza para crear el primero
//			cambiarPantalla(Pantalla.PANTALLA_MENU_RESUMEN_COBRANZA);
//		}
//		else {
			
			//si el monto del jugador obtenido
			//desde la persistencia es cero, lo
			//enviamos al menu de presentacion
			if(montoJugador == 0) {
				//entramos al menu de presentacion
//				cambiarPantalla(Pantalla.PANTALLA_MENU_PRESENTACION);

                continuarHaciaJuego();

			}
			else {
				//si el monto del jugador no
				//es cero significa que debemos
				//de dar la alternativa de 
				//recuperar el dinero
				cambiarPantalla(Pantalla.PANTALLA_MENU_RECUPERACION_DINERO);
			}
//		}
	}

    public void continuarHaciaJuego() {

        //cambio el estado de la maquina
        setEstado(EstadoMaquina.JUGANDO);

        //seteamos el modo de juego
        demo = false;

        //vamos al juego
        cambiarPantalla(Pantalla.PANTALLA_JUEGO, new FadeOutTransition(), new FadeInTransition());
    }


    /**
	 * Muestra el monto del Jackpot
	 * en la barra informativa superior
	 */
	public void mostrarInfoJackpot() {
		
		//configuramos el texto del
		//jackpot a mostrar en la 
		//barra informativa
		String textoInfo = "JACKPOT @" + integerFormat.format(montoJackpotUI * Sistema.FACTOR_CONVERSION_MONEDA);	
		
		//mostramos la info en la barra superior
		mostrarInfo(textoInfo);

	}
	
	/**
	 * Muestra info en la barra superior
	 */
	public void mostrarInfo(String texto) {
		generalInfoModule.mostrarTexto(texto);
	}
	
	private void cargarFuentes() {
//
//		try {
//
//			OutlineEffect outlineEffect = new OutlineEffect();
//			ColorEffect greyColorEffect = new ColorEffect(new Color(87,87,87));
//			ColorEffect whiteColorEffect = new ColorEffect(new Color(255,255,255));
//			ColorEffect yellowColorEffect = new ColorEffect(new Color(255,255,0));
//			ColorEffect blueskyColorEffect = new ColorEffect(new Color(129, 210, 242));
//			GradientEffect blueWhiteGradientEffect = new GradientEffect(new Color(74, 231, 255), java.awt.Color.white, 0.5f);
//			ShadowEffect shadowEffect = new ShadowEffect();
//
//			//cargo la fuente grande del juego
//			superDuperBigShadowOutlineGameFont = new UnicodeFont("resources/fonts/BigBimboNC.ttf", 320, false, false);
//			superDuperBigShadowOutlineGameFont.getEffects().add(outlineEffect);
//			superDuperBigShadowOutlineGameFont.getEffects().add(shadowEffect);
//			superDuperBigShadowOutlineGameFont.getEffects().add(blueWhiteGradientEffect);
//			superDuperBigShadowOutlineGameFont.addGlyphs("123");
//			superDuperBigShadowOutlineGameFont.loadGlyphs();
//
//			//cargo la fuente grande del juego
//			superBigShadowOutlineGameFont = new UnicodeFont("resources/fonts/BigBimboNC.ttf", 70, false, false);
//			superBigShadowOutlineGameFont.getEffects().add(outlineEffect);
//			superBigShadowOutlineGameFont.getEffects().add(shadowEffect);
//			superBigShadowOutlineGameFont.getEffects().add(whiteColorEffect);
//			superBigShadowOutlineGameFont.addAsciiGlyphs();
//			superBigShadowOutlineGameFont.loadGlyphs();
//
//			//cargo la fuente grande del juego
//			extraBigShadowOutlineGameFont = new UnicodeFont("resources/fonts/BigBimboNC.ttf", 55, false, false);
//			extraBigShadowOutlineGameFont.getEffects().add(outlineEffect);
//			extraBigShadowOutlineGameFont.getEffects().add(shadowEffect);
//			extraBigShadowOutlineGameFont.getEffects().add(whiteColorEffect);
//			extraBigShadowOutlineGameFont.addAsciiGlyphs();
//			extraBigShadowOutlineGameFont.loadGlyphs();
//
//			extraBigYellowBlueGradientShadowOutlineGameFont = new UnicodeFont("resources/fonts/BigBimboNC.ttf", 50, false, false);
//			extraBigYellowBlueGradientShadowOutlineGameFont.getEffects().add(outlineEffect);
//			extraBigYellowBlueGradientShadowOutlineGameFont.getEffects().add(shadowEffect);
//			extraBigYellowBlueGradientShadowOutlineGameFont.getEffects().add(whiteColorEffect);
//			extraBigYellowBlueGradientShadowOutlineGameFont.addAsciiGlyphs();
//			extraBigYellowBlueGradientShadowOutlineGameFont.loadGlyphs();
//
//			//cargo la fuente grande del juego
//			bigWhiteOutlineGameFont = new UnicodeFont("resources/fonts/BigBimboNC.ttf", 30, false, false);
//			bigWhiteOutlineGameFont.getEffects().add(outlineEffect);
//			bigWhiteOutlineGameFont.getEffects().add(whiteColorEffect);
//			bigWhiteOutlineGameFont.addAsciiGlyphs();
//			bigWhiteOutlineGameFont.loadGlyphs();
//
//			//cargo la fuente grande del juego
//			bigBlueskyOutlineGameFont = new UnicodeFont("resources/fonts/BigBimboNC.ttf", 42, false, false);
//			bigBlueskyOutlineGameFont.getEffects().add(outlineEffect);
//			bigBlueskyOutlineGameFont.getEffects().add(blueskyColorEffect);
//			bigBlueskyOutlineGameFont.addAsciiGlyphs();
//			bigBlueskyOutlineGameFont.loadGlyphs();
//
//			//cargo la fuente grande del juego
//			mediumLargeWhiteOutlineGameFont = new UnicodeFont("resources/fonts/BigBimboNC.ttf", 35, false, false);
//			mediumLargeWhiteOutlineGameFont.getEffects().add(outlineEffect);
//			mediumLargeWhiteOutlineGameFont.getEffects().add(whiteColorEffect);
//			mediumLargeWhiteOutlineGameFont.addAsciiGlyphs();
//			mediumLargeWhiteOutlineGameFont.loadGlyphs();
//
//			//cargo la fuente grande amarilla del juego
//			mediumLargeYellowOutlineGameFont = new UnicodeFont("resources/fonts/BigBimboNC.ttf", 35, false, false);
//			mediumLargeYellowOutlineGameFont.getEffects().add(outlineEffect);
//			mediumLargeYellowOutlineGameFont.getEffects().add(yellowColorEffect);
//			mediumLargeYellowOutlineGameFont.addAsciiGlyphs();
//			mediumLargeYellowOutlineGameFont.loadGlyphs();
//
//			//cargo la fuente mediana del juego
//			mediumWhiteOutlineGameFont = new UnicodeFont("resources/fonts/BigBimboNC.ttf", 30, false, false);
//			mediumWhiteOutlineGameFont.getEffects().add(outlineEffect);
//			mediumWhiteOutlineGameFont.getEffects().add(whiteColorEffect);
//			mediumWhiteOutlineGameFont.addAsciiGlyphs();
//			mediumWhiteOutlineGameFont.loadGlyphs();
//
//			//cargo la fuente chica del juego
//			smallWhiteOutlineGameFont = new UnicodeFont("resources/fonts/BigBimboNC.ttf", 28, false, false);
//			smallWhiteOutlineGameFont.getEffects().add(outlineEffect);
//			smallWhiteOutlineGameFont.getEffects().add(whiteColorEffect);
//			smallWhiteOutlineGameFont.addAsciiGlyphs();
//			smallWhiteOutlineGameFont.loadGlyphs();
//
//			//cargo la fuente chica del juego
//			smallBlueskyOutlineGameFont = new UnicodeFont("resources/fonts/BigBimboNC.ttf", 28, false, false);
//			smallBlueskyOutlineGameFont.getEffects().add(outlineEffect);
//			smallBlueskyOutlineGameFont.getEffects().add(blueskyColorEffect);
//			smallBlueskyOutlineGameFont.addAsciiGlyphs();
//			smallBlueskyOutlineGameFont.loadGlyphs();
//
//			//cargo la fuente chica del juego
//			//cargo la fuente grande del juego
//			bigYellowOutlineGameFont = new UnicodeFont("resources/fonts/BigBimboNC.ttf", 30, false, false);
//			bigYellowOutlineGameFont.getEffects().add(outlineEffect);
//			bigYellowOutlineGameFont.getEffects().add(yellowColorEffect);
//			bigYellowOutlineGameFont.addAsciiGlyphs();
//			bigYellowOutlineGameFont.loadGlyphs();
//
//			//cargo la fuente chica del juego
//			extraSmallBlueSkyOutlineGameFont = new UnicodeFont("resources/fonts/BigBimboNC.ttf", 16, false, false);
//			extraSmallBlueSkyOutlineGameFont.getEffects().add(outlineEffect);
//			extraSmallBlueSkyOutlineGameFont.getEffects().add(blueskyColorEffect);
//			extraSmallBlueSkyOutlineGameFont.addAsciiGlyphs();
//			extraSmallBlueSkyOutlineGameFont.loadGlyphs();
//
//			//cargo la fuente de los botones
//			botonVirtualFont = new UnicodeFont("resources/fonts/BigBimboNC.ttf", 24, false, false);
//			botonVirtualFont.getEffects().add(greyColorEffect);
//			botonVirtualFont.addAsciiGlyphs();
//			botonVirtualFont.loadGlyphs();
//
//			//cargo la fuente de los botones
//			botonBiggerVirtualFont = new UnicodeFont("resources/fonts/BigBimboNC.ttf", 28, false, false);
//			botonBiggerVirtualFont.getEffects().add(greyColorEffect);
//			botonBiggerVirtualFont.addAsciiGlyphs();
//			botonBiggerVirtualFont.loadGlyphs();
//
//
//		} catch (Exception e) {
//			e.printStackTrace();
//		}
	}

	/**
	 * Procesar el evento de un comando 
	 */
	public void procesarEventoComando(ComandoEvent comandoEvent) {
	
		/**
		 * Ingreso dinero en la maquina. Tomar
		 * acciones como chequeo de estado de juego,
		 * unos cuantos cambios graficos, etc.
		 */
		if(comandoEvent.getComandoId() == Comando.COMANDO_DINERO_INGRESADO) {
			
			//sonido de entrada de monedas
			ReproductorSonido.playSound(ReproductorSonido.ENTRADA_MONEDA);
			
			//si el estado de la maquina es en espera
			//cambio el estado a jugando porque hay
			//nuevo credito pronto para ser gastado
			if(demo) {
				
				//cambio el estado de la maquina
				estado = EstadoMaquina.JUGANDO;
				
				//si no estamos en el menu de
				//presentacion vamos a el
				if(getCurrentStateID() != Pantalla.PANTALLA_MENU_PRESENTACION) {
					cambiarPantalla(Pantalla.PANTALLA_MENU_PRESENTACION, new FadeOutTransition(), new FadeInTransition());
				}
				
				//si estamos en la pantalla de
				//juego y estamos en modo demo
				//detenemos el giro de los 
				//rodillos 
				if(getCurrentStateID() == Pantalla.PANTALLA_JUEGO) {
					Juego juego = (Juego)getState(Pantalla.PANTALLA_JUEGO);
					juego.rodillos.detenerRodillosInmediatamente();
				}
			}
			
			//chequea si el dinero ingresado
			//sucedio en alguna de las pantallas
			//de demo
			boolean ingresoCreditoModoDemo = false;
			
			//obtenemos el estado actual
			int estadoActual = getCurrentStateID();
			if(estadoActual == Pantalla.PANTALLA_MENU_PRESENTACION || 
			   estadoActual == Pantalla.PANTALLA_AYUDA_DESCRIPTIVA || 
			   estadoActual == Pantalla.PANTALLA_AYUDA_PREMIOS_NORMALES ||
			   estadoActual == Pantalla.PANTALLA_FICHA_PERSONAJE ||
			   estadoActual == Pantalla.PANTALLA_JUEGO) {
				
				ingresoCreditoModoDemo = true;
			}
			
			//obtengo la pantalla de seleccion de cuadro
			//para indicarle que ha ingresado dinero
			if(ingresoCreditoModoDemo) {
				MenuPresentacion menuPresentacion = (MenuPresentacion)getState(Pantalla.PANTALLA_MENU_PRESENTACION);
				menuPresentacion.ingresoDinero();
			}
		}
		else if(comandoEvent.getComandoId() == Comando.COMANDO_CAMBIO_SWITCH_CONFIGURACION) {

			//flag que indica si el comando
			//fue enviado porque debemos 
			//entrar o salir de la configuracion
			boolean entradaMenuConfiguracion = ((Byte)comandoEvent.getGenerico()) == 1;

			//si el comando fue para entrar
			//a la configuracion, lo hacemos
			if(entradaMenuConfiguracion) {
				//entramos al menu de configuracion
				entrarMenuConfiguracion();
			}
		}
		else if(comandoEvent.getComandoId() == Comando.COMANDO_CAPACIDAD_PAGO_EXCEDIDA) {
			
			//mostramos el dialogo de 
			//monedas restantes de pago
			confirmacionCapacidadPagoExcedida.mostrarDialogo(maquina.montoJugador);
			
		}
	}
	
	/**
	 * Entra al menu de configuracion y 
	 * mantiene registro de la ultima 
	 * pantalla en la que se estuvo
	 */
	private void entrarMenuConfiguracion() {
		
		if (getCurrentStateID() == Pantalla.PANTALLA_JUEGO 
				|| 
			getCurrentStateID() == Pantalla.PANTALLA_MENU_PRESENTACION
				|| 
			getCurrentStateID() == Pantalla.PANTALLA_FICHA_PERSONAJE) {

			// obtenemos la pantalla
			// del menu principal
			MenuPrincipal menuPrincipal = (MenuPrincipal) getState(Pantalla.PANTALLA_MENU_PRINCIPAL);

			// si la pantalla actual es del
			// tipo que conocemos, obtenemos
			// el id para configurarlo como
			// la pantalla a la cual volver
			if (getCurrentState() instanceof PantallaGenerica) {

				// seteamos el id de la pantalla
				// desde la cual venimos para salir
				// correctamente a la pantalla adecuada
				menuPrincipal.pantallaAnterior = ((PantallaGenerica) getCurrentState()).getId();

			} 
			else {

				// no sabemos de que
				// pantalla venimos
				menuPrincipal.pantallaAnterior = -1;

			}

			// cambiamos al menu principal
			cambiarPantalla(Pantalla.PANTALLA_MENU_PRINCIPAL);
		}
	}
	
	
	public void renderizar(GameContainer gc, StateBasedGame sbg, Graphics g) throws SlickException {
		
		//renderizamos el jackpot solo si
		//estamos en el menu de presentacion
		//o en el juego
		if(getCurrentStateID() == Pantalla.PANTALLA_JUEGO || getCurrentStateID() == Pantalla.PANTALLA_MENU_PRESENTACION ||
		   getCurrentStateID() == Pantalla.PANTALLA_AYUDA_PREMIOS_NORMALES || getCurrentStateID() == Pantalla.PANTALLA_AYUDA_DESCRIPTIVA) {
			generalInfoModule.renderizar(g);
		}
		
		//confirmacion de pago excedida
		confirmacionCapacidadPagoExcedida.renderizar(g);

	}
	
	public void actualizar(GameContainer arg0, StateBasedGame arg1, int elapsedTime) throws SlickException {
		
		//actualizamos el modulo jackpot
		generalInfoModule.actualizar(elapsedTime);
		
		//actualizamos componente
		confirmacionCapacidadPagoExcedida.actualizar(elapsedTime);

	}
	
	/**
	 * Pura y exclusivamente usado durante la etapa de 
	 * desarrollo para simular envios al puerto serial
	 * y simulacion de ejecucion de logica remota
	 */
	public void keyPressed(int key, char c) {
		
		if(c == 'p') {
			//'p'ablo... no se, no 
			//se me ocurria otra
			ManejadorEscritorio manejadorEscritorio = ((Juego)getState(Pantalla.PANTALLA_JUEGO)).manejadorEscritorio;
			if(manejadorEscritorio.escritorioActivoId == ManejadorEscritorio.ESCRITORIO_SLOTS) {
				//TODO SACARME!!
				maquina.configuracion.jugadaGanadoraBonusMenorMayor.monto = 1;
				manejadorEscritorio.cambiarEscritorio(ManejadorEscritorio.ESCRITORIO_MINIJUEGO_MENOR_MAYOR);	
			}
			else {
				manejadorEscritorio.cambiarEscritorio(ManejadorEscritorio.ESCRITORIO_SLOTS);
			}
		}
	}
	
	public byte getEstado() {
		return estado;
	}

	public void setEstado(byte estado) {
		this.estado = estado;
	}

	public int getWidth() {
		return gameContainer.getWidth();
	}
	
	public int getHeight() {
		return gameContainer.getHeight();
	}
	
	/**
	 * Cambia el estado actual del juego al
	 * asignado por parametro
	 * 
	 * @param pantallaId Definido en la clase Pantalla
	 */
	public void cambiarPantalla(int pantallaId, Transition transicionSalida, Transition transicionEntrada) {
	
		//obtengo el id de la pantalla actual
		int pantallaActualId = getCurrentStateID();
		
		//recorro los listeners para ejecutar 
		//la salida de la pantalla actual
		for(PantallaListener pantallaListener : pantallaListeners) {
			if(pantallaListener.getId() == pantallaActualId) {
				pantallaListener.saleDePantalla();
				break;
			}
		}
		
		//recorro los listeners para ejecutar
		//la entrada a la nueva pantalla
		for(PantallaListener pantallaListener : pantallaListeners) {
			if(pantallaListener.getId() == pantallaId) {
				pantallaListener.entraEnPantalla();
				break;
			}
		}

		if(pantallaId == Pantalla.PANTALLA_JUEGO || pantallaId == Pantalla.PANTALLA_MENU_PRESENTACION ||
		  pantallaId == Pantalla.PANTALLA_TABLA_PAGOS || pantallaId == Pantalla.PANTALLA_AYUDA_PREMIOS_NORMALES ||
		  pantallaId == Pantalla.PANTALLA_AYUDA_DESCRIPTIVA) {
			
			//si nos encontramos en alguna de
			//esas pantallas estamos en modo juego
			modoConfiguracion = false;
		}
		else {
			modoConfiguracion = true;
		}
		
		//ejecuto el cambio de pantalla efectivamente
		enterState(pantallaId, transicionSalida, transicionEntrada);

	}
	
	public void cambiarPantalla(int pantallaId) {
		cambiarPantalla(pantallaId, new EmptyTransition(), new EmptyTransition());
	}
	
	/**
	 * Antes de terminar la aplicacion no
	 * encargamos de cerrar todos los 
	 * recursos necesarios, como la base
	 * de datos por ejemplo 
	 */
	public boolean closeRequested() {
		return super.closeRequested();
	}
}