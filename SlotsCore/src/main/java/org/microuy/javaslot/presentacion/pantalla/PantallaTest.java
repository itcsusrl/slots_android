package org.microuy.javaslot.presentacion.pantalla;

import org.microuy.javaslot.presentacion.Principal;
import org.microuy.javaslot.presentacion.constante.Pantalla;
import org.microuy.javaslot.presentacion.effects.ExplosionEffect;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.particles.ParticleSystem;
import org.newdawn.slick.particles.effects.FireEmitter;

public class PantallaTest extends PantallaGenerica {

	//private ParticleSystem particleSystem;
	private ExplosionEffect explosionEffect;
	
	public PantallaTest(Principal principal) {
		super(Pantalla.PANTALLA_TEST, principal);
	}

	protected void renderizar(Graphics g) {

		explosionEffect.render();
		//particleSystem.render(150,150);

	}

	protected void actualizar(long elapsedTime) {

		explosionEffect.update((int)elapsedTime);
		//particleSystem.update((int)elapsedTime);

	}
	
	protected void inicializarRecursos() {

		explosionEffect = new ExplosionEffect();
		explosionEffect.inicializar();
		
		/*
		particleSystem = new ParticleSystem("resources/figuras/banana.png");
		particleSystem.setVisible(true);
		particleSystem.addEmitter(new FireEmitter());
		//particleSystem.setBlendingMode(ParticleSystem.BLEND_ADDITIVE);
		particleSystem.setBlendingMode(ParticleSystem.BLEND_COMBINE);
		*/
		
	}

	public void botonPresionado(byte botonId) {
		
	}
	
	public void entraEnPantalla() {
		
	}

	public void saleDePantalla() {
		
	}
}