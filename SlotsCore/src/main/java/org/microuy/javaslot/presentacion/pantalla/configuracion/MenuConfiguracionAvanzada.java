package org.microuy.javaslot.presentacion.pantalla.configuracion;

import java.util.ArrayList;
import java.util.List;

import org.microuy.javaslot.constante.Boton;
import org.microuy.javaslot.presentacion.Principal;
import org.microuy.javaslot.presentacion.constante.Pantalla;
import org.microuy.javaslot.presentacion.dominio.EstadoBoton;
import org.microuy.javaslot.presentacion.modulo.ui.ButtonUI;
import org.microuy.javaslot.presentacion.modulo.ui.EnfocableIf;
import org.microuy.javaslot.presentacion.pantalla.PantallaGenerica;
import org.microuy.javaslot.presentacion.pantalla.listener.PantallaListener;
import org.newdawn.slick.Color;
import org.newdawn.slick.Graphics;

public class MenuConfiguracionAvanzada extends PantallaGenerica implements PantallaListener {

	private Color colorFondo = Color.lightGray;
	
	private ButtonUI configuracionGeneralButtonUI;
	private ButtonUI pozoAcumuladoButtonUI;
	private ButtonUI menorMayorButtonUI;

	/**
	 * Lista contenedora de elementos
	 * que pueden ser enfocables
	 */
	private List<EnfocableIf> enfocables = new ArrayList<EnfocableIf>();
	
	public MenuConfiguracionAvanzada(Principal principal) {
		super(Pantalla.PANTALLA_MENU_CONFIGURACION_AVANZADA, principal);
	}

	public void entraEnPantalla() {

	}

	public void saleDePantalla() {

	}
	
	private void presionaFlechaAbajo() {

		//buscamos el elemento
		//enfocado actualmente
		int proximoEnfocado = 0;
		for(int i=0; i<enfocables.size(); i++) {
			EnfocableIf enfocableIf = enfocables.get(i);
			if(enfocableIf.isEnfocado()) {
				proximoEnfocado = i+1;
				if(proximoEnfocado > (enfocables.size()-1)) {
					proximoEnfocado = 0;
				}
				enfocableIf.setEnfocado(false);
				break;
			}
		}
		
		//enfocamos el proximo elemento
		EnfocableIf enfocableIf = enfocables.get(proximoEnfocado);
		enfocableIf.setEnfocado(true);
		
		if(enfocableIf instanceof ButtonUI) {
			//reprocesamos el texto 
			//de la descripcion
			reprocesarTextoDescripcion(((ButtonUI)enfocableIf).descripcion);
		}
	}
	
	private void presionaFlechaArriba() {

		//buscamos el elemento
		//enfocado actualmente
		int anteriorEnfocado = 0;
		for(int i=0; i<enfocables.size(); i++) {
			EnfocableIf enfocableIf = enfocables.get(i);
			if(enfocableIf.isEnfocado()) {
				anteriorEnfocado = i-1;
				if(anteriorEnfocado < 0) {
					anteriorEnfocado = enfocables.size()-1;
				}
				enfocableIf.setEnfocado(false);
				break;
			}
		}
		
		//enfocamos el proximo elemento
		EnfocableIf enfocableIf = enfocables.get(anteriorEnfocado);
		enfocableIf.setEnfocado(true);
		
		if(enfocableIf instanceof ButtonUI) {
			//reprocesamos el texto 
			//de la descripcion
			reprocesarTextoDescripcion(((ButtonUI)enfocableIf).descripcion);
		}
	}

	protected void inicializarRecursos() {
		
		//definimos un titulo para 
		//esta pantalla
		titulo = "MENU PRINCIPAL";
		
		//creamos los componentes
		configuracionGeneralButtonUI = new ButtonUI(this, 360, 200, "Configuracion General", "Configuracion general de la maquina", true);
		pozoAcumuladoButtonUI = new ButtonUI(this, 400, 290, "Pozo Acumulado", "Configuracion del JACKPOT", false);
		menorMayorButtonUI = new ButtonUI(this, 350, 380, "Minijuego 'Menor Mayor'", "Configuracion del minijuego 'Menor o mayor'", false);

		//agregamos a la lista de
		//elementos enfocables
		enfocables.add(configuracionGeneralButtonUI);
		enfocables.add(pozoAcumuladoButtonUI);
		enfocables.add(menorMayorButtonUI);
		
		//configuramos el primer texto
		reprocesarTextoDescripcion(configuracionGeneralButtonUI.descripcion);
		
	}

	protected void renderizar(Graphics g) {
		
		//pintamos el fondo
		Color colorOriginal = g.getColor();
		g.setColor(colorFondo);
		g.fillRect(0, 0, principal.getWidth(), principal.getHeight());
		g.setColor(colorOriginal);
		
		//renderizamos cada componente
		configuracionGeneralButtonUI.renderizar(g);
		pozoAcumuladoButtonUI.renderizar(g);
		menorMayorButtonUI.renderizar(g);
		
	}

	protected void actualizar(long elapsedTime) {
	
		configuracionGeneralButtonUI.actualizar(elapsedTime);
		pozoAcumuladoButtonUI.actualizar(elapsedTime);
		menorMayorButtonUI.actualizar(elapsedTime);

	}

	protected void botonPresionado(byte botonId) {

		if(botonId == Boton.BOTON1) {
			principal.cambiarPantalla(Pantalla.PANTALLA_MENU_PRINCIPAL);
		}
		else if(botonId == Boton.BOTON3) {
			presionaFlechaArriba();
		}
		else if(botonId == Boton.BOTON4) {
			presionaFlechaAbajo();
		}
		else if(botonId == Boton.BOTON6) {
			
			//recorremos la lista de botones
			//para ver cual esta enfocado 
			for(EnfocableIf enfocableIf : enfocables) {
				
				//si el boton se encuentra
				//enfocado entramos a la 
				//pantalla
				if(enfocableIf.isEnfocado()) {
					
					if(enfocableIf == configuracionGeneralButtonUI) {
						principal.cambiarPantalla(Pantalla.PANTALLA_MENU_CONFIGURACION_BASICA);
					}
					else if(enfocableIf == pozoAcumuladoButtonUI) {
						principal.cambiarPantalla(Pantalla.PANTALLA_MENU_JACKPOT);
					}
					else if(enfocableIf == menorMayorButtonUI) {
						principal.cambiarPantalla(Pantalla.PANTALLA_MENU_MENOR_MAYOR);
					}
				}
			}
		}
	}
}