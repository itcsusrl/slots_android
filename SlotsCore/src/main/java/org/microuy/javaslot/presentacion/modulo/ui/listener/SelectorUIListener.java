package org.microuy.javaslot.presentacion.modulo.ui.listener;

import org.microuy.javaslot.presentacion.modulo.ui.SelectorUI;

public interface SelectorUIListener {

	public void cambiaValorSelectorUI(SelectorUI selectorUI);
	
}
