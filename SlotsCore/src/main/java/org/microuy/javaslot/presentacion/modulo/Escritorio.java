package org.microuy.javaslot.presentacion.modulo;

import java.util.ArrayList;
import java.util.List;

import org.microuy.javaslot.presentacion.modulo.listener.EscritorioListener;
import org.microuy.javaslot.presentacion.pantalla.PantallaGenerica;
import org.newdawn.slick.Graphics;

public class Escritorio extends ModuloGenerico implements EscritorioListener {

	private List<ComponenteEscritorio> componentes;
	private List<EscritorioListener> escritorioListeners;
	public int id;
	
	public Escritorio(PantallaGenerica pantallaGenerica, int id) {
		
		//la posicion es automaticamente
		//ajustada cuando el escritorio
		//se agrega al manejador
		super(pantallaGenerica, 0, 0, 0, 0);
		
		//asignamos id
		this.id = id;
		
		//creamos la coleccion  
		//de componentes
		componentes = new ArrayList<ComponenteEscritorio>();
		
		//listeners del escritorio
		escritorioListeners = new ArrayList<EscritorioListener>();
		
	}
	
	public void agregarComponente(ComponenteEscritorio componenteEscritorio) {
		
		componentes.add(componenteEscritorio);
		if(componenteEscritorio instanceof EscritorioListener) {
			escritorioListeners.add((EscritorioListener)componenteEscritorio);
		}
	}
	
	public void render(Graphics g) {

		//renderizamos cada componente
		for(ComponenteEscritorio componenteEscritorio : componentes) {
			componenteEscritorio.render(g);
		}
	}
	
	public void update(long elapsedTime) {
		
		//actualizamos cada componente
		for(ComponenteEscritorio componenteEscritorio : componentes) {

			//logica propia de los 
			//componentes del escritorio
			componenteEscritorio.update(elapsedTime);
		}
	}
	
	public void setX(int x) {
		
		//nuevo valor x
		this.x = x;
		
		//actualizamos cada componente
		for(ComponenteEscritorio componenteEscritorio : componentes) {

			//actualizacion de coordenadas
			componenteEscritorio.setX(x);
		}
	}
	
	public void inicializarRecursos() {
		
	}

	public void entraEscritorio() {
		for(EscritorioListener escritorioListener : escritorioListeners) {
			escritorioListener.entraEscritorio();
		}
	}

	public void saleEscritorio() {
		for(EscritorioListener escritorioListener : escritorioListeners) {
			escritorioListener.saleEscritorio();
		}
	}
}