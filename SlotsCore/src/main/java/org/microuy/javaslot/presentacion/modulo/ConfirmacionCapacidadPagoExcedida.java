package org.microuy.javaslot.presentacion.modulo;

import org.microuy.javaslot.comm.listener.BotonListener;
import org.microuy.javaslot.comm.listener.ComandoEvent;
import org.microuy.javaslot.comm.listener.ComandoListener;
import org.microuy.javaslot.constante.Boton;
import org.microuy.javaslot.constante.Comando;
import org.microuy.javaslot.persistencia.dao.ConfiguracionDAO;
import org.microuy.javaslot.presentacion.Principal;
import org.microuy.javaslot.presentacion.constante.Pantalla;
import org.newdawn.slick.Color;
import org.newdawn.slick.Graphics;

public class ConfirmacionCapacidadPagoExcedida extends ModuloGenerico implements BotonListener, ComandoListener {
	
	private static final int WIDTH = 600;
	private static final int HEIGHT = 400;
	private Color colorFondo = new Color(Color.lightGray.r, Color.lightGray.g, Color.lightGray.b, 0.95f);
//	private UnicodeFont fuenteTitulo = Principal.mediumLargeWhiteOutlineGameFont;
//	private UnicodeFont fuente = Principal.smallWhiteOutlineGameFont;
	
	//indica si el modulo debe
	//de ser activado. Se activa
	//solo cuando falta credito
	//para pagar al jugador
	public boolean activado;
	
	//estado de la llave que
	//habilita el teclado
	private boolean llaveActivada;
	
	//credito faltante para 
	//completar el pago
	private float creditoFaltante; 
	
	private String titulo = "CAPACIDAD DE PAGO EXCEDIDA";
	private int anchoTitulo;

	public ConfirmacionCapacidadPagoExcedida(Principal principal) {
		
		super(null, (principal.getWidth()/2)-(WIDTH/2), (principal.getHeight()/2)-(HEIGHT/2), WIDTH, HEIGHT);
		this.principal = principal;
		
		//calculamos el ancho
		//en pixels del titulo
//		anchoTitulo = fuenteTitulo.getWidth(titulo);
		
	}

	public void render(Graphics g) {
		
		if(activado) {
		
			//pintamos el fondo
			Color colorOriginal = g.getColor();
			g.setColor(colorFondo);
			g.fillRoundRect(x, y, width, height, 20);
			g.setColor(colorOriginal);
			
//			//renderizamos el titulo
//			fuenteTitulo.drawString((principal.getWidth()/2)-(anchoTitulo/2), y + 10, titulo);
//
//			//renderizamos el texto
//			fuente.drawString(x+70, y+100, "Por favor comuniquese con el encargado.");
//			fuente.drawString(x+100, y+150, "La maquina todavia le debe dinero");
//			fuente.drawString(x+90, y+190, "y no tiene mas monedas para pagar.");
//			Principal.mediumLargeWhiteOutlineGameFont.drawString(x+80, y+270, "MONTO A PAGAR: $U " + creditoFaltante);
			
		}
	}
	
	public void mostrarDialogo(float creditoFaltante) {
	
		//asignamos valor
		this.creditoFaltante = creditoFaltante;
		
		//entramos a modo de configuracion
		//para que los botones queden rojos
		//nuevamente
		Principal.modoConfiguracion = true;
		
		//cambiamos a activado
		activado = true;
		
	}
	
	public void update(long elapsedTime) {
	
	}

	public void inicializarRecursos() {
		
	}

	public void procesarBotonPresionado(byte botonId) {
		
		if(activado && llaveActivada) {
			
			if(botonId == Boton.BOTON6) {
				
				//configuro en memoria 
				//el nuevo monto del jugador
				principal.logicaMaquina.maquina.montoJugador = 0f;

				//cambia el estado a 'pagado'
				//en el ultimo registro de pago
				//excedido
				principal.logicaMaquina.cambiarEstadoUltimoPagoExcedido(true);
				
				//escribimos en la persistencia
				//el monto que no fue posible 
				//pagar
				principal.logicaMaquina.actualizarValorConfiguracion(ConfiguracionDAO.CAPACIDAD_PAGO_EXCEDIDA, String.valueOf("0"));
				
				if(principal.getCurrentStateID() != Pantalla.PANTALLA_MENU_PRESENTACION) {
					//nos vamos al menu de presentacion
					principal.cambiarPantalla(Pantalla.PANTALLA_MENU_PRESENTACION);
				}

				//reconfiguramos el credito
				//faltante como cero
				creditoFaltante = 0;
				
				//salimos de esta pantalla
				activado = false;
			}
		}
	}

	public void procesarEventoComando(ComandoEvent comandoEvent) {
		if(comandoEvent.getComandoId() == Comando.COMANDO_CAMBIO_SWITCH_LLAVE_ADMINISTRADOR) {
			if(activado) {
				//chequeamos el estado que
				//envia el comando
				boolean estadoComando = (((Byte)comandoEvent.getGenerico()) == 1);
				//cambiamos el flag
				llaveActivada = estadoComando ? true : false;
			}
		}
	}
}