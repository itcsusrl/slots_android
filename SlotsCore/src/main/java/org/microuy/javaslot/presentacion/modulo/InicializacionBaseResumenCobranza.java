package org.microuy.javaslot.presentacion.modulo;

import org.microuy.javaslot.constante.Boton;
import org.microuy.javaslot.presentacion.Principal;
import org.microuy.javaslot.presentacion.modulo.ui.TextSelectorUI;
import org.microuy.javaslot.presentacion.pantalla.configuracion.MenuResumenCobranza;
import org.newdawn.slick.Color;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.UnicodeFont;

public class InicializacionBaseResumenCobranza extends ModuloGenerico {

	/**
	 * Flag seteado para indicar que fue 
	 * ejecutado el cambio de modo de 
	 * expulsion de la moneda
	 */
	public boolean cambioModoExpulsionMonedaEjecutado;
	
	//indica si este modulo
	//debe de estar habilitado
	public boolean moduloHabilitado;

	//indica el estado del motor
	public boolean motorPrendido;
	
//	//referencia a la fuente
//	private UnicodeFont fuente = Principal.mediumLargeWhiteOutlineGameFont;
	
	//contador de monedas
	private TextSelectorUI contadorMonedasUI;

	//cantidad de monedas
	//contabilizadas
	private int cantidadMonedas;
	
	public InicializacionBaseResumenCobranza(MenuResumenCobranza menuResumenCobranza) {
		
		super(menuResumenCobranza, 0, 0, 0, 0);
		
		this.width = 600;
		this.height = 250;
		this.x = (principal.getWidth() / 2) - (width / 2);
		this.y = 240;
		
		
	}

	public void render(Graphics g) {
		
		if(moduloHabilitado) {

			//color previo
			Color colorPrevio = g.getColor();
			
			//configuramos el 
			//color de fondo
			g.setColor(Color.gray);
			
			//rellenamos con el color 
			//de fondo un rectangulo
			g.fillRoundRect(x, y, width, height, 20);

//			//titulo
//			fuente.drawString(350, 255, "INICIALIZACION BASE");

			//pintamos el borde
			g.setColor(Color.black);
			g.drawRoundRect(x, y, width, height, 20, 100);
			
			//volvemos al color anterior
			g.setColor(colorPrevio);
			
			//renderizamos el contador
			//de monedas
			contadorMonedasUI.render(g);
		}
	}

	public void update(long elapsedTime) {
		if(moduloHabilitado) {
			//actualizamos la logica del
			//componente grafico del contador 
			//de monedas
			contadorMonedasUI.update(elapsedTime);
		}
	}
	
	/**
	 * Habilita este modulo tanto
	 * para su logica como para 
	 * dibujarse en pantalla
	 */
	public void inicializarBase() {

		moduloHabilitado = true;
	}

	public void inicializarRecursos() {
		
		contadorMonedasUI = new TextSelectorUI(pantallaGenerica, 300, 360, 600, "Contador Monedas", "", true);
		contadorMonedasUI.agregarElemento(0, "0");

	}
	
	public void incrementarMoneda() {
		cambiarCantidadMonedas(cantidadMonedas+1);
	}
	
	public void decrementarMoneda() {
		if(cantidadMonedas > 0) {
			cambiarCantidadMonedas(cantidadMonedas-1);
		}
	}
	
	private void actualizarValorContabilizadorUI() {
		
		//borramos todos los elementos
		//de la lista
		contadorMonedasUI.borrarElementos();
		
		//agregamos el nuevo elemento
		contadorMonedasUI.agregarElemento(cantidadMonedas, String.valueOf(cantidadMonedas));
	}
	
	public void botonPresionado(byte botonId) {

		//prende o apaga el motor
		if(botonId == Boton.BOTON1) {
			
			//cambiamos el estado del motor
			principal.logicaMaquina.cambiarEstadoMotor(!motorPrendido);
		
			//obtenemos el estado del motor
			//para que se ejecute un callback
			//desde el PIC y se configure el 
			//estado del primer boton que es
			//el que indica el estado del motor
			principal.logicaMaquina.obtenerEstadoMotor();
			
		}
		else if(botonId == Boton.BOTON2) {
			decrementarMoneda();
		}
		else if(botonId == Boton.BOTON3) {
			incrementarMoneda();
		}
		else if(botonId == Boton.BOTON5) {

			//reseteamos las monedas 
			cambiarCantidadMonedas(0);
		}
		else if(botonId == Boton.BOTON6) {
			
			//finaliza en el resumen de 
			//cobranza la edicion del 
			//monto base
			((MenuResumenCobranza)pantallaGenerica).finalizaEdicionBaseCallback(cantidadMonedas);
			
			//reseteamos las monedas 
			cambiarCantidadMonedas(0);
			
			//deshabilitamos el modulo de
			//inicializacion de monto base
			moduloHabilitado = false;
			
			//volvemos al estado original para asi
			//habilitamos el cambio nuevamente cuando
			//volvamos a entrar a este modulo
			cambioModoExpulsionMonedaEjecutado = false;
			
			//apagamos el motor
			principal.logicaMaquina.cambiarEstadoMotor(false);

		}
	}
	
	private void cambiarCantidadMonedas(int cantidad) {
		
		cantidadMonedas = cantidad;
		actualizarValorContabilizadorUI();
	}
	
	/**
	 * Llamado cuando el PIC envia
	 * el comando indicando el estado
	 * del motor
	 * @param prendido
	 */
	public void actualizarEstadoMotor(boolean prendido) {
		
		//configura el nuevo valor
		motorPrendido = prendido;
	}
}