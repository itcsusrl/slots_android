package org.microuy.javaslot.presentacion.pantalla;

import org.microuy.javaslot.constante.Boton;
import org.microuy.javaslot.excepcion.SystemException;
import org.microuy.javaslot.presentacion.Principal;
import org.microuy.javaslot.presentacion.constante.Pantalla;
import org.microuy.javaslot.util.Timer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.Image;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.state.transition.FadeInTransition;
import org.newdawn.slick.state.transition.FadeOutTransition;

public class AyudaPremiosNormales extends PantallaGenerica {

	//el logo del juego
	private Image logoImage;
	
	//fondo
//	private Image fondoImage;
	
	//marco azul superior
	private Image marcoAzul;

	//tabla de pagos
//	private Image paytableImage;

//	//fuente de los premios
//	private UnicodeFont fuenteAzul = Principal.bigBlueskyOutlineGameFont;
//	private UnicodeFont fuenteBlanca = Principal.extraBigShadowOutlineGameFont;

	//timer usado durante el demo
	private Timer demoTimer = new Timer(15000);
//	private Timer demoTimer = new Timer(2000);

	public AyudaPremiosNormales(Principal principal) throws SlickException {
		super(Pantalla.PANTALLA_AYUDA_PREMIOS_NORMALES,principal);
	}

	protected void renderizar(Graphics g) {
		
		//fondo
//		g.drawImage(fondoImage, 0, 180);
		
//		g.drawImage(paytableImage, 0, 200);

		//renderizamos el logo
		g.drawImage(marcoAzul, 0, 50);

		//renderizamos el logo
		g.drawImage(logoImage, 30, 60);

//		fuenteAzul.drawString(700, 60, "AYUDA");
//		fuenteBlanca.drawString(610, 100, "tabla de pagos");
		
	}
	
	protected void actualizar(long elapsedTime) {
		
		//estamos en modo demo si
		//el el monto del jugador
		//es igual a cero
		if(principal.logicaMaquina.maquina.montoJugador == 0) {

			//si cumplimos el timer
			//volvemos al menu de 
			//presentacion
			if(demoTimer.action(elapsedTime)) {
				principal.cambiarPantalla(Pantalla.PANTALLA_MENU_PRESENTACION, new FadeOutTransition(), new FadeInTransition());
			}
		}
	}

	protected void inicializarRecursos() {
		
		try {
			
			//logo 
			logoImage = new Image("resources/LOGO_big-17.png").getScaledCopy(0.55f);
			
			//fondo
//			fondoImage = new Image("resources/help/HELP_BG.png");
			
			//marco azul superior
			marcoAzul = new Image("resources/marco-14.png");
			
			//tabla de pagos
//			paytableImage = new Image("resources/help/HELP_paytable.png");

			
		} catch (SlickException e) {
			throw new SystemException(e.getMessage());
		}
	}
	
	public void botonPresionado(byte botonId) {
		
		System.out.println("---> " + principal.demo);
		
		if(!principal.demo) {
			
			//volver
			if(botonId == Boton.BOTON1) {
				//volvemos al juego
				principal.cambiarPantalla(Pantalla.PANTALLA_JUEGO, new FadeOutTransition(), new FadeInTransition());
			}
			else if(botonId == Boton.BOTON5) { //tabla de pagos
				//volvemos al juego
				principal.cambiarPantalla(Pantalla.PANTALLA_AYUDA_PREMIOS_NORMALES, new FadeOutTransition(), new FadeInTransition());
			}
			else if(botonId == Boton.BOTON6) { //reglas
				//volvemos al juego
				principal.cambiarPantalla(Pantalla.PANTALLA_AYUDA_DESCRIPTIVA, new FadeOutTransition(), new FadeInTransition());
			}
		}
	}
	
	public void entraEnPantalla() {
		//refrescamos la info
		principal.mostrarInfo("TABLA DE PAGOS");
	}
	
	public void saleDePantalla() {
		
	}
}