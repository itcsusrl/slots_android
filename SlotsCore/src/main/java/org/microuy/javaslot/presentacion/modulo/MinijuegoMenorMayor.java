package org.microuy.javaslot.presentacion.modulo;

import java.text.DecimalFormat;

import org.microuy.javaslot.constante.Boton;
import org.microuy.javaslot.constante.Sistema;
import org.microuy.javaslot.dominio.util.Pair;
import org.microuy.javaslot.excepcion.SystemException;
import org.microuy.javaslot.presentacion.Principal;
import org.microuy.javaslot.presentacion.constante.Pantalla;
import org.microuy.javaslot.presentacion.modulo.listener.EscritorioListener;
import org.microuy.javaslot.presentacion.pantalla.Juego;
import org.microuy.javaslot.presentacion.pantalla.PantallaGenerica;
import org.microuy.javaslot.presentacion.util.CartaUtil;
import org.microuy.javaslot.sonido.ReproductorSonido;
import org.microuy.javaslot.textoAnimado.efectos.TextoItineranteEffect;
import org.microuy.javaslot.util.MathUtil;
import org.microuy.javaslot.util.Timer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.Image;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.UnicodeFont;

public class MinijuegoMenorMayor extends ModuloGenerico implements EscritorioListener {

	private DecimalFormat integerFormat;
	
	//flag para chequear si se
	//debe de ejecutar la logica
	private boolean ejecutaLogica;
	
	//fondo del minijuego
//	private Image fondo;
	
	//marco azul superior
	private Image marcoAzul;
	
	private TextoItineranteEffect jugandoPorTextoZoomin;
	private TextoItineranteEffect menorOMayorTextoZoomin;
	
	/**
	 * Coleccion con posiciones e
	 * imagenes de los lomos de
	 * carta y sus respectivas 
	 * posiciones
	 */
	private Image[] lomosCartas;
	private Pair[] posicionLomosCartas;
	
	/**
	 * Coleccion con posiciones e
	 * imagenes del mazo de cartas
	 * de la maquina acumuladas
	 */
	private Image[] maquinaCartas;
	private Pair[] posicionMaquinaCartas;
	
	/**
	 * Coleccion con posiciones e
	 * imagenes del mazo de cartas
	 * del jugador acumuladas
	 */
	private Image[] jugadorCartas;
	private Pair[] posicionJugadorCartas;
	
	//mazo de cartas
	private Image mazoCartas;
	
	//lomo de la carta
	private Image lomoCarta;
	
	//posicion final del 
	//mazo de cartas
	private int POSICION_FINAL_MAZO_X = 65;
	private int POSICION_FINAL_MAZO_Y = 205;
	
	//posicion final de la carta 
	//sacada del mazo por la maquina
	private int POSICION_FINAL_CARTA_MAQUINA_X = 360;
	private int POSICION_FINAL_CARTA_MAQUINA_Y = 205;
	
	//posicion final de la carta 
	//sacada del mazo por el jugador
	private int POSICION_FINAL_CARTA_JUGADOR_X = 670;
	private int POSICION_FINAL_CARTA_JUGADOR_Y = 205;
	
	//calculamos cual es la
	//traslacion en cada eje
	private int traslacionTotalCartaMazoY;
	private int traslacionTotalCartaMazoX;
	
	//calculamos cual es la
	//traslacion en cada eje
	private int traslacionTotalCartaMaquinaY;
	private int traslacionTotalCartaMaquinaX;

	//calculamos cual es la
	//traslacion en cada eje
	private int traslacionTotalCartaJugadorY;
	private int traslacionTotalCartaJugadorX;

	//timer de entrada de cada 
	//cartas al mazo
	private Timer timerEntradaCartaMazo;

	//contador de tiempo 
	private int contadorTiempoEntradaMazoLogica;
	
	//timer de traslacion de una 
	//carta del mazo a la seccion
	//de la carta de la maquina
	private Timer timerEntradaCartaMaquina;

	//contador de tiempo 
	private int contadorTiempoEntradaMaquinaLogica;

	//timer de traslacion de una 
	//carta del mazo a la seccion
	//de la carta del jugador
	private Timer timerEntradaCartaJugador;

	//contador de tiempo 
	private int contadorTiempoEntradaJugadorLogica;

	//la carta que esta 
	//entrando al mazo
	private int cartaEntradaMazoActual;
	
	//cuantas cartas van al mazo
	private final int CANTIDAD_CARTAS_MAZO = 20;
	
	//id de la carta actual 
	//de la maquina
	private int idCartaMaquina;
	
//	//fuente del texto
//	private UnicodeFont fuente;
//	private UnicodeFont fuenteGrande;
	
	//flag para saber si
	//el jugador fue ganador
	//de la ultima apuesta
	private boolean ganaApuesta;
	
	//timer usado para la animacion
	//del monto de la apuesta luego
	//de que se dio el resultaod
	private Timer timerMontoResultadoApuesta;
	
	//timer usado para la animacion de
	//la salida de las cartas
	private Timer timerSalidaCartas;
	private int salidaCartaMaquinaPosicionAnteriorX;
	private int salidaCartaJugadorPosicionAnteriorX;
	private int salidaCartaMaquinaTraslacionX;
	private int salidaCartaJugadorTraslacionX;

	/**
	 * Monto actual del minijuego.
	 * A partir de este monto se
	 * duplica tras cada jugada
	 * ganada por el jugador
	 */
	public float montoActual;
	public float jugandoPor;
	
	public MinijuegoMenorMayor(PantallaGenerica pantallaGenerica, int x, int y, int width, int height) {
		super(pantallaGenerica, x, y, width, height);
	}

	public void inicializarRecursos() {
		
		try {
			
			//creamos el fondo
//			fondo = new Image("resources/minigame_BG.jpg");

			//marco azul superior
			marcoAzul = new Image("resources/marco-14.png");
			
			//creamos el lomo 
			//de la carta
			lomoCarta = new Image("resources/cartas/cards-back.png");
			
			//mazo de cartas
			mazoCartas = new Image("resources/cartas/cards-deck.png");
			
			//creamos el mazo
			crearMazo();

			//calculamos la traslacion
			//de las cartas del mazo
			traslacionTotalCartaMazoX = (((Integer)posicionLomosCartas[0].clave) - POSICION_FINAL_MAZO_X - lomosCartas[0].getWidth());
			traslacionTotalCartaMazoY = principal.getHeight() - POSICION_FINAL_MAZO_Y;
			
			//calculamos la traslacion
			//de la salida de las cartas
			salidaCartaMaquinaTraslacionX = principal.getWidth() - POSICION_FINAL_CARTA_MAQUINA_X; 
			salidaCartaJugadorTraslacionX = principal.getWidth() - POSICION_FINAL_CARTA_JUGADOR_X;
			
//			//obtenemos la referencia
//			//a la fuente que usaremos
//			fuente = Principal.bigBlueskyOutlineGameFont;
//			fuenteGrande = Principal.superBigShadowOutlineGameFont;
			
			//creamos el formateador
			//de numeros
			integerFormat = new DecimalFormat("#");
			
			//cremos las animaciones
			//de texto
//			jugandoPorTextoZoomin = new TextoItineranteEffect(fuente, crearTextoJugandoPor(), 570, 60, 1500);
//			menorOMayorTextoZoomin = new TextoItineranteEffect(fuenteGrande, "�menor o mayor?", 470, 90, 1500);
            jugandoPorTextoZoomin = new TextoItineranteEffect(null, crearTextoJugandoPor(), 570, 60, 1500);
            menorOMayorTextoZoomin = new TextoItineranteEffect(null, "�menor o mayor?", 470, 90, 1500);

			//dejamos el texto fuera de la pantalla
			//para que no aparezca nada al principio
			jugandoPorTextoZoomin.modificadorX = principal.getWidth();
			menorOMayorTextoZoomin.modificadorX = principal.getWidth();

			//mantenemos renderizado para que
			//luego de que la animacion termine
			//no desaparezca
			jugandoPorTextoZoomin.setMantenerRenderizado(true);
			menorOMayorTextoZoomin.setMantenerRenderizado(true);
			
			//creamos el timer usado durante
			//la animacion del resultado de
			//la apuesta
			timerMontoResultadoApuesta = new Timer(2300);
			timerMontoResultadoApuesta.setActive(false);
			
			//creamos el timer usado durante
			//la animacion de la salida de 
			//las cartas
			timerSalidaCartas = new Timer(700);
			timerSalidaCartas.setActive(false);

		} catch (SlickException e) {
			throw new SystemException(e.getMessage());
		}
	}
	
	private String crearTextoJugandoPor() {
		
		String str = "jugando por " + integerFormat.format(MathUtil.roundTwoDecimals(Sistema.FACTOR_CONVERSION_MONEDA * jugandoPor));
		return str;
	}
	
	/**
	 * Crea todas las instancias 
	 * de imagenes usadas para
	 * entrar animadas
	 * @throws org.newdawn.slick.SlickException
	 */
	private void crearMazo() {

		//creamos las colecciones 
		//de imagenes y posiciones
		//para los lomos de carta
		lomosCartas = new Image[CANTIDAD_CARTAS_MAZO];
		posicionLomosCartas = new Pair[CANTIDAD_CARTAS_MAZO];
		
		//reseteamos todas las cartas
		//del mazo de la maquina
		maquinaCartas = new Image[(CANTIDAD_CARTAS_MAZO/2)];
		posicionMaquinaCartas = new Pair[(CANTIDAD_CARTAS_MAZO/2)];
		
		//reseteamos todas las cartas
		//del mazo del jugador
		jugadorCartas = new Image[(CANTIDAD_CARTAS_MAZO/2)];
		posicionJugadorCartas = new Pair[(CANTIDAD_CARTAS_MAZO/2)];
		
		//reiniciamos la posicion
		//de las cartas del mazo
		//para que queden fuera de
		//la vista del jugador
		reinicarPosicionMazo();
	}
	
	private void reinicarPosicionMazo() {

		//reiniciamos las cartas
		//del mazo de cartas de
		//lomo
		for(int i=0; i<CANTIDAD_CARTAS_MAZO; i++) {
			
			//creamos una copia con 
			//escala de la imagen
			lomosCartas[i] = lomoCarta.copy();
			
			//creamos la posicion
			//de la carta
			Pair posicionLomo = new Pair();
			posicionLomo.clave = (Integer)(principal.getWidth() / 2) - (lomosCartas[i].getWidth());
			posicionLomo.valor = (Integer)principal.getHeight();
			posicionLomosCartas[i] = posicionLomo;
		}
		
		//reiniciamos la coleccion de
		//cartas de la maquina
		for(int y=0; y<maquinaCartas.length; y++) {
			maquinaCartas[y] = null;
			posicionMaquinaCartas[y] = null;
		}

		//reiniciamos la coleccion de
		//cartas del jugador
		for(int y=0; y<jugadorCartas.length; y++) {
			jugadorCartas[y] = null;
			posicionJugadorCartas[y] = null;
		}
	}

	public void render(Graphics g) {
		
		//dibujamos el fondo
//		fondo.draw(x, y);

		//el marco
		marcoAzul.draw(0,57);

		//dibujamos el mazo
		mazoCartas.draw(x+20, y+220);
		
		//dibujamos cada carta del mazo
		for(int i=0; i<lomosCartas.length; i++) {
			if(lomosCartas[i] != null) {
				lomosCartas[i].draw(x+((Integer)(posicionLomosCartas[i].clave)), y+((Integer)(posicionLomosCartas[i].valor)));	
			}
		}
		
		//dibujamos cada carta 
		//de la maquina
		for(int i=0; i<maquinaCartas.length; i++) {
			if(maquinaCartas[i] != null) {
				maquinaCartas[i].draw(x+((Integer)(posicionMaquinaCartas[i].clave)), y+((Integer)(posicionMaquinaCartas[i].valor)));	
			}
		}

		//dibujamos cada carta 
		//del jugador
		for(int i=0; i<jugadorCartas.length; i++) {
			if(jugadorCartas[i] != null) {
				jugadorCartas[i].draw(x+((Integer)(posicionJugadorCartas[i].clave)), y+((Integer)(posicionJugadorCartas[i].valor)));	
			}
		}
		
		//renderizamos el texto 
		//de 'menor o mayor'
		jugandoPorTextoZoomin.modificadorX = x;
		jugandoPorTextoZoomin.modificadorY = y;
		jugandoPorTextoZoomin.render(g);
		
		menorOMayorTextoZoomin.modificadorX = x;
		menorOMayorTextoZoomin.modificadorY = y;
		menorOMayorTextoZoomin.render(g);
		
	}
	
	public void update(long elapsedTime) {
		
		if(ejecutaLogica) {
			
			if(timerEntradaCartaMazo.isActive()) { /** CORRESPONDE A CUANDO ENTRAN LAS CARTAS AL MAZO */
				
				if(timerEntradaCartaMazo.action(elapsedTime)) {
					
					//configuramos la posicion
					//final de la carta
					posicionLomosCartas[cartaEntradaMazoActual].clave = (Integer)POSICION_FINAL_MAZO_X;
					posicionLomosCartas[cartaEntradaMazoActual].valor = (Integer)POSICION_FINAL_MAZO_Y;
					
					//pasamos a la 
					//siguiente carta
					cartaEntradaMazoActual++;
					
					//reseteamos el contador al
					//tiempo total del timer
					contadorTiempoEntradaMazoLogica = (int)timerEntradaCartaMazo.getDelay();
					
					if(cartaEntradaMazoActual > (lomosCartas.length-1)) {
						
						//el timer del mazo lo
						//dejamos inactivo
						timerEntradaCartaMazo.setActive(false);
						
						//una vez terminadas de repartir
						//todas las cartas del mazo, comenzamos
						//la animacion para mostrar la primer
						//carta por la cual jugar
						animarEntradaCartaMaquina();
					}
				}
				else {
					
					//incrementamos el tiempo
					//de ejecucion del metodo
					contadorTiempoEntradaMazoLogica -= elapsedTime;
					
					int posicionActualCartaX = (int)((contadorTiempoEntradaMazoLogica * traslacionTotalCartaMazoX) / timerEntradaCartaMazo.getDelay());
					int posicionActualCartaY = (int)((contadorTiempoEntradaMazoLogica * traslacionTotalCartaMazoY) / timerEntradaCartaMazo.getDelay());
					
					posicionLomosCartas[cartaEntradaMazoActual].clave = posicionActualCartaX + POSICION_FINAL_MAZO_X;
					posicionLomosCartas[cartaEntradaMazoActual].valor = posicionActualCartaY + POSICION_FINAL_MAZO_Y;
					
				}
			}
			else if(timerEntradaCartaMaquina.isActive()) { /** CORRESPONDE A CUANDO ENTRA LA CARTA DE LA MAQUINA */
				
				//nos quedamos con la ultima carta
				//de la coleccion de cartas de la 
				//maquina que no sea nula, de esta
				//manera hacemos que solo se anime
				//la ultima, las anteriores ya se
				//tuvieron que haber animado
				int ultimaCartaMaquina = -1;
				for(int i=(posicionMaquinaCartas.length-1); i>=0; i--) {
					//chequeamos por la  
					//primera que no sea nula
					if(posicionMaquinaCartas[i] != null) {
						ultimaCartaMaquina = i;
						break;
					}
				}

				//la carta ya llego a su  
				//posicion final definida
				if(timerEntradaCartaMaquina.action(elapsedTime)) {
					
					//configuramos la posicion
					//final de la carta
					posicionMaquinaCartas[ultimaCartaMaquina].clave = (Integer)POSICION_FINAL_CARTA_MAQUINA_X;
					posicionMaquinaCartas[ultimaCartaMaquina].valor = (Integer)POSICION_FINAL_CARTA_MAQUINA_Y;

					//el timer de la carta
					//de la maquina lodejamos 
					//inactivo
					timerEntradaCartaMaquina.setActive(false);
					
					//pedimos a la logica que genere
					//un id de carta para la maquina
					idCartaMaquina = principal.logicaMaquina.generarCartaMaquinaMinijuegoMenorMayor();
					
					//cambiamos la carta del reverso
					//al frente de la carta
					maquinaCartas[ultimaCartaMaquina] = CartaUtil.obtenerImagenCartaPorId(idCartaMaquina);
					
					//comenzamos la animacion de
					//la aparicion de la carta
					//del jugador
					animarEntradaCartaJugador();
					
				}
				else {
					
					//incrementamos el tiempo
					//de ejecucion del metodo
					contadorTiempoEntradaMaquinaLogica += elapsedTime;
					
					int posicionActualCartaX = (int)((contadorTiempoEntradaMaquinaLogica * traslacionTotalCartaMaquinaX) / timerEntradaCartaMaquina.getDelay());
					int posicionActualCartaY = (int)((contadorTiempoEntradaMaquinaLogica * traslacionTotalCartaMaquinaY) / timerEntradaCartaMaquina.getDelay());

					//nueva posicion de la carta
					posicionMaquinaCartas[ultimaCartaMaquina].clave = posicionActualCartaX + POSICION_FINAL_MAZO_X;
					posicionMaquinaCartas[ultimaCartaMaquina].valor = posicionActualCartaY + POSICION_FINAL_MAZO_Y;
					
				}
			}
			else if(timerEntradaCartaJugador.isActive()) { /** CORRESPONDE A CUANDO ENTRA LA CARTA DEL JUGADOR */
				
				//nos quedamos con la ultima carta
				//de la coleccion de cartas del
				//jugador que no sea nula, de esta
				//manera hacemos que solo se anime
				//la ultima, las anteriores ya se
				//tuvieron que haber animado
				int ultimaCartaJugador = -1;
				for(int i=(posicionJugadorCartas.length-1); i>=0; i--) {
					//chequeamos por la  
					//primera que no sea nula
					if(posicionJugadorCartas[i] != null) {
						ultimaCartaJugador = i;
						break;
					}
				}

				//la carta ya llego a su  
				//posicion final definida
				if(timerEntradaCartaJugador.action(elapsedTime)) {
					
					//configuramos la posicion
					//final de la carta
					posicionJugadorCartas[ultimaCartaJugador].clave = (Integer)POSICION_FINAL_CARTA_JUGADOR_X;
					posicionJugadorCartas[ultimaCartaJugador].valor = (Integer)POSICION_FINAL_CARTA_JUGADOR_Y;

					//el timer de la carta
					//de la maquina lodejamos 
					//inactivo
					timerEntradaCartaJugador.setActive(false);
					
					//creamos el texto de 
					//jugando por
					jugandoPorTextoZoomin.cambiarTexto(false, crearTextoJugandoPor());
					menorOMayorTextoZoomin.cambiarTexto(false, "�menor o mayor?");
					
					//reiniciamos la animacion
					//de la pregunta menor mayor
					menorOMayorTextoZoomin.reiniciar();
					
					//dejamos que los textos se
					//sigan renderizando luego 
					//de completada su animacion
					jugandoPorTextoZoomin.setMantenerRenderizado(true);
					menorOMayorTextoZoomin.setMantenerRenderizado(true);

				}
				else {
					
					//incrementamos el tiempo
					//de ejecucion del metodo
					contadorTiempoEntradaJugadorLogica += elapsedTime;
					
					int posicionActualCartaX = (int)((contadorTiempoEntradaJugadorLogica * traslacionTotalCartaJugadorX) / timerEntradaCartaJugador.getDelay());
					int posicionActualCartaY = (int)((contadorTiempoEntradaJugadorLogica * traslacionTotalCartaJugadorY) / timerEntradaCartaJugador.getDelay());
					
					//nueva posicion de la carta
					posicionJugadorCartas[ultimaCartaJugador].clave = posicionActualCartaX + POSICION_FINAL_MAZO_X;
					posicionJugadorCartas[ultimaCartaJugador].valor = posicionActualCartaY + POSICION_FINAL_MAZO_Y;
					
				}
			}	
			
			//si debemos de animar el monto 
			//del resultado de la apuesta
			if(timerMontoResultadoApuesta.isActive()) {
				
				//si se cumplio el tiempo maximo
				//de la animacion de los creditos
				if(timerMontoResultadoApuesta.action(elapsedTime)) {

					if(ganaApuesta) {
						
						//actualizamos el 
						//monto actual
						montoActual = (float)MathUtil.roundTwoDecimals(jugandoPor);
						
						//duplicamos jugando por
						jugandoPor = (float)MathUtil.roundTwoDecimals(montoActual * 2f);
						
						//comienza la animacion de
						//la salida de las cartas
						animarSalidaCartas();
						
					}
					else {
						
						montoActual = 0f;
						jugandoPor = 0f;
						
						//obtenemos la pantalla de juego
						//para luego cambiar de escritorio
						Juego juego = ((Juego)principal.getState(Pantalla.PANTALLA_JUEGO));
						juego.cambiarEscritorio();
						
					}
					
					//termino el tiempo
					//de la animacion
					timerMontoResultadoApuesta.setActive(false);
					
				}
			}
			
			//si las cartas de la maquina y del
			//jugador deben de empezar a salir 
			if(timerSalidaCartas.isActive()) {
				
				//obtenemos la ultima carta
				//del jugador y de la maquina
				int ultimaCartaJugador = -1;
				for(int i=(posicionJugadorCartas.length-1); i>=0; i--) {
					//chequeamos por la  
					//primera que no sea nula
					if(posicionJugadorCartas[i] != null) {
						ultimaCartaJugador = i;
						break;
					}
				}
				
				//nos quedamos con la ultima carta
				//de la coleccion de cartas de la 
				//maquina que no sea nula, de esta
				//manera hacemos que solo se anime
				//la ultima, las anteriores ya se
				//tuvieron que haber animado
				int ultimaCartaMaquina = -1;
				for(int i=(posicionMaquinaCartas.length-1); i>=0; i--) {
					//chequeamos por la  
					//primera que no sea nula
					if(posicionMaquinaCartas[i] != null) {
						ultimaCartaMaquina = i;
						break;
					}
				}
				
				//animamos las cartas
				int nuevaPosicionCartaMaquinaX = (int)((timerSalidaCartas.getCurrentTick() * salidaCartaMaquinaTraslacionX) / timerSalidaCartas.getDelay());
				int nuevaPosicionCartaJugadorX = (int)((timerSalidaCartas.getCurrentTick() * salidaCartaJugadorTraslacionX) / timerSalidaCartas.getDelay());
				int diferenciaCartaMaquinaX = nuevaPosicionCartaMaquinaX - salidaCartaMaquinaPosicionAnteriorX;
				int diferenciaCartaJugadorX = nuevaPosicionCartaJugadorX - salidaCartaJugadorPosicionAnteriorX;
				salidaCartaMaquinaPosicionAnteriorX = nuevaPosicionCartaMaquinaX;
				salidaCartaJugadorPosicionAnteriorX = nuevaPosicionCartaJugadorX;

				//cambiamos las posiciones 
				//con la diferencia calculada
				posicionMaquinaCartas[ultimaCartaMaquina].clave = ((Integer)posicionMaquinaCartas[ultimaCartaMaquina].clave) + diferenciaCartaMaquinaX;
				posicionJugadorCartas[ultimaCartaJugador].clave = ((Integer)posicionJugadorCartas[ultimaCartaJugador].clave) + diferenciaCartaJugadorX;

				//si se cumplio el tiempo movemos 
				//las cartas totalmente fuera de la
				//pantalla
				if(timerSalidaCartas.action(elapsedTime)) {
					
					//movemos fuera de la 
					//pantalla las dos cartas
					posicionMaquinaCartas[ultimaCartaMaquina].clave = principal.getWidth();
					posicionJugadorCartas[ultimaCartaJugador].clave = principal.getWidth();
					
					//seteamos el timer como inactivo
					//para no seguir entrando aca
					timerSalidaCartas.setActive(false);
					
					//comenzamos una nueva mano
					animarEntradaCartaMaquina();
					
				}
			}
		}
		
		//renderizamos el texto 
		//de 'menor o mayor'
		jugandoPorTextoZoomin.update(elapsedTime);
		menorOMayorTextoZoomin.update(elapsedTime);

	}
	
	/**
	 * Configuracion de la animacion de las 
	 * cartas que entran desde fuera de 
	 * la pantalla hacia el mazo
	 */
	private void animarEntradaCartasMazo() {
	
		timerEntradaCartaMazo = new Timer(50);
		cartaEntradaMazoActual = 0;
		timerEntradaCartaMazo.setCurrentTick(0);
		timerEntradaCartaMazo.setActive(true);
		contadorTiempoEntradaMazoLogica = (int)timerEntradaCartaMazo.getDelay();
		
	}
	
	/**
	 * Configuracion de la animacion para cada
	 * carta de la maquina que sale del mazo
	 */
	private void animarEntradaCartaMaquina() {
		
		//cambiamos la referencia de la 
		//carta superior del mazo de lomos
		//hacia la coleccion de cartas de
		//la maquina
		for(int i=(CANTIDAD_CARTAS_MAZO-1), j=0; i>=0; i--, j++) {
			
			//si la imagen de la carta de 
			//lomo actual no es nula, 
			//movemos esa misma
			if(lomosCartas[i] != null) {
				
				//flag para terminar el
				//for en el que estamos
				boolean salir = false;
				
				//nos movemos por la lista
				//de imagenes de cartas de
				//la maquina, y la imagen 
				//del lomo de la carta en el
				//primer lugar donde el valor 
				//sea nulo
				for(int y=0; y<maquinaCartas.length; y++) {
					
					//si el elemento actual es
					//nulo, ponemos la carta de
					//lomo ahi
					if(maquinaCartas[y] == null) {
						
						//referenciamos la imagen
						//y la posicion en la nueva
						//coleccion
						maquinaCartas[y] = lomosCartas[i];
						posicionMaquinaCartas[y] = posicionLomosCartas[i];
						
						//dereferenciamos la imagen
						//y la posicion de la vieja
						//coleccion
						lomosCartas[i] = null;
						posicionLomosCartas[i] = null;
						
						//salimos del for
						salir = true;
						break;
					}
				}
				
				if(salir) {
					//salimos del for
					break;
				}
			}
		}

		//inicializamos variables
		timerEntradaCartaMaquina = new Timer(500);
		cartaEntradaMazoActual = 0;
		timerEntradaCartaMaquina.setCurrentTick(0);
		timerEntradaCartaMaquina.setActive(true);
		contadorTiempoEntradaMaquinaLogica = 0;
	
		//calculamos la traslacion
		//de las cartas de la maquina
		traslacionTotalCartaMaquinaX = POSICION_FINAL_CARTA_MAQUINA_X - ((Integer)posicionLomosCartas[0].clave);
		traslacionTotalCartaMaquinaY = POSICION_FINAL_CARTA_MAQUINA_Y - ((Integer)posicionLomosCartas[0].valor);

	}
	
	/**
	 * Configuracion de la animacion para cada
	 * carta del jugador que sale del mazo
	 */
	private void animarEntradaCartaJugador() {
		
		//cambiamos la referencia de la 
		//carta superior del mazo de lomos
		//hacia la coleccion de cartas de
		//la maquina
		for(int i=(CANTIDAD_CARTAS_MAZO-1), j=0; i>=0; i--, j++) {
			
			//si la imagen de la carta de 
			//lomo actual no es nula, 
			//movemos esa misma
			if(lomosCartas[i] != null) {
				
				//flag para terminar el
				//for en el que estamos
				boolean salir = false;
				
				//nos movemos por la lista
				//de imagenes de cartas de
				//la maquina, y la imagen 
				//del lomo de la carta en el
				//primer lugar donde el valor 
				//sea nulo
				for(int y=0; y<jugadorCartas.length; y++) {
					
					//si el elemento actual es
					//nulo, ponemos la carta de
					//lomo ahi
					if(jugadorCartas[y] == null) {
						
						//referenciamos la imagen
						//y la posicion en la nueva
						//coleccion
						jugadorCartas[y] = lomosCartas[i];
						posicionJugadorCartas[y] = posicionLomosCartas[i];
						
						//dereferenciamos la imagen
						//y la posicion de la vieja
						//coleccion
						lomosCartas[i] = null;
						posicionLomosCartas[i] = null;
						
						//salimos del for
						salir = true;
						break;
					}
				}
				
				if(salir) {
					//salimos del for
					break;
				}
			}
		}

		//inicializamos variables
		timerEntradaCartaJugador = new Timer(500);
		cartaEntradaMazoActual = 0;
		timerEntradaCartaJugador.setCurrentTick(0);
		timerEntradaCartaJugador.setActive(true);
		contadorTiempoEntradaJugadorLogica = 0;
		
		//calculamos la traslacion
		//de las cartas de la maquina
		traslacionTotalCartaJugadorX = POSICION_FINAL_CARTA_JUGADOR_X - ((Integer)posicionLomosCartas[0].clave);
		traslacionTotalCartaJugadorY = POSICION_FINAL_CARTA_JUGADOR_Y - ((Integer)posicionLomosCartas[0].valor);

	}
	
	/**
	 * Anima el resultado de la
	 * apuesta, tanto perdedora
	 * como ganadora. Las animaciones
	 * son de creditos y encendido
	 * apagado de texto
	 */
	private void animarResultadoApuesta() {
		
		//timer activo y en cero
		timerMontoResultadoApuesta.setActive(true);
		timerMontoResultadoApuesta.setCurrentTick(0);
		
	}
	
	/**
	 * Comienza la animacion de la
	 * salida de las cartas para dar
	 * lugar a una nueva mano
	 */
	private void animarSalidaCartas() {
		
		//inicializamos variables
		timerSalidaCartas.setCurrentTick(0);
		timerSalidaCartas.setActive(true);


		//reseteamos variables
		salidaCartaMaquinaPosicionAnteriorX = 0;
		salidaCartaJugadorPosicionAnteriorX = 0;
		
	}
	
	/**
	 * Finaliza las animaciones
	 * de los textos
	 */
	private void finalizarTextos() {
		
		//finalizamos los textos
		jugandoPorTextoZoomin.finalizar();
		menorOMayorTextoZoomin.finalizar();
		jugandoPorTextoZoomin.setMantenerRenderizado(false);
		menorOMayorTextoZoomin.setMantenerRenderizado(false);

	}

	/**
	 * Entra al escritorio encargado 
	 * de manejar este minijuego
	 */
	public void entraEscritorio() {

		//cargamos la musica
		ReproductorSonido.cargarSonidos(new int[]{ReproductorSonido.YEAH, ReproductorSonido.DINERO_GANADO});
		ReproductorSonido.stopMusic(ReproductorSonido.MUSICA_MINIJUEGO, false);
		ReproductorSonido.loopMusic(ReproductorSonido.MUSICA_MINIJUEGO, 0.4f);
		
		//creamos el mazo
		crearMazo();

		//el monto inicial es cero
		//porque todavia no gano nada
		montoActual = 0f;
				
		//se juega por el monto
		//apostado en los slots
		jugandoPor = (float)MathUtil.roundTwoDecimals(principal.maquina.configuracion.jugadaGanadoraBonusMenorMayor.monto);
		
		//animamos las cartas
		animarEntradaCartasMazo();
		ejecutaLogica = true;

		//inicializa el juego
		principal.maquina.logicaMaquina.inicializarMinijuegoMenorMayor();
		
	}

	/**
	 * Sale del escritorio encargado 
	 * de manejar este minijuego
	 */
	public void saleEscritorio() {
		
		//finalizamos el juego
		//en la logica de la maquina
		principal.logicaMaquina.reconfigurarVariablesMenorMayor();
		
		//la jugada ganadora de este 
		//juego es igualada a null
		((Juego)principal.getState(Pantalla.PANTALLA_JUEGO)).muestraJugadasGanadoras.jugadaLineaGanadoraBonusMenorMayor = null;
		
		//paramos la musica
		ReproductorSonido.stopAllMusic(true);
		
		ejecutaLogica = false;
		
		//finalizamos los
		//textos
		finalizarTextos();
		
		//reiniciamos la posicion de las cartas
		reinicarPosicionMazo();
		
	}
	
	/**
	 * Da vuelta la carta del jugador
	 * para ver su valor luego de la
	 * apuesta
	 */
	private void mostrarCartaJugador(int idCartaJugador) {
		
		//nos quedamos con la ultima carta
		//de la coleccion de cartas de la 
		//maquina que no sea nula, de esta
		//manera hacemos que solo se anime
		//la ultima, las anteriores ya se
		//tuvieron que haber animado
		int ultimaCartaJugador = -1;
		for(int i=(posicionJugadorCartas.length-1); i>=0; i--) {
			//chequeamos por la  
			//primera que no sea nula
			if(posicionJugadorCartas[i] != null) {
				ultimaCartaJugador = i;
				break;
			}
		}

		//cambiamos la carta del reverso
		//al frente de la carta
		jugadorCartas[ultimaCartaJugador] = CartaUtil.obtenerImagenCartaPorId(idCartaJugador);

	}
	
	public void botonPresionado(byte botonId) {

	}
}