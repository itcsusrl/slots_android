package org.microuy.javaslot.presentacion.pantalla.listener;

public interface BotonSinEstadoListener {

	public void botonPresionadoSinEstado(byte botonId);
	
}
