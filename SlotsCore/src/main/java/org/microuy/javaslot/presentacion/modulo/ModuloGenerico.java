package org.microuy.javaslot.presentacion.modulo;

import org.microuy.javaslot.presentacion.Principal;
import org.microuy.javaslot.presentacion.pantalla.PantallaGenerica;
import org.newdawn.slick.Graphics;

public abstract class ModuloGenerico implements ComponenteEscritorio {

	protected long elapsedTime;
	
	protected PantallaGenerica pantallaGenerica;
	protected Principal principal;
	
	public int x;
	public int y;
	public int width;
	public int height;
	
	public ModuloGenerico(PantallaGenerica pantallaGenerica, int x, int y, int width, int height) {
		
		this.pantallaGenerica = pantallaGenerica;
		
		if(pantallaGenerica != null) {
			principal = pantallaGenerica.principal;	
		}
		
		this.x = x;
		this.y = y;
		this.height = height;
		this.width = width;
		
		inicializarRecursos();
	}

	public void renderizar(Graphics g) {
		
		//renderizo la implementacion de este modulo
		render(g);
		
	}
	
	public void actualizar(long elapsedTime) {
	
		this.elapsedTime = elapsedTime;
		update(elapsedTime);
	}
	
	public abstract void render(Graphics g);
	
	public abstract void update(long elapsedTime);
	
	public abstract void inicializarRecursos();

	public void setPantallaGenerica(PantallaGenerica pantallaGenerica) {
		this.pantallaGenerica = pantallaGenerica;
	}

	public int getX() {
		return x;
	}

	public void setX(int x) {
		this.x = x;
	}

	public int getY() {
		return y;
	}

	public void setY(int y) {
		this.y = y;
	}

	public int getWidth() {
		return width;
	}

	public void setWidth(int width) {
		this.width = width;
	}

	public int getHeight() {
		return height;
	}

	public void setHeight(int height) {
		this.height = height;
	}
}