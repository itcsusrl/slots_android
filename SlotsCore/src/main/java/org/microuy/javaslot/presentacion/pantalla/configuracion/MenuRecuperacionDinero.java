package org.microuy.javaslot.presentacion.pantalla.configuracion;

import java.util.ArrayList;
import java.util.List;

import org.microuy.javaslot.constante.Boton;
import org.microuy.javaslot.persistencia.dao.ConfiguracionDAO;
import org.microuy.javaslot.presentacion.Principal;
import org.microuy.javaslot.presentacion.constante.Pantalla;
import org.microuy.javaslot.presentacion.dominio.EstadoBoton;
import org.microuy.javaslot.presentacion.pantalla.MenuPresentacion;
import org.microuy.javaslot.presentacion.pantalla.PantallaGenerica;
import org.microuy.javaslot.presentacion.pantalla.listener.PantallaListener;
import org.newdawn.slick.Color;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.UnicodeFont;

public class MenuRecuperacionDinero extends PantallaGenerica implements PantallaListener {

	private Color colorFondo = Color.lightGray;
//	private UnicodeFont fuente = Principal.mediumWhiteOutlineGameFont;
//	private UnicodeFont fuenteGrande = Principal.bigWhiteOutlineGameFont;

	//obtenemos el monto a 
	//recuperar del jugador
	private float montoRecuperacion;
	
	public MenuRecuperacionDinero(Principal principal) {
		super(Pantalla.PANTALLA_MENU_RECUPERACION_DINERO, principal);
	}

	public void entraEnPantalla() {

		//obtenemos el monto del
		//jugador a recuperar
		montoRecuperacion = Float.parseFloat(principal.maquina.logicaMaquina.obtenerValorConfiguracion(ConfiguracionDAO.MONTO_JUGADOR));
		
	}

	public void saleDePantalla() {

	}
	
	protected void inicializarRecursos() {
		
		//definimos un titulo para 
		//esta pantalla
		titulo = "MENU RECUPERACION DINERO";
		
	}

	protected void renderizar(Graphics g) {
		
		//pintamos el fondo
		Color colorOriginal = g.getColor();
		g.setColor(colorFondo);
		g.fillRect(0, 0, principal.getWidth(), principal.getHeight());
		g.setColor(colorOriginal);
		
//		//escribimos el mensaje en pantalla
//		fuente.drawString(100, 200, "Esta maquina ha sido apagada mientras");
//		fuente.drawString(100, 240, "el jugador estaba jugando.");
//		fuente.drawString(100, 280, "El monto a recuperar es de $U " + montoRecuperacion);
//		fuenteGrande.drawString(100, 360, "Si desea recuperar el monto");
//		fuenteGrande.drawString(100, 400, "presione ASIGNAR MONTO");
//		fuenteGrande.drawString(100, 480, "Si NO desea recuperar el credito");
//		fuenteGrande.drawString(100, 520, "presione VOLVER");
		
	}

	protected void actualizar(long elapsedTime) {
	
	}

	protected void botonPresionado(byte botonId) {

		if(botonId == Boton.BOTON1) {
			
			//actualizamos el valor en 
			//la persistencia
			principal.logicaMaquina.actualizarValorConfiguracion(ConfiguracionDAO.MONTO_PREMIOS_SESION_JUGADOR, "0");
			principal.logicaMaquina.actualizarValorConfiguracion(ConfiguracionDAO.MONTO_APOSTADO_SESION_JUGADOR, "0");

			//restamos al monto actual 
			//del jugador el monto de
			//recuperacion	
			principal.maquina.actualizarMontoJugador(0f);

			//el usuario no desea 
			//recuperar el dinero 
			principal.cambiarPantalla(Pantalla.PANTALLA_MENU_PRINCIPAL);
		}
		else if(botonId == Boton.BOTON6) {
			
			//configuramos el monto 
			//nuevamente al jugador
			principal.maquina.actualizarMontoJugador(montoRecuperacion);

			//continuamos hacia el juego
			((MenuPresentacion)principal.getState(Pantalla.PANTALLA_MENU_PRESENTACION)).continuarHaciaJuego(false, false);

		}
	}
}