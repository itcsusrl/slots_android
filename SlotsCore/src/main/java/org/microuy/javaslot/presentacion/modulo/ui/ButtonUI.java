package org.microuy.javaslot.presentacion.modulo.ui;

import org.microuy.javaslot.presentacion.Principal;
import org.microuy.javaslot.presentacion.modulo.ModuloGenerico;
import org.microuy.javaslot.presentacion.pantalla.PantallaGenerica;
import org.microuy.javaslot.util.Timer;
import org.newdawn.slick.Color;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.UnicodeFont;

public class ButtonUI extends ModuloGenerico implements EnfocableIf {

	public boolean enfocado;
	private Color colorAnimado;
	private Color colorFuente = Color.white;
	private Color colorFuenteEnfocado = Color.red;
	public String texto;
	public String descripcion;
	
	private Timer animacionColorTimer;
	private boolean direccionAnimacionIncrementa;
	private int anchoTexto;
	private int altoTexto;
	
//	public static UnicodeFont fuente = Principal.bigWhiteOutlineGameFont;
	
	public ButtonUI(PantallaGenerica pantallaGenerica, int x, int y, String texto, String descripcion, boolean enfocado) {
	
//		super(pantallaGenerica, x, y, 0, fuente.getHeight("A")+4);
        super(pantallaGenerica, x, y, 0, 10+4);
		this.enfocado = enfocado;
		this.texto = texto;
		this.descripcion = descripcion;
		
		//calculamos el ancho y
		//el alto del texto
//		anchoTexto = fuente.getWidth(texto);
//		altoTexto = fuente.getHeight(texto);

        anchoTexto = 50;
        altoTexto = 10;


    }

	public void setTexto(String texto) {

		this.texto = texto;

		//recalculamos el ancho
		//y el alto del texto
//		anchoTexto = fuente.getWidth(texto);
//		altoTexto = fuente.getHeight(texto);

        anchoTexto = 50;
        altoTexto = 10;


    }

	public void setColorFuente(Color colorFuente) {
		this.colorFuente = colorFuente;
	}

	public void render(Graphics g) {

		//dibujamos el fondo
		g.setColor(enfocado ? colorAnimado : Color.gray);
		g.fillRoundRect(x, y, ((float)anchoTexto * 1.15f), ((float)altoTexto * 1.5f), 10);
		
		//dibujamos el texto del boton
//		fuente.drawString(x + (anchoTexto * 0.15f) / 2 , y + (altoTexto * 0.5f) / 2, texto, enfocado ? colorFuenteEnfocado : colorFuente);

	}

	public void update(long elapsedTime) {

		//si el boton se encuentra enfocado
		//realizamos la animacion del color
		//de fondo
		if(enfocado) {
			
			//realizamos el cambio de color 
			//para la animacion del color 
			//de fondo
			if(animacionColorTimer.action(elapsedTime)) {

				if(colorAnimado.b <= 0.15) {
					direccionAnimacionIncrementa = true;
				}
				else if(colorAnimado.b >= 0.55) {
					direccionAnimacionIncrementa = false;
				}
				
				float montoOperacion = 0.025f;
				if(direccionAnimacionIncrementa) {
					colorAnimado.b += montoOperacion;
					colorAnimado.g += montoOperacion;
					colorAnimado.r += montoOperacion;
				}
				else {
					colorAnimado.b -= montoOperacion;
					colorAnimado.g -= montoOperacion;
					colorAnimado.r -= montoOperacion;
				}
			}
		}
	}
	
	public void inicializarRecursos() {
		
		//creamos el timer de la animacion
		animacionColorTimer = new Timer(20);
		
		//creamos el color base 
		//de la animacion
		colorAnimado = new Color(Color.gray);
		
	}

	public boolean isEnfocado() {
		return enfocado;
	}

	public void setEnfocado(boolean enfocado) {
		this.enfocado = enfocado;
	}
}