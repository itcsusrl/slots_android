package org.microuy.javaslot.presentacion.util;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import org.microuy.javaslot.excepcion.SystemException;
import org.newdawn.slick.Image;
import org.newdawn.slick.SlickException;

public class CartaUtil {

	private static Map<Integer, String> rutaImagenes = new HashMap<Integer, String>();
	private static Map<Integer, SlotsImage> imagenes = new HashMap<Integer, SlotsImage>();
	private static Map<Integer, SlotsAnimation> animaciones = new HashMap<Integer, SlotsAnimation>();

	/**
	 * Cargamos todos los recursos graficos
	 * relacionados a las cartas de la 
	 * maquina. Entre recursos consideramos
	 * las imagenes estaticas y las animaciones
	 * con todos sus respectivos cuadros que
	 * las conforman.
	 */
	static {
		
		try {

			rutaImagenes.put(1, "resources/cartas/cards-01.png");
			rutaImagenes.put(2, "resources/cartas/cards-02.png");
			rutaImagenes.put(3, "resources/cartas/cards-03.png");
			rutaImagenes.put(4, "resources/cartas/cards-04.png");
			rutaImagenes.put(5, "resources/cartas/cards-05.png");
			rutaImagenes.put(6, "resources/cartas/cards-06.png");
			rutaImagenes.put(7, "resources/cartas/cards-07.png");
			rutaImagenes.put(8, "resources/cartas/cards-08.png");
			rutaImagenes.put(9, "resources/cartas/cards-09.png");
			rutaImagenes.put(10, "resources/cartas/cards-10.png");
			rutaImagenes.put(11, "resources/cartas/cards-11.png");
			rutaImagenes.put(12, "resources/cartas/cards-12.png");
			
			/**
			 * Cargamos imagenes
			 */
			Iterator<Integer> ids = rutaImagenes.keySet().iterator();
			while(ids.hasNext()) {
				
				//obtenemos el id de
				//carta actual
				Integer idCartaActual = ids.next();
				
				//obtenemos el string correspondiente
				//a la ruta de la carta actual
				String rutaCartaActual = rutaImagenes.get(idCartaActual);
				
				/**
				 * Cargamos la imagen actual
				 */
				imagenes.put(idCartaActual, new SlotsImage(idCartaActual, rutaCartaActual));
				
			}
			
			/**
			 * Cargamos animaciones
			 */
			animaciones.put(1, new SlotsAnimation(1, "resources/cartas/carta-01.png", new Image[]{new Image("resources/cartas/cards-01.png")}, 1));
			animaciones.put(2, new SlotsAnimation(2, "resources/cartas/carta-02.png", new Image[]{new Image("resources/cartas/cards-02.png")}, 1));
			animaciones.put(3, new SlotsAnimation(3, "resources/cartas/carta-03.png", new Image[]{new Image("resources/cartas/cards-03.png")}, 1));
			animaciones.put(4, new SlotsAnimation(4, "resources/cartas/carta-04.png", new Image[]{new Image("resources/cartas/cards-04.png")}, 1));
			animaciones.put(5, new SlotsAnimation(5, "resources/cartas/carta-05.png", new Image[]{new Image("resources/cartas/cards-05.png")}, 1));
			animaciones.put(6, new SlotsAnimation(6, "resources/cartas/carta-06.png", new Image[]{new Image("resources/cartas/cards-06.png")}, 1));
			animaciones.put(7, new SlotsAnimation(7, "resources/cartas/carta-07.png", new Image[]{new Image("resources/cartas/cards-07.png")}, 1));
			animaciones.put(8, new SlotsAnimation(8, "resources/cartas/carta-08.png", new Image[]{new Image("resources/cartas/cards-08.png")}, 1));
			animaciones.put(9, new SlotsAnimation(9, "resources/cartas/carta-09.png", new Image[]{new Image("resources/cartas/cards-09.png")}, 1));
			animaciones.put(10, new SlotsAnimation(10, "resources/cartas/carta-10.png", new Image[]{new Image("resources/cartas/cards-10.png")}, 1));
			animaciones.put(11, new SlotsAnimation(11, "resources/cartas/carta-11.png", new Image[]{new Image("resources/cartas/cards-11.png")}, 1));
			animaciones.put(12, new SlotsAnimation(12, "resources/cartas/carta-12.png", new Image[]{new Image("resources/cartas/cards-12.png")}, 1));
			
		} catch (SlickException e) {
			throw new SystemException(e.getMessage());
		}
	}

	public static SlotsAnimation obtenerAnimacionCartaPorId(int id) {
		return animaciones.get(id);
	}

	public static SlotsImage obtenerImagenCartaPorId(int id) {
		return imagenes.get(id);
	}
	
	public static String obtenerRutaImagenCartaPorId(int id) {
		return rutaImagenes.get(id);
	}
}