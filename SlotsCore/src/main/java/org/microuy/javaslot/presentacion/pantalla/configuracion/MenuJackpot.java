package org.microuy.javaslot.presentacion.pantalla.configuracion;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;

import org.microuy.javaslot.constante.Boton;
import org.microuy.javaslot.constante.Sistema;
import org.microuy.javaslot.persistencia.dao.ConfiguracionDAO;
import org.microuy.javaslot.presentacion.Principal;
import org.microuy.javaslot.presentacion.constante.Pantalla;
import org.microuy.javaslot.presentacion.dominio.EstadoBoton;
import org.microuy.javaslot.presentacion.modulo.ui.ButtonUI;
import org.microuy.javaslot.presentacion.modulo.ui.EnfocableIf;
import org.microuy.javaslot.presentacion.modulo.ui.SelectorUI;
import org.microuy.javaslot.presentacion.modulo.ui.TextSelectorUI;
import org.microuy.javaslot.presentacion.modulo.ui.listener.SelectorUIListener;
import org.microuy.javaslot.presentacion.pantalla.PantallaGenerica;
import org.microuy.javaslot.presentacion.pantalla.listener.PantallaListener;
import org.newdawn.slick.Color;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.UnicodeFont;

public class MenuJackpot extends PantallaGenerica implements PantallaListener, SelectorUIListener {

	private Color colorFondo = Color.lightGray;
	private TextSelectorUI montoMinimoTextSelectorUI;
	private TextSelectorUI porcentajeExtraidoApuestaTextSelectorUI;
	private TextSelectorUI probabilidadJugadorTextSelectorUI;
	private TextSelectorUI aperturaPozoTextSelectorUI;
	private ButtonUI restablecerMontoActualButtonUI;
	
	private DecimalFormat decimalFormat = new DecimalFormat("#.##");

	//fuente de esta pantalla
//	private UnicodeFont fuente = Principal.mediumWhiteOutlineGameFont;
	
	/**
	 * Lista contenedora de elementos
	 * que pueden ser enfocables
	 */
	private List<EnfocableIf> enfocables = new ArrayList<EnfocableIf>();
	
	public MenuJackpot(Principal principal) {
		super(Pantalla.PANTALLA_MENU_JACKPOT, principal);
	}

	public void entraEnPantalla() {

	}

	public void saleDePantalla() {

	}
	
	/**
	 * Obtiene el componente enfocado actualmente
	 * @return
	 */
	private EnfocableIf obtenerEnfocado() {
		
		EnfocableIf enfocableIf = null;
		
		for(EnfocableIf enIf : enfocables) {
			if(enIf.isEnfocado()) {
				enfocableIf = enIf;
				break;
			}
		}
		
		return enfocableIf;
	}
	
	private void presionaFlechaAbajo() {

		//buscamos el elemento
		//enfocado actualmente
		int proximoEnfocado = 0;
		for(int i=0; i<enfocables.size(); i++) {
			EnfocableIf enfocableIf = enfocables.get(i);
			if(enfocableIf.isEnfocado()) {
				proximoEnfocado = i+1;
				if(proximoEnfocado > (enfocables.size()-1)) {
					proximoEnfocado = 0;
				}
				enfocableIf.setEnfocado(false);
				break;
			}
		}
		
		//enfocamos el proximo elemento
		EnfocableIf enfocableIf = enfocables.get(proximoEnfocado);
		enfocableIf.setEnfocado(true);
		
		if(enfocableIf instanceof TextSelectorUI) {
			//reprocesamos el texto 
			//de la descripcion
			reprocesarTextoDescripcion(((TextSelectorUI)enfocableIf).descripcion);
		}
		else if(enfocableIf instanceof ButtonUI) {
			//reprocesamos el texto 
			//de la descripcion
			reprocesarTextoDescripcion(((ButtonUI)enfocableIf).descripcion);
		}
	}
	
	private void presionaFlechaArriba() {

		//buscamos el elemento
		//enfocado actualmente
		int anteriorEnfocado = 0;
		for(int i=0; i<enfocables.size(); i++) {
			EnfocableIf enfocableIf = enfocables.get(i);
			if(enfocableIf.isEnfocado()) {
				anteriorEnfocado = i-1;
				if(anteriorEnfocado < 0) {
					anteriorEnfocado = enfocables.size()-1;
				}
				enfocableIf.setEnfocado(false);
				break;
			}
		}
		
		//enfocamos el proximo elemento
		EnfocableIf enfocableIf = enfocables.get(anteriorEnfocado);
		enfocableIf.setEnfocado(true);
		
		if(enfocableIf instanceof TextSelectorUI) {
			//reprocesamos el texto 
			//de la descripcion
			reprocesarTextoDescripcion(((TextSelectorUI)enfocableIf).descripcion);
		}
		else if(enfocableIf instanceof ButtonUI) {
			//reprocesamos el texto 
			//de la descripcion
			reprocesarTextoDescripcion(((ButtonUI)enfocableIf).descripcion);
		}
	}

	private void presionaFlechaIzquierda() {
		
		for(EnfocableIf enfocableIf : enfocables) {
			if(enfocableIf.isEnfocado()) {
				if(enfocableIf instanceof TextSelectorUI) {
					TextSelectorUI textSelectorUI = (TextSelectorUI)enfocableIf;
					textSelectorUI.proximoElemento();
					break;
				}
			}
		}
	}

	private void presionaFlechaDerecha() {
		
		for(EnfocableIf enfocableIf : enfocables) {
			if(enfocableIf.isEnfocado()) {
				if(enfocableIf instanceof TextSelectorUI) {
					TextSelectorUI textSelectorUI = (TextSelectorUI)enfocableIf;
					textSelectorUI.previoElemento();
					break;
				}
			}
		}
	}

	protected void inicializarRecursos() {
		
		//definimos un titulo para 
		//esta pantalla
		titulo = "CONFIGURACION JACKPOT";
		
		//creamos los componentes
		montoMinimoTextSelectorUI = new TextSelectorUI(this, 130, 150, 550,  "Monto Minimo", ConfiguracionDAO.JACKPOT_MONTO_MINIMO, true);
		montoMinimoTextSelectorUI.agregarSelectorUIListener(this);
		montoMinimoTextSelectorUI.agregarElemento(100, "100");
		montoMinimoTextSelectorUI.agregarElemento(1000, "1.000");
		montoMinimoTextSelectorUI.agregarElemento(2000, "2.000");
		montoMinimoTextSelectorUI.agregarElemento(5000, "5.000");
		montoMinimoTextSelectorUI.agregarElemento(10000, "10.000");
		montoMinimoTextSelectorUI.agregarElemento(15000, "15.000");
		montoMinimoTextSelectorUI.agregarElemento(20000, "20.000");
		montoMinimoTextSelectorUI.agregarElemento(30000, "30.000");
		montoMinimoTextSelectorUI.agregarElemento(50000, "50.000");
		montoMinimoTextSelectorUI.agregarElemento(100000, "100.000");
		montoMinimoTextSelectorUI.mostrarClavePorDefecto(Sistema.JACKPOT_MONTO_MINIMO);
		//cargamos la descripcion del  
		//componente desde la base de datos
		montoMinimoTextSelectorUI.descripcion = principal.logicaMaquina.obtenerDescripcionConfiguracion(montoMinimoTextSelectorUI.selectorUI.codigo);
		
		porcentajeExtraidoApuestaTextSelectorUI = new TextSelectorUI(this, 130, 190, 550,  "% Extraido Apuesta", ConfiguracionDAO.JACKPOT_PORCENTAJE_EXTRAIDO_APUESTA, false);
		porcentajeExtraidoApuestaTextSelectorUI.agregarSelectorUIListener(this);
		
		//divisor usado para el porcentaje
		//de extraccion de la apuesta
		float porcentajeExtraidoDivisor = 0.5f;
		//repeticiones del valor anterior 
		int repeticionesPorcentajeExtraido = (int)(100f / porcentajeExtraidoDivisor);
		
		//cargamos los valores del TextSelector
		for(int i=1; i<=repeticionesPorcentajeExtraido; i++) {
			float tmp = porcentajeExtraidoDivisor * (float)i;
			porcentajeExtraidoApuestaTextSelectorUI.agregarElemento(tmp, ""+tmp);
		}
		
		porcentajeExtraidoApuestaTextSelectorUI.mostrarClavePorDefecto(Sistema.JACKPOT_PORCENTAJE_EXTRAIDO_APUESTA);
		//cargamos la descripcion del  
		//componente desde la base de datos
		porcentajeExtraidoApuestaTextSelectorUI.descripcion = principal.logicaMaquina.obtenerDescripcionConfiguracion(porcentajeExtraidoApuestaTextSelectorUI.selectorUI.codigo);
		
		probabilidadJugadorTextSelectorUI = new TextSelectorUI(this, 130, 230, 550,  "Probabilidades Jugador", ConfiguracionDAO.JACKPOT_PROBABILIDADES_JUGADOR, false);
		probabilidadJugadorTextSelectorUI.agregarSelectorUIListener(this);
		probabilidadJugadorTextSelectorUI.agregarElemento(100, "1/1");
		probabilidadJugadorTextSelectorUI.agregarElemento(50, "1/2");
		probabilidadJugadorTextSelectorUI.agregarElemento(20, "1/5");
		probabilidadJugadorTextSelectorUI.agregarElemento(10, "1/10");
		probabilidadJugadorTextSelectorUI.agregarElemento(2, "1/50");
		probabilidadJugadorTextSelectorUI.agregarElemento(1, "1/100");
		probabilidadJugadorTextSelectorUI.agregarElemento(0.8, "1/125");
		probabilidadJugadorTextSelectorUI.agregarElemento(0.4, "1/250");
		probabilidadJugadorTextSelectorUI.agregarElemento(0.2, "1/500");
		probabilidadJugadorTextSelectorUI.agregarElemento(0.1, "1/1.000");
		probabilidadJugadorTextSelectorUI.agregarElemento(0.01, "1/10.000");
		
		probabilidadJugadorTextSelectorUI.mostrarClavePorDefecto(Sistema.JACKPOT_PROBABILIDADES_JUGADOR);
		//cargamos la descripcion del  
		//componente desde la base de datos
		probabilidadJugadorTextSelectorUI.descripcion = principal.logicaMaquina.obtenerDescripcionConfiguracion(probabilidadJugadorTextSelectorUI.selectorUI.codigo);

		aperturaPozoTextSelectorUI = new TextSelectorUI(this, 130, 270, 550,  "Apertura Pozo", ConfiguracionDAO.JACKPOT_APERTURA_POZO, false);
		aperturaPozoTextSelectorUI.descripcion = principal.logicaMaquina.obtenerDescripcionConfiguracion(aperturaPozoTextSelectorUI.selectorUI.codigo);
		aperturaPozoTextSelectorUI.agregarSelectorUIListener(this);
		aperturaPozoTextSelectorUI.agregarElemento(0f, "0");
		aperturaPozoTextSelectorUI.agregarElemento(1f, "1");
		aperturaPozoTextSelectorUI.agregarElemento(2f, "2");
		aperturaPozoTextSelectorUI.agregarElemento(3f, "3");
		aperturaPozoTextSelectorUI.agregarElemento(4f, "4");
		aperturaPozoTextSelectorUI.agregarElemento(5f, "5");
		aperturaPozoTextSelectorUI.agregarElemento(6f, "6");
		aperturaPozoTextSelectorUI.agregarElemento(7f, "7");
		aperturaPozoTextSelectorUI.agregarElemento(8f, "8");
		aperturaPozoTextSelectorUI.agregarElemento(9f, "9");
		aperturaPozoTextSelectorUI.agregarElemento(10f, "10");
		aperturaPozoTextSelectorUI.agregarElemento(15f, "15");
		aperturaPozoTextSelectorUI.agregarElemento(20f, "20");
		aperturaPozoTextSelectorUI.agregarElemento(30f, "30");
		aperturaPozoTextSelectorUI.agregarElemento(50f, "50");
		aperturaPozoTextSelectorUI.agregarElemento(100f, "100");
		aperturaPozoTextSelectorUI.mostrarClavePorDefecto(Sistema.JACKPOT_APERTURA_POZO);
		
		//creamos el boton
		restablecerMontoActualButtonUI = new ButtonUI(this, 300, 450, "Reestablecer", "Reestablece el monto acumulado del JACKPOT a cero.", false);
		
		//agregamos a la lista de
		//elementos enfocables
		enfocables.add(montoMinimoTextSelectorUI);
		enfocables.add(porcentajeExtraidoApuestaTextSelectorUI);
		enfocables.add(probabilidadJugadorTextSelectorUI);
		enfocables.add(aperturaPozoTextSelectorUI);
		enfocables.add(restablecerMontoActualButtonUI);
		
		//configuramos el primer texto
		reprocesarTextoDescripcion(montoMinimoTextSelectorUI.descripcion);
		
	}

	protected void renderizar(Graphics g) {
		
		//pintamos el fondo
		Color colorOriginal = g.getColor();
		g.setColor(colorFondo);
		g.fillRect(0, 0, principal.getWidth(), principal.getHeight());
		g.setColor(colorOriginal);
		
		//renderizamos cada componente
		montoMinimoTextSelectorUI.renderizar(g);
		porcentajeExtraidoApuestaTextSelectorUI.renderizar(g);
		probabilidadJugadorTextSelectorUI.renderizar(g);
		aperturaPozoTextSelectorUI.renderizar(g);
		restablecerMontoActualButtonUI.renderizar(g);
		
		//dibujamos el monto actual
		//distribuido del pozo 
//		fuente.drawString(270, 400, "Monto actual: " + decimalFormat.format(principal.maquina.logicaMaquina.distribuidorPremios.montoDistribuidoJackpot));
		
	}

	protected void actualizar(long elapsedTime) {
	
		montoMinimoTextSelectorUI.actualizar(elapsedTime);
		porcentajeExtraidoApuestaTextSelectorUI.actualizar(elapsedTime);
		probabilidadJugadorTextSelectorUI.actualizar(elapsedTime);
		aperturaPozoTextSelectorUI.actualizar(elapsedTime);
		restablecerMontoActualButtonUI.actualizar(elapsedTime);
		
	}

	protected void botonPresionado(byte botonId) {

		if(botonId == Boton.BOTON1) {
			principal.cambiarPantalla(Pantalla.PANTALLA_MENU_CONFIGURACION_AVANZADA);
		}
		else if(botonId == Boton.BOTON3) {
			presionaFlechaArriba();
		}
		else if(botonId == Boton.BOTON4) {
			presionaFlechaAbajo();
		}

		//logica de presionado de botones
		//izquierda y derecha solo para
		//componentes TextUI
		if(obtenerEnfocado() instanceof TextSelectorUI) {
			if(botonId == Boton.BOTON5) {
				presionaFlechaDerecha(); 
			}
			else if(botonId == Boton.BOTON6) {
				presionaFlechaIzquierda();
			}
		}
		else if(obtenerEnfocado() instanceof ButtonUI) {
			
			if(botonId == Boton.BOTON6) {

				//logica de presionado de botones
				//de tipo ButtonUI
				if(obtenerEnfocado() == restablecerMontoActualButtonUI) {
					
					//actualizamos el valor de configuracion
					principal.maquina.logicaMaquina.actualizarValorConfiguracion(ConfiguracionDAO.JACKPOT_MONTO_ACTUAL, "" + 0f);
				}
			}
		}
	}

	/**
	 * Llamado cuando cambia algun valor 
	 * de alguno de los combo selectores
	 * de la pantalla
	 */
	public void cambiaValorSelectorUI(SelectorUI selectorUI) {

		//ejecutamos el cambio 
		//en la persistencia
		principal.logicaMaquina.actualizarValorConfiguracion(selectorUI.codigo, selectorUI.elementoActualClave.toString());

	}
}