package org.microuy.javaslot.presentacion.modulo.listener.vo;

import org.microuy.javaslot.dominio.JugadaLineaGanadora;

public class MuestraJugadaLineaGanadoraVO {

	private int repeticion;
	private JugadaLineaGanadora jugadaLineaGanadora;
	
	public MuestraJugadaLineaGanadoraVO(JugadaLineaGanadora jugadaLineaGanadora) {
		this.jugadaLineaGanadora = jugadaLineaGanadora;
	}
	
	public int getRepeticion() {
		return repeticion;
	}
	
	public void incrementarRepeticion() {
		repeticion++;
	}
	
	public void setRepeticion(int repeticion) {
		this.repeticion = repeticion;
	}
	public JugadaLineaGanadora getJugadaLineaGanadora() {
		return jugadaLineaGanadora;
	}
	public void setJugadaLineaGanadora(JugadaLineaGanadora jugadaLineaGanadora) {
		this.jugadaLineaGanadora = jugadaLineaGanadora;
	}
}