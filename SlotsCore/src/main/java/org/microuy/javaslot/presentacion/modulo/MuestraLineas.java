package org.microuy.javaslot.presentacion.modulo;

import java.util.ArrayList;
import java.util.List;

import org.microuy.javaslot.excepcion.SystemException;
import org.microuy.javaslot.presentacion.pantalla.PantallaGenerica;
import org.microuy.javaslot.sonido.ReproductorSonido;
import org.microuy.javaslot.util.Timer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.Image;
import org.newdawn.slick.SlickException;

public class MuestraLineas extends ModuloGenerico {

	public static final byte TIPO_ENCENDIDO_APAGADO = 0;
	public static final byte TIPO_ENCENDIDO_PRENDIDO_UNA_VEZ = 1;
	
	//mapa que indica para el key (id de linea) cual
	//es la lista de figuras a dibujar en pantalla
	private List<Image> lineasGanadorasImages;
	private Image contenedorLineasImage;
	
	//variables usadas para configurar 
	//que es lo que se desea mostrar
	private short[] lineas;
	
	private byte tipoEncendido = TIPO_ENCENDIDO_APAGADO;
	
	//variable que configura si 
	//se pintan las lineas o no
	private boolean pintaLineas;

	//timer de intermitencia
	private Timer timerIntermitencia;
	
	public MuestraLineas(PantallaGenerica pantallaGenerica, int x, int y, int width, int height) {
		
		super(pantallaGenerica, x, y, width, height);
		
	}

	public void inicializarRecursos() {

		//inicializacion variables
		lineas = new short[0];
		
		//cargamos las imagenes de las lineas
		lineasGanadorasImages = new ArrayList<Image>();
		try {
			
			//contenedor de lineas
			contenedorLineasImage = new Image("resources/lineas/lines-num.png");
			
			//cada linea independiente
//			lineasGanadorasImages.add(new Image("resources/lineas/lines-01.png"));
//			lineasGanadorasImages.add(new Image("resources/lineas/lines-02.png"));
//			lineasGanadorasImages.add(new Image("resources/lineas/lines-03.png"));
//			lineasGanadorasImages.add(new Image("resources/lineas/lines-04.png"));
//			lineasGanadorasImages.add(new Image("resources/lineas/lines-05.png"));
//			lineasGanadorasImages.add(new Image("resources/lineas/lines-06.png"));
//			lineasGanadorasImages.add(new Image("resources/lineas/lines-07.png"));
//			lineasGanadorasImages.add(new Image("resources/lineas/lines-08.png"));
//			lineasGanadorasImages.add(new Image("resources/lineas/lines-09.png"));
//			lineasGanadorasImages.add(new Image("resources/lineas/lines-10.png"));
            for(int i=0; i<10; i++) {
                lineasGanadorasImages.add(new Image("resources/trans_back.png"));
            }

		} catch (SlickException e) {
			throw new SystemException(e.getMessage());
		}
		
		//cargamos los sonidos correspondientes
		ReproductorSonido.cargarSonidos(new int[]{ReproductorSonido.JUGADA_GANADORA_INTERMITENCIA});
		
	}

	public void render(Graphics g) {
		
		//declaramos las coordenadas a usar
		int xLinea = x;
		int yLinea = 200;
		
		//dibujamos el contendor de lineas
		contenedorLineasImage.draw(xLinea, yLinea);
		
		if(lineas.length > 0) {
			
			//flag seteado por el timer de
			//intermitencia para cambiar
			//de estado permanentemente
			if(pintaLineas) {
				for(int i=0; i<lineas.length; i++) {
					if(lineas.length > 0 && (lineas[i]-1) != -1) {
						//dibujamos la imagen de la linea
						lineasGanadorasImages.get(lineas[i]-1).draw(xLinea, yLinea);					
					}
				}
			}
		}
	}

	public void update(long elapsedTime) {
		
		if(tipoEncendido == TIPO_ENCENDIDO_PRENDIDO_UNA_VEZ) {

			pintaLineas = true;
			if(timerIntermitencia.action(elapsedTime)) {
				//cambio el estado al opuesto del actual
				tipoEncendido = TIPO_ENCENDIDO_APAGADO;
			}
		}
	}
	
	public void mostrarLineas(short[] lineas, byte tipoEncendido, long tiempoIntermitencia) {

		this.pintaLineas = true;
		if(timerIntermitencia == null) {
			timerIntermitencia = new Timer((int)tiempoIntermitencia);	
		}
		else {
			timerIntermitencia.setDelay(tiempoIntermitencia);
			timerIntermitencia.setCurrentTick(0);
		}
		
		if(lineas != null) {
			this.lineas = lineas;
		}
		else {
			this.lineas = new short[0];
		}	
		
		this.tipoEncendido = tipoEncendido;
	}
	
	public void ocultarLineas() {

		tipoEncendido = TIPO_ENCENDIDO_APAGADO;
		this.pintaLineas = false;
	}
	
	public boolean isPintaLineas() {
		return pintaLineas;
	}

	public void setPintaLineas(boolean pintaLineas) {
		this.pintaLineas = pintaLineas;
	}

	public short[] getLineas() {
		return lineas;
	}
}