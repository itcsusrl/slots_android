package org.microuy.javaslot.presentacion.util;

import org.microuy.javaslot.slick.Animation;
import org.newdawn.slick.Image;

public class SlotsAnimation extends Animation {

	public int figuraId;
	public String rutaImagenPrincipal;

	public SlotsAnimation(int figuraId, String rutaImagenPrincipal, Image[] frames, int duration) {
		super(frames, duration);
		this.rutaImagenPrincipal = rutaImagenPrincipal;
		this.figuraId = figuraId;
	}
}