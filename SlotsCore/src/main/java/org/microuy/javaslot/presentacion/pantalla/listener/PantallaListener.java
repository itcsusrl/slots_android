package org.microuy.javaslot.presentacion.pantalla.listener;

public interface PantallaListener {
	
	/**
	 * Devuelve el id de pantalla. 
	 * El id es el definido en la clase Pantalla
	 * @return
	 */
	public int getId();
	
	/**
	 * Cuando se entra a una pantalla 
	 * se ejecuta este metodo
	 */
	public void entraEnPantalla();
	
	/**
	 * Cuando se sale de una pantalla se
	 * ejecuta este metodo
	 */
	public void saleDePantalla();
	
}
