package org.microuy.javaslot.presentacion.modulo;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

import org.microuy.javaslot.constante.Boton;
import org.microuy.javaslot.dominio.util.Pair;
import org.microuy.javaslot.presentacion.dominio.EstadoBoton;
import org.microuy.javaslot.presentacion.pantalla.PantallaGenerica;
import org.newdawn.slick.Graphics;

public class ManejadorEscritorio extends ModuloGenerico {
	
	public static final int ESCRITORIO_SLOTS = 0;
	public static final int ESCRITORIO_MINIJUEGO_MENOR_MAYOR = 1;
	
	/**
	 * Mapa contenedor de los esritorios
	 */
	public HashMap<Integer, Escritorio> escritorios = new HashMap<Integer, Escritorio>();
	
	/**
	 * Id del escritorio que se 
	 * encuentra activo actualmente
	 */
	public int escritorioActivoId = -1;
	
	/**
	 * Id del escritorio destino
	 * cuando se cambia de un 
	 * escritorio a otro
	 */
	public int escritorioDestinoId = -1;
	
	//variables usadas para 
	//calculos durante la animacion
	//de traslacion de escritorio
	private int origenX;
	private int origenY;
	private int destinoX;
	private int destinoY;
	private int posicionActualX;
	private int posicionActualY;
	private int posicionAnteriorX;
	private int posicionAnteriorY;
	
	//tiempo que demora en 
	//completarse la traslacion
	private final int TIEMPO_TRASLACION = 500;
	
	//tiempo actual de la traslacion
	//NOTA: debe de comenzar en 1, sino 
	//multiplica por cero y anula todos
	//los valores
	private int tiempoActualTraslacion = 1;
	
	/**
	 * Estados posibles del 
	 * Manejador de Escritorios 
	 */
	public static final byte ESTADO_ESTATICO = 0;
	public static final byte ESTADO_TRASLADANDO = 1;
	
	//estado actual
	public int estadoActual = ESTADO_ESTATICO;
	
	public ManejadorEscritorio(PantallaGenerica pantallaGenerica) {

		//el tamano del manejador de escritorio 
		//es del tamano total del juego
		super(pantallaGenerica, 0, 0, pantallaGenerica.principal.getWidth(), pantallaGenerica.principal.getHeight());
		
	}
	
	/**
	 * Devuelve un objeto de tipo Pair en el que
	 * la clave es la posicion en 'X' y el valor es
	 * la posicion en 'Y'
	 * @param idEscritorio
	 * @return
	 */
	public static Pair obtenerCoordenadasEscritorio(int idEscritorio, int width, int height) {
		Pair pair = new Pair();
		pair.clave = idEscritorio * width;
		pair.valor = 0;
		return pair;
	}
	
	/**
	 * Agrega un escritorio con 
	 * el id especificado 
	 * @param id
	 * @param escritorio
	 */
	public void agregarEscritorio(Escritorio escritorio, boolean activo) {
		
		//obtenemos las coordenadas 
		//del escritorio en base al id
		Pair coordenadas = ManejadorEscritorio.obtenerCoordenadasEscritorio(escritorio.id, width, height);
		
		//seteamos las coordenadas
		escritorio.setX((Integer)coordenadas.clave);
		escritorio.setY((Integer)coordenadas.valor);
		
		//agregamos el escritorio
		escritorios.put(escritorio.id, escritorio);
		
		if(activo) {
			escritorioActivoId = escritorio.id;
		}
	}

	public void render(Graphics g) {

		if(escritorios != null) {
			
			if(estadoActual == ESTADO_ESTATICO) {
				//renderizamos
				escritorios.get(escritorioActivoId).renderizar(g);
			}
			else {
				
				//creamos un iterador de escritorios
				Iterator<Escritorio> escritoriosIt = escritorios.values().iterator();
				
				//iteramos
				while(escritoriosIt.hasNext()) {
					
					//obtenemos la iteracion actual
					Escritorio escritorio = escritoriosIt.next();

					//renderizamos
					escritorio.renderizar(g);
				}
			}
		}
	}
	
	public void update(long elapsedTime) {

		if(escritorios != null) {
			
			if(estadoActual == ESTADO_TRASLADANDO) {

				//creamos un iterador de escritorios
				Iterator<Escritorio> escritoriosIt = escritorios.values().iterator();
				
				//calculamos la nueva posicion en x
				posicionActualX = (tiempoActualTraslacion * principal.getWidth()) / TIEMPO_TRASLACION;
				
				//calculamos la diferencia
				//en el eje x
				int diferenciaX = 0;
				if(destinoX < origenX) {
					diferenciaX = posicionAnteriorX - posicionActualX;	
				}
				else {
					diferenciaX = posicionActualX - posicionAnteriorX;
				}
				
				//para calcular la diferencia
				//en la proxima vuelta
				posicionAnteriorX = posicionActualX;

				//iteramos
				while(escritoriosIt.hasNext()) {
					
					//iteracion actual
					Escritorio escritorio = escritoriosIt.next();
					
					//adicionamos diferencia
					escritorio.setX((escritorio.getX() + diferenciaX));
					
					//actualizamos
					escritorio.actualizar(elapsedTime);
					
				}
				
				//incrementamos el tiempo
				//de la animacion
				tiempoActualTraslacion += elapsedTime;

				if(tiempoActualTraslacion >= TIEMPO_TRASLACION) {
					
					//finaliza la animacion
					estadoActual = ESTADO_ESTATICO;
					
					//notificamos la salida del escritorio
					escritorios.get(escritorioActivoId).saleEscritorio();
					
					//configuramos el id del 
					//escritorio activo
					escritorioActivoId = escritorioDestinoId;
					
					//notificamos la entrada al escritorio
					escritorios.get(escritorioActivoId).entraEscritorio();
					
					//ajustamos la posicion final
					escritorios.get(escritorioActivoId).setX(0);
					
					//reseteamos el id del 
					//escritorio destino
					escritorioDestinoId = -1;
					
				}
			}
			
			//actualizamos
			escritorios.get(escritorioActivoId).actualizar(elapsedTime);

		}
	}
	
	/**
	 * Cambia el escritorio usando 
	 * la animacion de traslacion
	 * adecuada
	 * 
	 * @param escritorioDestinoId
	 */
	public void cambiarEscritorio(int escritorioDestinoId) {
		
		//seteo el id del 
		//escritorio destino
		this.escritorioDestinoId = escritorioDestinoId;
		
		//configuramos las variables necesarias
		origenX = escritorios.get(escritorioDestinoId).getX();
		origenY = escritorios.get(escritorioDestinoId).getY();
		destinoX = 0;
		destinoY = 0;
		posicionActualX = 0;
		posicionAnteriorX = 0;
		tiempoActualTraslacion = 1;
		estadoActual = ESTADO_TRASLADANDO;
		
	}

	public void inicializarRecursos() {
		
		if(escritorios != null) {
			
			//creamos un iterador de escritorios
			Iterator<Escritorio> escritoriosIt = escritorios.values().iterator();
			
			//iteramos
			while(escritoriosIt.hasNext()) {
				
				//obtenemos la iteracion actual
				Escritorio escritorio = escritoriosIt.next();

				//inicializamos recursos
				escritorio.inicializarRecursos();
			}
		}
	}
}