package org.microuy.javaslot.presentacion.effects;

import java.io.IOException;

import org.microuy.javaslot.excepcion.SystemException;
import org.newdawn.slick.particles.ConfigurableEmitter;
import org.newdawn.slick.particles.ParticleIO;
import org.newdawn.slick.particles.ParticleSystem;

public class FiguraGanadoraEffect extends GenericEffect {

	//sistema de particulas de slick
	private ParticleSystem system;
	
	//emisor usado por el sistema de particulas
	private ConfigurableEmitter emitter;
	
	//coordenadas de renderizacion
	private int x;
	private int y;
	
	public boolean renderizar;
	
	public FiguraGanadoraEffect() {
		inicializar();
	}
	
	public void inicializar() {

		try {

			//cargamos el sistema de particulas
			//desde el archivo xml de configuracion
			system = ParticleIO.loadConfiguredSystem("resources/particles/sistema_figura_rodillo_jackpot.xml");

		} 
		catch (IOException e) {
			throw new SystemException("Failed to load particle system");
		}
	}
	
	public void setImageName(String imageName) {
		
		//obtenemos el emisor creado en el xml
		emitter = (ConfigurableEmitter) system.getEmitter(0);

		//seteamos la ruta a la imagen
		emitter.setImageName(imageName);
		
	}
	
	public void setCoordinates(int x, int y) {
		
		this.x = x;
		this.y = y;
		
		//obtenemos el emisor creado en el xml
		//para configurar la nueva coordenada
		emitter = (ConfigurableEmitter) system.getEmitter(0);
		emitter.setPosition(x, y);
	}

	public void render() {
		if(renderizar) {
			system.render();
		}
	}

	public void update(int delta) {
		if(renderizar) {
			system.update(delta);
		}
	}
}