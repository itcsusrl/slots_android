package org.microuy.javaslot.presentacion.modulo.ui;

public interface EnfocableIf {

	public boolean isEnfocado();
	public void setEnfocado(boolean enfocado);
	
}
