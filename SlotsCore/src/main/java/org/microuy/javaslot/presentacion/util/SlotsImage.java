package org.microuy.javaslot.presentacion.util;

import org.newdawn.slick.Image;
import org.newdawn.slick.SlickException;

public class SlotsImage extends Image {

	public int figuraId;
	public String rutaFigura;
	
	public SlotsImage(int figuraId, String rutaFigura) throws SlickException {
		super(rutaFigura);
		this.figuraId = figuraId;
		this.rutaFigura = rutaFigura;
	}
}