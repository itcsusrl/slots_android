package org.microuy.javaslot.presentacion.effects;

public abstract class GenericEffect {

	public abstract void inicializar();
	public abstract void render();
	public abstract void update(int elapsedTime);
	
}
