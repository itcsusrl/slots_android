package org.microuy.javaslot.logica;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

import org.microuy.javaslot.constante.FiguraTipo;
import org.microuy.javaslot.constante.JugadaTipo;
import org.microuy.javaslot.constante.Sistema;
import org.microuy.javaslot.dominio.Figura;
import org.microuy.javaslot.dominio.JugadaGanadora;
import org.microuy.javaslot.dominio.JugadaGanadoraBonusMenorMayor;
import org.microuy.javaslot.dominio.JugadaGanadoraJackpot;
import org.microuy.javaslot.dominio.JugadaLineaGanadora;
import org.microuy.javaslot.dominio.Linea;
import org.microuy.javaslot.dominio.Maquina;
import org.microuy.javaslot.persistencia.dao.ConfiguracionDAO;
import org.microuy.javaslot.util.SlotsRandomGenerator;

/**
 * Distribuye premios cancelados entre las
 * jugadas ganadoras definidas en la 
 * configuracion de la maquina. 
 * La distribucion la hace de la forma
 * mas optima en base a las probabilidades
 * de pago de cada jugada ganadora
 * 
 * @author Pablo Caviglia
 *
 */
public class DistribuidorPremios {

	//premios normales distribuidos
	//en este hashmap
	public HashMap<JugadaGanadora, Float> premiosDistribuidos;
	
	//monto distribuido para el jackpot
	public float montoDistribuidoJackpot;
	
	//monto distribuido para el 
	//minijuego 'menor o mayor'
	public float montoDistribuidoMenorMayor;

	/**
	 * Cuando se genera un premio se guarda en
	 * esta variable toda la informacion del mismo
	 */
	public JugadaLineaGanadora premioGenerado;
	
	/**
	 * Tras generar un premio (o no), es necesario
	 * generar tambien una posicion valida en los
	 * rodillos para representar la jugada. Ya que
	 * puede llegar a ser bastante costoso (en cuanto
	 * a trabajo de procesamiento), buscar el premio
	 * exacto, lo que se hace es crear una tirada
	 * falsa, que no respeta las posiciones de las 
	 * figuras en los rodillos, de esta manera se
	 * logra entregar un premio, o cancelar otro, 
	 * con el fin de normalizar los pagos a los 
	 * valores indicados por el administrador.
	 */
	public int[][] tiradaFicticia;
	
	/**
	 * La maquina a la que se encuentra 
	 * ligado el distribuidor de premios
	 */
	private Maquina maquina;
	
	/**
	 * Define un porcentaje de entrega de premios.
	 * Esto significa que a pesar de haber premios
	 * prontos para ser entregados, los mismos seran
	 * entregados solamente un porcentaje de veces
	 * de todas las ejecuciones.
	 * 
	 * Esto fue implementado para que no se entregaran
	 * inmediatamente los premios chicos, y de esta manera
	 * lograr incrementar todas las jugadas ganadoras, asi
	 * las jugadas grandes tienen mas posibilidades de salir 
	 */
	public short porcentajeEntregaPremios;
	public static short modificadorPorcentajeEntregapremios;
	
	public DistribuidorPremios(Maquina maquina) {
		
		this.maquina = maquina;
		premiosDistribuidos = new HashMap<JugadaGanadora, Float>();
		
		//obtenemos la lista de jugadas 
		//ganadoras correspondientes a la
		//configuracion de la maquina
		List<JugadaGanadora> jugadasGanadoras = maquina.configuracion.getJugadasGanadoras();
		
		//recorremos la lista de jugadas ganadoras
		for(JugadaGanadora jugadaGanadora : jugadasGanadoras) {

			//solo distribuimos premios en 
			//jugadas de tipo normal
			if(jugadaGanadora.tipo == JugadaTipo.NORMAL) {
				
				//insertamos en el hashmap
				//la jugada ganadora actual
				premiosDistribuidos.put(jugadaGanadora, 0f);
			}
		}
	}
	
	/**
	 * Distribuye un porcentaje dado del
	 * monto de la apuesta actual. 
	 */
	public void distribuirMontoPremioJackpot() {
		
		//obtenemos los datos de la apuesta
		//para cerciorarnos de que es una
		//apuesta maxima
		short lineasApostadas = maquina.lineasApostadas;
		float dineroApostadoPorLinea = maquina.dineroApostadoPorLinea;
		float apuestaActualUsuario = lineasApostadas * dineroApostadoPorLinea;
		
		//si el usuario hizo la apuesta
		//minima para participar en el jackpot
		// se le toma un porcentaje de la 
		//apuesta para participar por el JACKPOT
		if(apuestaActualUsuario >= Sistema.JACKPOT_APERTURA_POZO) {
			
			//calculamos el monto a extraer
			float montoExtraidoApuesta = ((Sistema.JACKPOT_PORCENTAJE_EXTRAIDO_APUESTA * apuestaActualUsuario) / 100f);
			
			//el monto extraido lo agregamos al
			//dinero 'ganado' en la maquina, ya
			//que el monto de la apuesta no va a
			//ser tocado, pero el monto extraido
			//de la apuesta ya va a ser ingresado
			//al pozo del JACKPOT
			maquina.estadisticaMaquina.dineroGanado += montoExtraidoApuesta;
			
			//incrementamos el monto
			montoDistribuidoJackpot += montoExtraidoApuesta;
			
			//guardamos en la persistencia
			//el nuevo monto del jackpot
			maquina.logicaMaquina.actualizarValorConfiguracion(ConfiguracionDAO.JACKPOT_MONTO_ACTUAL, ""+montoDistribuidoJackpot);

		}
	}
	
	/**
	 * Distribuye un porcentaje dado del
	 * monto de la apuesta actual en el
	 * pozo acumulado del minijuego 
	 * 'menor o mayor'
	 */
	public void distribuirMontoPremioMenorMayor() {
		
		//obtenemos los datos de la apuesta
		//para cerciorarnos de que es una
		//apuesta maxima
		short lineasApostadas = maquina.lineasApostadas;
		float dineroApostadoPorLinea = maquina.dineroApostadoPorLinea;
		float apuestaActualUsuario = lineasApostadas * dineroApostadoPorLinea;
		
		//calculamos el monto a extraer
		float montoExtraidoApuesta = ((Sistema.MENOR_MAYOR_PORCENTAJE_EXTRAIDO_APUESTA * apuestaActualUsuario) / 100f);
		
		//el monto extraido lo agregamos al
		//dinero 'ganado' en la maquina, ya
		//que el monto de la apuesta no va a
		//ser tocado, pero el monto extraido
		//de la apuesta ya va a ser ingresado
		//al pozo del minijuego 'menor o mayor'
		maquina.estadisticaMaquina.dineroGanado += montoExtraidoApuesta;
		
		//incrementamos el monto
		montoDistribuidoMenorMayor += montoExtraidoApuesta;
		
		//guardamos en la persistencia
		//el nuevo monto del jackpot
		maquina.logicaMaquina.actualizarValorConfiguracion(ConfiguracionDAO.MENOR_MAYOR_MONTO_ACTUAL, ""+montoDistribuidoMenorMayor);

	}
	
	/**
	 * Distribuye el monto asignado entre
	 * todos los premios disponibles en 
	 * la configuracion de la maquina
	 * 
	 * @param monto
	 */
	public void distribuirMontoPremioNormal(float monto) {

		//obtenemos la lista de las posibles jugadas
		//ganadoras para obtener su porcentaje de 
		//distribucion configurado
		List<JugadaGanadora> jugadasGanadoras = new ArrayList<JugadaGanadora>(maquina.jugadasGanadoras);
		
		//recorremos la lista de jugadas ganadoras 
		//para repartir el monto en base a los 
		//porcentajes ed distribucion de cada jugada
		for(JugadaGanadora jugadaGanadora : jugadasGanadoras) {
			
			if(jugadaGanadora.tipo == JugadaTipo.NORMAL) {
				
				//obtenemos el porcentaje de 
				//distribucion de esta jugada
				int porcentajeDistribucionActual = jugadaGanadora.porcentajeDistribucion;

				//calculamos que porcion del monto le
				//corresponde a la jugada ganadora en
				//base al porcentaje de pago que ya
				//tiene asignado
				float montoAsignableJugadaGanadora = ((((float)porcentajeDistribucionActual) * monto) / 100f); 
				
				//buscamos la jugada ganadora en el
				//mapa para incrementar su valor con
				//el monto calculado anteriormente
				Float montoActual = premiosDistribuidos.get(jugadaGanadora);
				Float montoJugada = montoActual + montoAsignableJugadaGanadora;
				
				//incrementamos el monto en el mapa
				premiosDistribuidos.put(jugadaGanadora, montoJugada);
				
				//registramos la distribucion
				//de premios en la base de datos
				if(!maquina.test) {
					maquina.logicaMaquina.persistirDistribucionPremio(jugadaGanadora.obtenerFormateado(), montoJugada);
				}
			}
		}
	}
	
	/**
	 * Teniendo en cuenta las lineas y el
	 * dinero apostado en la ultima apuesta,
	 * analiza si se puede pagar alguno de 
	 * los premios que tiene guardado el
	 * distribuidor de premios pronto para
	 * dar.
	 * En caso de que se haya generado algun
	 * premio, se guarda en la variable 
	 * 'premioGenerado'. Es importante que se
	 * utilice esa variable antes de llamarse 
	 * nuevamente a este metodo si no se quiere
	 * pisar el premio con un nuevo.
	 * 
	 * @return
	 */
	public synchronized boolean generarPremio() {
		
		boolean tienePremio = false;
		
		//jugada ganadora disponible y 
		//pagable hasta el momento
		JugadaGanadora jugadaGanadoraDisponible = null;

		/**
		 * Chequeamos la creacion del premio JACKPOT
		 */
		JugadaGanadoraJackpot jugadaGanadoraJackpot = generarJackpot();
		JugadaGanadoraBonusMenorMayor jugadaGanadoraBonusMenorMayor = (jugadaGanadoraJackpot != null ? null : generarBonusMenorMayor());
		
		/**
		 * Si la instancia 'jugadaGanadoraJackpot' no
		 * es nula significa que el jugador ha sacado
		 * el JACKPOT 	
		 */
		if(jugadaGanadoraJackpot != null) {
			
			//referenciamos al premio
			jugadaGanadoraDisponible = jugadaGanadoraJackpot;
		}
		else if(jugadaGanadoraBonusMenorMayor != null) {
			
			//referenciamos al premio
			jugadaGanadoraDisponible = jugadaGanadoraBonusMenorMayor;
		}
//		else { //comunmente se entra a este 'else'
//			   //debido a que el 'if' previo 
//			   //es de cuando el jugador gana el 
//			   //JACKPOT o el bonus 'menor mayor'
//			
//			/**
//			 * Con un calculo aleatorio estimamos si 
//			 * el premio generado debe de ser entregado.
//			 * Esto se hace para que no siempre que se
//			 * llega al monto necesario para entregar un
//			 * premio se entregue. Comunmente esto sucede
//			 * cuando apostadores fuertes dejan de jugar, y
//			 * los apostadores debiles toman su lugar, 
//			 * pudiendo acceder a montos mas chicos asignables
//			 * por el modulo distribuidor de premios.
//			 * 
//			 * TODO Antes la cuenta era |porcentajeEntregaPremios + modificadorPorcentajeEntregapremios|
//			 * pero decidio sacarse el modificador debido a que entro en accion el Modulo Evaluador
//			 * de Dinero Real
//			 */
//			if(SlotsRandomGenerator.ejecutarPorcentajeEntrega((porcentajeEntregaPremios))) {
//				
//				//obtenemos el monto apostado
//				//por el cliente en la tirada
//				float dineroApostadoPorLinea = maquina.dineroApostadoPorLinea;
//				
//				//recorremos la lista de premios
//				Iterator<JugadaGanadora> jugadasGanadorasIt = premiosDistribuidos.keySet().iterator();
//				while(jugadasGanadorasIt.hasNext()) {
//					
//					//obtenemos la jugada ganadora actual
//					JugadaGanadora jugadaGanadoraActual = jugadasGanadorasIt.next();
//				
//					//obtenemos el monto asociada a ella
//					float montoJugadaGanadora = premiosDistribuidos.get(jugadaGanadoraActual);
//
//					
//					//si la jugada ganadora tiene al
//					//menos algo de dinero para distribuir
//					if(montoJugadaGanadora > 0) {
//						
//						//chequeamos que el monto de la jugada, a pesar de ser mayor de
//						//cero, pueda pagarse en base a lo que haya apostado el jugador
//						float montoNecesario = jugadaGanadoraActual.multiplicador * dineroApostadoPorLinea;
//						
//						//chequeamos que el monto necesario
//						//sea menor o igual que el monto
//						//distribuible por la jugada ganadora
//						if(montoNecesario <= montoJugadaGanadora) {
////						if(montoNecesario <= (montoJugadaGanadora * dineroApostadoPorLinea)) {
//						/**
//						 * TODO Antes se hacia lo que esta anteriormente
//						 * comentado. Se hace un cambio importante para
//						 * que el monto que el jugador se pueda llevar de
//						 * los pozos distribuidos sean relativos a la cantidad
//						 * de dinero apostado por el jugador. De esta manera
//						 * le damos mas a los que apuestan mas, y menos a los
//						 * que apuestan menos.
//						 */
//						
//						
//							//por ultimo chequeamos si alguna jugada
//							//anterior ya asigno algun premio y si
//							//es mayor al actual para no usar este
//							if(jugadaGanadoraDisponible == null) {
//								jugadaGanadoraDisponible = jugadaGanadoraActual;
//							}
//							else {
//								
//								//verificamos que la jugada asignada
//								//antes no tuviera mayor premio que
//								//la actual
//								float premioAnterior = jugadaGanadoraDisponible.multiplicador * dineroApostadoPorLinea;
//								
//								//si el monto del premio anterior es
//								//menor, entonces asignamos el nuevo
//								//premio
//								if(premioAnterior < montoNecesario) {
//									jugadaGanadoraDisponible = jugadaGanadoraActual;
//								}
//							}
//						}
//					}
//				}
//				
//				//decrementamos el monto de la 
//				//jugada ganadora disponible
//				if(jugadaGanadoraDisponible != null) {
//					
//					//calculamos el monto a decrementar
//					float montoDecremento = (float)jugadaGanadoraDisponible.multiplicador * maquina.dineroApostadoPorLinea;
//
//					//decrementamos el monto disponible
//					float nuevoMontoJugadaGanadora = premiosDistribuidos.get(jugadaGanadoraDisponible) - montoDecremento;
//					
//					//lo insertamos nuevamente
//					premiosDistribuidos.put(jugadaGanadoraDisponible, nuevoMontoJugadaGanadora);
//					
//				}
//			}
//		}
		
		//generamos la tirada ficticia
		generarTiradaFicticia(jugadaGanadoraDisponible);

		//seteamos el flag correspondiente para
		//que el metodo devuelva si fue creada
		//una jugada con o sin premio
		if(jugadaGanadoraDisponible != null) {
			tienePremio = true;
		}
		else {
			tienePremio = false;
		}
		
		return tienePremio;
	}
	
	/**
	 * Genera una jugada ganadora instantaneamente
	 * exclusivamente para el modo demo del juego
	 */
	public void generarJugadaGanadoraDemo() {
		
		JugadaGanadora jugadaGanadoraDisponible = null;
		
		//recorremos la lista de todas las
		//jugadas ganadoras del sistema y
		//elegimos una aleatoriamente
		List<JugadaGanadora> jugadasGanadorasNormal = maquina.configuracion.jugadasGanadorasNormal;
		jugadaGanadoraDisponible = jugadasGanadorasNormal.get(SlotsRandomGenerator.getInstance().nextInt(jugadasGanadorasNormal.size()));
		
		//generamos la tirada ficticia
		generarTiradaFicticia(jugadaGanadoraDisponible);

	}
	
	/**
	 * Evalula la posible generacion
	 * de un premio de tipo Bonus 
	 * Mayor Menor, lo que daria al
	 * usuario la posibilidad de jugar
	 * el minijuegos
	 * @return
	 */
	private JugadaGanadoraBonusMenorMayor generarBonusMenorMayor() {
		
		JugadaGanadoraBonusMenorMayor bonus = null;
		
		//el monto con el que se ingresa 
		//a este tipo de bonus es el monto
		//apostado en su totalidad
		float montoIngresoBonus = maquina.dineroApostadoPorLinea * (float)maquina.lineasApostadas;
		
		//para acceder al bonus se tiene que
		//poder cubrir al menos dos apuestas
		//de mayor o menor
		float montoMinimoIngresoBonus = montoIngresoBonus * 2f * 2f;
		
		//solo un porcentaje accede
		//al uso de este tipo de premio
//		boolean ejecutaEntregaPremio = SlotsRandomGenerator.ejecutarPorcentajeEntrega(5);
		boolean ejecutaEntregaPremio = SlotsRandomGenerator.ejecutarPorcentajeEntrega(15); //TODO CAMBIARME TEMPORAL!!!
		
		//si el monto minimo necesario para
		//ingresar al bonus es cubierto por
		//el monto distribuido disponible, 
		//se puede entrar al juego
		if(montoMinimoIngresoBonus <= montoDistribuidoMenorMayor && ejecutaEntregaPremio) {
			bonus = maquina.configuracion.jugadaGanadoraBonusMenorMayor;
			bonus.monto = montoIngresoBonus;
		}
		else {
			bonus = null;
		}
		
		return bonus;
	}
	
	/**
	 * Evalua la posible entrega del
	 * JACKPOT en base a un analisis
	 * de la situacion estadistica de
	 * la maquina
	 * 
	 * @return
	 */
	private JugadaGanadoraJackpot generarJackpot() {
		
		JugadaGanadoraJackpot jackpot = null;
		
		//el monto minimo necesario que haya en el
		//pozo es el configurado en el sistema, menos
		//un 5% que se configura para tener un factor
		//de aleatoriedad mas interesante
		float montoMinimoJackpot = ((float)Sistema.JACKPOT_MONTO_MINIMO) - ((5f*(float)Sistema.JACKPOT_MONTO_MINIMO)/100f);
		
		//chequeamos que el monto actual
		//del jackpot sea mayor o igual al
		//monto minimo configurado
		if(montoDistribuidoJackpot >= montoMinimoJackpot) {
			
			//Obtenemos el porcentaje de probabilidades
			//del jugador de lograr obtener el jackpot
			//una vez que llegamos al monto necesario.
			//Esto se realiza para que no se lleve el
			//Jackpot el jugador que llegue primero al
			//monto, sino el que entre en el porcentaje
			//de probabilidades de ganar
			float probabilidadesJugadorJackpot = Sistema.JACKPOT_PROBABILIDADES_JUGADOR;
			
			//obtenemos el valor aleatorio
			float valorAleatorio = SlotsRandomGenerator.getInstance().nextFloat() * 100f;
			
			//si el valor aleatorio contiene al valor
			//definido como probabilidad del jugador,
			//significa que se llego al porcentaje 
			//deseado para entregar el JACKPOT
			if(valorAleatorio <= probabilidadesJugadorJackpot) {
				jackpot = maquina.configuracion.jugadaGanadoraJackpot;
				jackpot.monto = montoDistribuidoJackpot;
			}
		}
		
		return jackpot;
	}
	
	/**
	 * Dada una jugada ganadora por parametro genera una
	 * tirada ficticia preparada para entregar el premio
	 * de la jugada recibida por parametro. Si la jugada
	 * recibida es nula, el metodo crea una jugada ficticia
	 * sin premio.
	 * 
	 * @param jugadaGanadora
	 */
	private void generarTiradaFicticia(JugadaGanadora jugadaGanadora) {
		
		//obtenemos la cantidad de rodillos
		int cantRodillos = maquina.configuracion.getRodillos().size();
		
		//calculamos el alto (visible) de cada rodillo
		int figurasVisibleRodillo = maquina.configuracion.getLineas().get(0).getForma().length;
		
		//obtenemos la lista de todas las 
		//figuras que componen la maquina
		List<Figura> figurasMaquina = maquina.configuracion.getFiguras();

		//creamos una lista de figuras 'normales', 
		//sin figuras scatter, wild y bonus
		List<Figura> figuras = new ArrayList<Figura>();
		
		//coleccion temporal para mantener 
		//la lista de figuras no usables 
		//durante la creacion de un premio
		List<Integer> figurasNoUsables = new ArrayList<Integer>();

		//figuras que no contienen premio
		List<Figura> figurasSinPremio = new ArrayList<Figura>();

		//figuras que se pueden usar
		List<Integer> figurasUsables = new ArrayList<Integer>();

		//creamos una lista con figuras permitidas
		List<Figura> figurasPermitidas = new ArrayList<Figura>();
		
		//hacemos el filtro de figuras
		for(Figura figuraMaquinaActual : figurasMaquina) {
			if(figuraMaquinaActual.getTipo() == FiguraTipo.NORMAL || figuraMaquinaActual.getTipo() == FiguraTipo.BONUS_MENOR_MAYOR) {
				figuras.add(figuraMaquinaActual);
			}
		}
		
		//variable util
		int cantidadFiguras = figuras.size();
		
		//constante que indica cuantas
		//figuras lindantes tiene la figura
		//de un rodillo con respecto a los
		//rodillos lindantes
		int FIGURAS_LINDANTES = 3;
		
		//contenedor de la tirada ficticia
		int[][] tirada = new int[cantRodillos][figurasVisibleRodillo];
		
		//configuramos el premio como vacio
		premioGenerado = null;
		
		//en caso de que la jugada ganadora
		//no sea nula (exista premio), guardamos
		//la forma de la linea elegida 
		int[][] formaLineaGanadora = null;
		
		/**
		 * Si existe premio, agregamos a la jugada
		 * sin premio generada lineas atras, la 
		 * jugada del premio y creamos una tirada
		 * ficticia con un premio
		 */
		if(jugadaGanadora != null) {
			
			//obtenemos cuantas lineas fueron
			//apostadas por el jugador
			int lineasApostadas = maquina.lineasApostadas;
			
			//elegimos una linea aleatoriamente
			int lineaElegida = (int)((SlotsRandomGenerator.getInstance().nextFloat() * (float)lineasApostadas) + 1f);
			
			//obtenemos la lista de lineas
			List<Linea> lineas = maquina.configuracion.getLineas();
			
			//referencia a la forma de
			//la linea elegida 
			Linea linea = null;
			
			//recorremos la lista buscando
			//el id correspondiente al valor
			//de la variable 'lineaElegida'
			for(Linea lineaActual : lineas) {
				if(lineaActual.getId() == lineaElegida) {
					formaLineaGanadora = lineaActual.getForma();
					linea = lineaActual;
					break;
				}
			}
			
			//recorremos cada rodillo
			for(int rodilloActual=0; rodilloActual<cantRodillos; rodilloActual++) {
				
				//recorremos cada figura visible del rodillo
				for(int posicionDentroRodilloActual=0; posicionDentroRodilloActual<figurasVisibleRodillo; posicionDentroRodilloActual++) {
					
					//identificamos si la posicion
					//actual posee premio en la 
					//forma de la linea
					boolean figuraPremio = formaLineaGanadora[posicionDentroRodilloActual][rodilloActual] == 1;
					
					//si el largo de las figuras
					//del premio no alcanza el largo
					//de la linea (5 generalmente) entonces
					//no es una figura con premio
					if(jugadaGanadora.figurasIds.length < rodilloActual+1) {
						figuraPremio = false;
					}
					
					//si es una posicion de premio
					//se guarda el id de la figura
					//en la posicion actual
					if(figuraPremio) {
						int idFiguraPremio = jugadaGanadora.figurasIds[rodilloActual];
						tirada[rodilloActual][posicionDentroRodilloActual] = idFiguraPremio;
					}
				}
			}
			
			//se genera el premio
			premioGenerado = new JugadaLineaGanadora();
			premioGenerado.setJugadaGanadora(jugadaGanadora);
			premioGenerado.setFiguras(jugadaGanadora.figuras);
			premioGenerado.setLinea(linea);
			premioGenerado.setTipo(jugadaGanadora.tipo);
			premioGenerado.setGenerado(true);
			
		}
		
		
		/**
		 * Generamos una jugada SIN premio.
		 * Mas adelante se chequea si se 
		 * debe de generar premio, en caso de
		 * que si haya que crear premio, a la
		 * jugada sin premio creada a continuacion
		 * se le agregan las figuras correspondientes
		 * al premio que se desee dar
		 */
		
		//indica cuantas figuras de tipo
		//bonus mayor menor fueron ya puestas
		//en la jugada sin premio generada
		byte figurasBonusMenorMayorPuestas = 0;
		
		//recorremos cada rodillo
		for(int rodilloActual=0; rodilloActual<cantRodillos; rodilloActual++) {
			
			//recorremos cada figura visible del rodillo
			for(int posicionDentroRodilloActual=0; posicionDentroRodilloActual<figurasVisibleRodillo; posicionDentroRodilloActual++) {
				
				//chequeamos (en caso de que existe premio), si
				//el mismo se encuentra en la posicion que estamos
				//recorriendo actualmente
				boolean figuraPremio = false;
				
				//si existe premio
				if(formaLineaGanadora != null) {
					figuraPremio = formaLineaGanadora[posicionDentroRodilloActual][rodilloActual] == 1;	
				}
				
				//el primer rodillo nos interesa siempre
				//y cuando haya un premio generado, de 
				//esta manera podemos detectar un caso
				//de generacion erroneo de premio
				if(rodilloActual == 0 && premioGenerado != null) {
					
					//solo si no es la posicion de 
					//la figura con premio
					if(!figuraPremio) {
						
						//rastreamos la figura con premio
						//en el primer rodillo
						int idFiguraPremio = premioGenerado.getFiguras().get(0).getId();
						
						//creamos un subconjunto de figuras
						//que no contenga el id de la figura
						//con premio
						figurasSinPremio.clear();
						
						//recorremos la lista de todas las
						//figuras que contiene la maquina
						for(Figura figuraActual : figurasMaquina) {

							//obtenemos la figura 
							//recorrida actual
							int idFiguraActual = figuraActual.getId();
							
							//si el id de figura actual es 
							//diferente al del premio, lo 
							//seleccionamos como figura para 
							//la posicion de tirada ficticia 
							//actual
							if(idFiguraPremio != idFiguraActual && 
									figuraActual.getTipo() != FiguraTipo.WILD && 
									figuraActual.getTipo() != FiguraTipo.BONUS &&
									figuraActual.getTipo() != FiguraTipo.JACKPOT &&
									figuraActual.getTipo() != FiguraTipo.SCATTER) {
								figurasSinPremio.add(figuraActual);
							}
						}
						
						//elegimos una figura aleatoria 
						//de la lista de figuras sin premio
						int posicionAleatoria = (int)(SlotsRandomGenerator.getInstance().nextFloat() * (float)figurasSinPremio.size());
						
						//guardamos el id de la figura en
						//el array de la tirada ficticia
						tirada[rodilloActual][posicionDentroRodilloActual] = figurasSinPremio.get(posicionAleatoria).getId();
						
					}
				}
				else if(rodilloActual == 1) {
					
					//El segundo rodillo nos
					//interesa debido a que los premios van
					//siempre de izquierda a derecha empezando
					//desde el primer rodillo. Si hacemos que no
					//coincida ninguna figura del primer rodillo
					//con el segundo, no va a generase ningun premio
				
					//obtenemos el primer rodillo para buscar
					//figuras que poner el segundo que sean
					//diferentes asi no se generan premios
					int[] primerRodillo = tirada[0];
					
					//lista con figuras que no deben ser usadas
					//en el segundo rodillo para no generar premios
					figurasNoUsables.clear();
					
					//obtenemos las figuras no admitidas 
					//como parte del segundo rodillo para
					//la posicion actual
					//La figura actual debe de ser diferente a las figuras del
					//primer rodillo en las posiciones figuraActual -1, 0 y +1
					for(int i=posicionDentroRodilloActual-1, j=0; j<FIGURAS_LINDANTES; i++, j++) {
						
						//si i es menor que cero significa
						//que estamos en una figura previa
						//a las visibles por ende la damos
						//por descontado porque no se evalua
						//para los premios
						if(i >= 0 && i < FIGURAS_LINDANTES) {
							figurasNoUsables.add(primerRodillo[i]);
						}
					}
					
					//una vez que tenemos una lista con
					//las figuras que no deben de ser 
					//usadas procedemos a elegir las figuras
					//que si se pueden usar
					figurasUsables.clear();
					
					for(Figura figuraActual : figuras) {
						
						//flag para indicar que la 
						//figura es usable
						boolean usable = true;
					
						//recorremos la lista de figuras no usables
						for(Integer idFiguraNoUsable : figurasNoUsables) {
							if(idFiguraNoUsable.intValue() == figuraActual.getId()) {
								
								//la figura no es usable
								usable = false;
								break;
							}
						}
						
						//si es usable la agregamos
						if(usable) {
							figurasUsables.add(figuraActual.getId());
						}
					}
					
					/**
					 * Ahora que tenemos la lista de figuras que
					 * si podemos usar, buscamos aleatoriamente en
					 * esa lista y generamos finalmente los valores
					 * del segundo rodillo 
					 */
					int cantidadFigurasUsables = figurasUsables.size();
					
					//buscamos figuras aleatorias
					int valorAleatorio = (int)(SlotsRandomGenerator.getInstance().nextFloat() * (float)cantidadFigurasUsables);
					
					//obtenemos el id de figura aleatorio 
					int figuraAleatoriaId = figurasUsables.get(valorAleatorio);
					
					/**
					 * Solo guardamos en la tirada la figura
					 * seleccionada aleatoriamente si sucede
					 * que no existe una figura con premio en
					 * la posicion actual
					 */
					if(!figuraPremio) {
						//guardamos el id de la figura en
						//el array de la tirada ficticia
						tirada[rodilloActual][posicionDentroRodilloActual] = figuraAleatoriaId;	
					}
				}
				else {
					
					/**
					 * Solo guardamos en la tirada la figura
					 * seleccionada aleatoriamente si sucede
					 * que no existe una figura con premio en
					 * la posicion actual
					 */
					if(!figuraPremio) {

						//si se genero un premio, creamos un 
						//subconjunto de figuras diferentes
						//a la del premio
						if(premioGenerado != null) {

							//creamos una lista con figuras permitidas
							figurasPermitidas.clear();
							
							//obtenemos la lista de figuras 
							//que componen el premio generado
							List<Figura> figurasPremio = premioGenerado.getFiguras();

							//recorremos todas las figuras
							for(Figura figuraActual : figuras) {
								
								boolean existe = false;
								
								//recorremos la lista de figuras 
								//del premio para removerlas de
								//la lista de jugadas permitidas
								for(Figura figuraPremioActual : figurasPremio) {
									
									//si tienen el mismo id, entonces
									//no es una figura permitida
									if(figuraActual.getId() == figuraPremioActual.getId()) {
										existe = true;
										break;
									}
								}
								
								//si no existe la figura en
								//el premio, la agregamos a
								//la lista de permitidas
								if(!existe) {
									figurasPermitidas.add(figuraActual);
								}
							}
							
							//buscamos figuras aleatorias
							int valorAleatorio = (int)(SlotsRandomGenerator.getInstance().nextFloat() * (float)figurasPermitidas.size());
							
							//obtenemos la figura aleatorio 
							Figura figuraAleatoria = figurasPermitidas.get(valorAleatorio);
							
							//guardamos el id de la figura en
							//el array de la tirada ficticia
							tirada[rodilloActual][posicionDentroRodilloActual] = figuraAleatoria.getId();

						}
						else {
							
							//buscamos figuras aleatorias
							int valorAleatorio = (int)(SlotsRandomGenerator.getInstance().nextFloat() * (float)cantidadFiguras);
							
							//obtenemos la figura aleatorio 
							Figura figuraAleatoria = figuras.get(valorAleatorio);
							
							//si la figura aleatoria es
							//de tipo bonus menor mayor,
							//debemos cerciorarnos que ya
							//no la hayamos usado demasiadas
							//veces
							if(figuraAleatoria.getTipo() == FiguraTipo.BONUS_MENOR_MAYOR) {
								
								//increntamos las veces usada
								figurasBonusMenorMayorPuestas++;

								//no se puede usar mas de dos
								//veces en una jugada sin premio
								if(figurasBonusMenorMayorPuestas == 1) {
									Iterator<Figura> itFigurasUsables = figuras.iterator();
									while(itFigurasUsables.hasNext()) {
										Figura foo = itFigurasUsables.next();
										if(foo.getTipo() == FiguraTipo.BONUS_MENOR_MAYOR) {
											
											//removemos la figura que
											//no queremos que aparezca 
											//mas
											itFigurasUsables.remove();
											
											//reconfiguramos el tamano
											//de la coleccion de figuras
											cantidadFiguras = figuras.size();
											
											//quitamos la figura que 
											//precisabamos y nos vamos
											break;
										}
									}
								}
							}
							
							//guardamos el id de la figura en
							//el array de la tirada ficticia
							tirada[rodilloActual][posicionDentroRodilloActual] = figuraAleatoria.getId();
						}
					}
				}
			}
		}
		
		/**
		 * Asignamos la tirada creada para
		 * que ya sea usable por otras capas
		 * del sistema
		 */
		tiradaFicticia = tirada;
		
	}
	
	/**
	 * Calcula cual es el monto distribuido
	 * actualmente entre la suma de los 
	 * montos de todos los premios
	 * @return
	 */
	public float obtenerMontoDistribuidoDisponible() {
		
		float montoDistribuido = 0f;
	
		//obtenemos los montos de todas
		//las jugadas que tenemos
		Iterator<Float> montos = premiosDistribuidos.values().iterator();
		while(montos.hasNext()) {
			
			//monto actual
			Float montoActual = montos.next();
			
			//chequeo
			if(montoActual != null) {
				montoDistribuido += montoActual;
			}
		}
		
		return montoDistribuido;
	}
}