package org.microuy.javaslot.logica.configuracion;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

import org.microuy.javaslot.constante.FiguraTipo;
import org.microuy.javaslot.constante.JugadaTipo;
import org.microuy.javaslot.constante.Sistema;
import org.microuy.javaslot.dominio.Carta;
import org.microuy.javaslot.dominio.Figura;
import org.microuy.javaslot.dominio.JugadaGanadora;
import org.microuy.javaslot.dominio.JugadaGanadoraBonusMenorMayor;
import org.microuy.javaslot.dominio.JugadaGanadoraJackpot;
import org.microuy.javaslot.dominio.Linea;
import org.microuy.javaslot.dominio.Maquina;
import org.microuy.javaslot.dominio.MultiplicadorComparator;
import org.microuy.javaslot.dominio.Rodillo;
import org.microuy.javaslot.dominio.probabilidad.ProbabilidadJugadaGanadora;
import org.microuy.javaslot.dominio.probabilidad.ProbabilidadMaquina;
import org.microuy.javaslot.excepcion.ConfiguracionInvalidaException;

import combinatorics.CombinatoricsVector;
import combinatorics.Generator;
import combinatorics.permutations.PermutationWithRepetitionGenerator;

/**
 * Clase abstracta que define elementos basicos que tiene
 * una maquina tragamonedas.
 * 
 * Estos elementos son:
 * 
 * 	1) Figuras disponibles 
 *  2) Cantidad de rodillos y composicion de los mismos
 *  3) Coleccion de lineas de juego
 *  4) Coleccion de jugadas ganadoras 
 *  
 * Con estos elementos combinados formamos una maquina con
 * determinadas probabilidades de ganancia para la casa
 * y el jugador.
 * 
 * Esta clase debe de ser extendida para implementarse los 
 * metodos faltantes que son los que definen las probabilidades
 * de la maquina.
 * 
 * @author Pablo Caviglia
 *
 */
public abstract class Configuracion {

	protected List<Carta> cartas = new ArrayList<Carta>();
	protected List<Linea> lineas = new ArrayList<Linea>();
	protected List<Figura> figuras = new ArrayList<Figura>();
	protected List<JugadaGanadora> jugadasGanadoras = new ArrayList<JugadaGanadora>();
	
	//filtros de la coleccion 'jugadasGanadoras'
	public List<JugadaGanadora> jugadasGanadorasNormal = new ArrayList<JugadaGanadora>();
	public List<JugadaGanadora> jugadasGanadorasScatter = new ArrayList<JugadaGanadora>();
	public List<JugadaGanadora> jugadasGanadorasBonus = new ArrayList<JugadaGanadora>();
	public List<JugadaGanadora> jugadasGanadorasWild = new ArrayList<JugadaGanadora>();
	public JugadaGanadoraJackpot jugadaGanadoraJackpot;
	public JugadaGanadoraBonusMenorMayor jugadaGanadoraBonusMenorMayor;
	
	protected List<Rodillo> rodillos = new ArrayList<Rodillo>();
	protected Maquina maquina;
	
	public void inicializarConfiguracion(Maquina maquina) {
		
		//asigno la maquina
		this.maquina = maquina;
		
		//inicializo todo
		inicializarFiguras();
		
		//creamos la figura JACKPOT que es  
		//comun a todas las configuraciones
		Figura figuraJackpot = new Figura(500, "FiguraJackpot", FiguraTipo.JACKPOT);
		figuras.add(figuraJackpot);
		
		//creamos la figura BONUS_MENOR_MAYOR que   
		//es comun a todas las configuraciones
		Figura figuraBonusMenorMayor = new Figura(800, "FiguraBonusMenorMayor", FiguraTipo.BONUS_MENOR_MAYOR);
		figuras.add(figuraBonusMenorMayor);
		
		//validamos que las figuras que
		//hayan sido creadas mantengan
		//una consistencia logica dentro
		//del sistema
		validarFiguras();
		
		//inicializamos las lineas
		inicializarLineas();
		
		//inicializamos las cartas
		inicializarCartas();
		
		//inicializamos las jugadas ganadoras
		inicializarJugadasGanadoras();
		
		//inicializamos los rodillos
		inicializarRodillos();

		//jugada wild si se indica
		//que tiene que estar segun
		//la configuracion del sistema
		if(Sistema.APUESTA_WILD == 1) {
			
			String figuraWildStr = String.valueOf(obtenerFiguraWild().getId());
			String formaJugada = "";
			
			//formamos la forma de la 
			//linea ganadora
			for(int i=0; i<rodillos.size(); i++) {
				
				if(i < (rodillos.size()-1)) {
					formaJugada += figuraWildStr + ",";
				}
				else {
					formaJugada += figuraWildStr;
				}
			}
			
			//creamos la jugada de tipo Wild
			jugadasGanadoras.add(JugadaGanadora.configurarJugadaGanadora(maquina, formaJugada, figuras, JugadaTipo.WILD, 1500, 1));
			
		}

		//ordeno la lista de jugadas ganadoras por
		//su factor multiplicador en orden descendente
		Collections.sort(jugadasGanadoras, new MultiplicadorComparator());
		
		//filtramos en colecciones diferentes 
		//las jugadas ganadoras normales de 
		//las scatter y bonus
		filtrarJugadasGanadorasNormal();
		filtrarJugadasGanadorasScatter();
		filtrarJugadasGanadorasBonus();
		filtrarJugadasGanadorasWild();
		
		//valido los rodillos
		validarRodillos();
		
		//valido las lineas
		validarLineas();
		
		//valido las jugadas ganadoras
		validarJugadasGanadoras();
		
		//configuramos la jugada ganadora JACKPOT
		JugadaGanadoraJackpot jugadaGanadoraJackpot = JugadaGanadoraJackpot.configurarJugadaGanadoraJackpot(maquina);
		if(jugadaGanadoraJackpot != null) {
			jugadasGanadoras.add(jugadaGanadoraJackpot);
			filtrarJugadaGanadoraJackpot();
		}
		
		//configuramos la jugada ganadora BONUS MENOR MAYOR
		JugadaGanadoraBonusMenorMayor jugadaGanadoraBonusMenorMayor = JugadaGanadoraBonusMenorMayor.configurarJugadaGanadoraBonusMenorMayor(maquina);
		if(jugadaGanadoraBonusMenorMayor != null) {
			jugadasGanadoras.add(jugadaGanadoraBonusMenorMayor);
			filtrarJugadaGanadoraBonusMenorMayor();
		}
	}
	
	/**
	 * Agrega en la coleccion 'jugadasGanadorasNormal' 
	 * todas aquellas jugadas ganadoras que el tipo
	 * sea normal
	 */
	private void filtrarJugadasGanadorasNormal() {
		for(JugadaGanadora jugadaGanadora : jugadasGanadoras) {
			if(jugadaGanadora.tipo == JugadaTipo.NORMAL) {
				jugadasGanadorasNormal.add(jugadaGanadora);
			}
		}
	}
	
	/**
	 * Agrega en la coleccion 'jugadasGanadorasScatter' 
	 * todas aquellas jugadas ganadoras que el tipo
	 * sea scatter
	 */
	private void filtrarJugadasGanadorasScatter() {
		for(JugadaGanadora jugadaGanadora : jugadasGanadoras) {
			if(jugadaGanadora.tipo == JugadaTipo.SCATTER) {
				jugadasGanadorasScatter.add(jugadaGanadora);
			}
		}
	}
	
	/**
	 * Agrega en la coleccion 'jugadasGanadorasWild' 
	 * todas aquellas jugadas ganadoras que el tipo
	 * sea wild
	 */
	private void filtrarJugadasGanadorasWild() {
		for(JugadaGanadora jugadaGanadora : jugadasGanadoras) {
			if(jugadaGanadora.tipo == JugadaTipo.WILD) {
				jugadasGanadorasWild.add(jugadaGanadora);
			}
		}
	}
	
	/**
	 * Agrega en la coleccion 'jugadasGanadorasBonus' 
	 * todas aquellas jugadas ganadoras que el tipo
	 * sea bonus
	 */
	private void filtrarJugadasGanadorasBonus() {
		for(JugadaGanadora jugadaGanadora : jugadasGanadoras) {
			if(jugadaGanadora.tipo == JugadaTipo.BONUS) {
				jugadasGanadorasBonus.add(jugadaGanadora);
			}
		}
	}
	
	private void filtrarJugadaGanadoraJackpot() {
		for(JugadaGanadora jugadaGanadora : jugadasGanadoras) {
			if(jugadaGanadora.tipo == JugadaTipo.JACKPOT) {
				jugadaGanadoraJackpot = (JugadaGanadoraJackpot)jugadaGanadora;
				break;
			}
		}
	}

	private void filtrarJugadaGanadoraBonusMenorMayor() {
		for(JugadaGanadora jugadaGanadora : jugadasGanadoras) {
			if(jugadaGanadora.tipo == JugadaTipo.BONUS_MENOR_MAYOR) {
				jugadaGanadoraBonusMenorMayor = (JugadaGanadoraBonusMenorMayor)jugadaGanadora;
				break;
			}
		}
	}

	/**
	 * Valida que entre todos los puntos porcentuales
	 * de las jugadas ganadoras se sume exactamente
	 * 100%
	 * 
	 */
	private void validarJugadasGanadoras() {
		
		//contabiliza la suma de porcentajes
		int porcentajeTotal = 0;
		
		//recorremos la lista de 
		//jugadas ganadoras
		for(JugadaGanadora jugadaGanadora : jugadasGanadoras) {
			
			//obtenemos el porcentaje de 
			//distribucion de la jugada
			int porcentajeDistribucion = jugadaGanadora.porcentajeDistribucion;
			
			//incrementamos
			porcentajeTotal += porcentajeDistribucion;
			
		}
		
//		//evalulamos porcentaje total
//		if(porcentajeTotal != 100) {
//			throw new ConfiguracionInvalidaException("El total de los porcentajes de distribucion de las jugadas ganadoras suman " + porcentajeTotal + "% en vez de 100%");
//		}
	}
	
	/**
	 * Valido que el formato de las lineas
	 * sea el mismo. 
	 * Por ejemplo 5 rodillos x 3 posiciones:
     *
	 * 				XXXXX
	 * 				XXXXX
	 * 				XXXXX 
	 */
	private void validarLineas() {
		
		//obtengo el tamano modelo de la linea
		int tamanoX = lineas.get(0).getForma().length;
		int tamanoY = lineas.get(0).getForma()[0].length;
		
		for(Linea linea : lineas) {
			
			int tamanoActualX = linea.getForma().length;
			int tamanoActualY = linea.getForma()[0].length;
			
			if(tamanoActualX != tamanoX || tamanoActualY != tamanoY) {
				
				throw new ConfiguracionInvalidaException("El formato de las lineas es diferente entre si");
			}	
		}
	}
	
	/**
	 * Valida que todo se encuentre
	 * correctamente entre las figuras
	 * agregadas a la maquina
	 */
	private void validarFiguras() {
		
		/**
		 * Hacemos una primer validacion de 
		 * elementos no repetidos por su id
		 */
		for(Figura figura : figuras) {
			
			//contador de repeticiones
			//de la figura actual
			int repeticionesFigura = 0;
			
			//recorremos nuevamente la lista
			//para comparar el id de figura
			//actual con el del segundo recorrido
			for(Figura figuraComparar : figuras) {
				
				//comparacion de id's de figura
				if(figura.getId() == figuraComparar.getId()) {
					
					//es igual... incrementamos
					repeticionesFigura++;
				}
			}
			
			/**
			 * Si hay mas de una repeticion
			 * existe error debido a que solo
			 * puede haber una figura con el
			 * mismo id
			 */
			if(repeticionesFigura > 1) {
				throw new ConfiguracionInvalidaException("Solo puede haber un mismo ID de figura por maquina. ID Figura=" + figura.getId());
			}
		}
	}

		/**
	 * Valido que la cantidad de figuras de 
	 * cada rodillo sea la misma para todos
	 */
	private void validarRodillos() {
		
		int rodilloFiguraCant = rodillos.get(0).getFiguras().size();
		
		for(Rodillo rodilloActual : rodillos) {
			
			//validamos que todos los rodillos 
			//contengan la misma cantidad de 
			//figuras
			if(rodilloActual.getFiguras().size() != rodilloFiguraCant) {
				throw new ConfiguracionInvalidaException("La cantidad de figuras por rodillo es diferente entre si");
			}
			
			//recorremos las figuras del rodillo
			//para chequear que no hayan figuras
			//de tipo JACKPOT, ya que esas figuras
			//no deben de ser agregadas
//			for(Figura figuraActual : rodilloActual.getFiguras()) {
//				if(figuraActual.getTipo() == FiguraTipo.JACKPOT) {
//					String mensajeErrorJackpot = "No se permiten figuras JACKPOT en los rodillos. " +
//												 "Esto sucede debido a que los premios de tipo JACKPOT " +
//												 "son premios generados de un modo ficticio, y no usando " +
//												 "las figuras de los rodillos.";
//					throw new ConfiguracionInvalidaException(mensajeErrorJackpot);
//				}
//			}
		}
	}
	
	/**
	 * Inicializa las cartas. Actualmente es
	 * un sistema bastante simple ya que todas
	 * las configuraciones comparten la misma
	 * configuracion de cartas definidas aqui.
	 * Cada carta posee un id el cual es 
	 * identificador de la carta, y tambien
	 * sirve como comparador en el juego menor
	 * o mayor
	 */
	public final void inicializarCartas() {
		
		cartas.add(new Carta(1));
		cartas.add(new Carta(2));
		cartas.add(new Carta(3));
		cartas.add(new Carta(4));
		cartas.add(new Carta(5));
		cartas.add(new Carta(6));
		cartas.add(new Carta(7));
		cartas.add(new Carta(8));
		cartas.add(new Carta(9));
		cartas.add(new Carta(10));
		cartas.add(new Carta(11));
		cartas.add(new Carta(12));
		
	}
	
	
	/**
	 * Inicializa las lineas validas de la maquina
	 *
	 * 			  !!! ATENCION !!!
	 * 
	 * No modificar esta configuracion. La forma de 
	 * las lineas se encuentra directamente ligado
	 * con el codigo que las dibuja en pantalla, una
	 * modificacion a este metodo no lo refleja
	 * directamente en el renderizado de las mismas.
	 */
	public final void inicializarLineas() {
		
		lineas.add(new Linea(1, new int[][] {{0,0,0,0,0},
			  	 							 {1,1,1,1,1},
			  	 							 {0,0,0,0,0}}));
		
		lineas.add(new Linea(2, new int[][] {{1,1,1,1,1},
						  	 				 {0,0,0,0,0},
						  	 				 {0,0,0,0,0}}));
		
		lineas.add(new Linea(3, new int[][] {{0,0,0,0,0},
					  	 					 {0,0,0,0,0},
					  	 					 {1,1,1,1,1}}));
		
		lineas.add(new Linea(4, new int[][] {{1,1,0,0,0},
						  	 				 {0,0,1,0,0},
						  	 				 {0,0,0,1,1}}));
		
		lineas.add(new Linea(5, new int[][] {{0,0,0,1,1},
						  	 			 	 {0,0,1,0,0},
						  	 			 	 {1,1,0,0,0}}));
		
		lineas.add(new Linea(6, new int[][] {{1,1,1,1,0},
											 {0,0,0,0,1},
											 {0,0,0,0,0}}));
		
		lineas.add(new Linea(7, new int[][] {{0,0,0,0,0},
					  	 					 {0,0,0,0,1},
					  	 					 {1,1,1,1,0}}));
		
		lineas.add(new Linea(8, new int[][] {{0,0,0,0,0},
						  	 				 {1,1,0,1,1},
						  	 				 {0,0,1,0,0}}));
		
		lineas.add(new Linea(9, new int[][] {{0,0,1,0,0},
					  	 					 {1,1,0,1,1},
					  	 					 {0,0,0,0,0}}));		
		
		lineas.add(new Linea(10, new int[][] {{0,1,0,0,0},
					 						  {1,0,1,0,1},
					 						  {0,0,0,1,0}}));
		
	}
	
	/**
	 * Inicializa la coleccion de figuras
	 */
	protected abstract void inicializarFiguras();
	
	/**
	 * Inicializa la coleccion con jugadas ganadoras.
	 * Cabe aclarar que las jugadas ganadoras son evaluadas
	 * de izquierda a derecha comenzando por el primer reel.
	 * No es necesario definir en las jugadas ningun tipo
	 * de combinacion con comodines, esto se evalua
	 * automaticamente al finalizar cada tirada.
	 * 
	 */
	protected abstract void inicializarJugadasGanadoras();
	
	/**
	 * Inicializa los rodillos con las figuras correspondientes
	 */
	protected abstract void inicializarRodillos();
	
	public List<JugadaGanadora> getJugadasGanadoras() {
		return jugadasGanadoras;
	}
	public void setJugadasGanadoras(List<JugadaGanadora> jugadasGanadoras) {
		this.jugadasGanadoras = jugadasGanadoras;
	}
	public List<Rodillo> getRodillos() {
		return rodillos;
	}
	public void setRodillos(List<Rodillo> rodillos) {
		this.rodillos = rodillos;
	}
	public List<Linea> getLineas() {
		return lineas;
	}
	public List<Carta> getCartas() {
		return cartas;
	}
	public void setLineas(List<Linea> lineas) {
		this.lineas = lineas;
	}
	public List<Figura> getFiguras() {
		return figuras;
	}
	public void setFiguras(List<Figura> figuras) {
		this.figuras = figuras;
	}

	public Figura obtenerFiguraPorId(int id) {
		
		Figura f = null;
		
		for(Figura figura : figuras) {
			if(figura.getId() == id) {
				f = figura;
				break;
			}
		}
		
		return f;
	}
	
	public Figura obtenerFiguraBonus() {
		
		for(Figura figura : figuras) {
			if(figura.getTipo() == FiguraTipo.BONUS) {
				return figura;
			}
		}
		
		return null;
	}
	
	public Figura obtenerFiguraJackpot() {
		
		for(Figura figura : figuras) {
			if(figura.getTipo() == FiguraTipo.JACKPOT) {
				return figura;
			}
		}
		
		return null;
	}

	public Figura obtenerFiguraBonusMenorMayor() {
		
		for(Figura figura : figuras) {
			if(figura.getTipo() == FiguraTipo.BONUS_MENOR_MAYOR) {
				return figura;
			}
		}
		
		return null;
	}

	public Figura obtenerFiguraWild() {
		
		for(Figura figura : figuras) {
			if(figura.getTipo() == FiguraTipo.WILD) {
				return figura;
			}
		}
		
		return null;
	}
	
	public Figura obtenerFiguraScatter() {
		
		for(Figura figura : figuras) {
			if(figura.getTipo() == FiguraTipo.SCATTER) {
				return figura;
			}
		}
		
		return null;
	}
	
	/**
	 * Devuelve la representacion de los rodillos
	 * y sus figuras en un arreglo bidimensional
	 * @return
	 */
	private int[][] obtenerArregloRodillos() {
		
		int[][] arreglo = new int[getRodillos().size()][getRodillos().get(0).getFiguras().size()];
		
		//recorremos la lista de rodillos
		//para luego obtener sus figuras
		for(int i=0; i<rodillos.size(); i++) {
			
			//obtenemos el rodillo actual
			Rodillo rodillo = rodillos.get(i);
			
			//obtenemos las figuras del rodillo actual
			List<Figura> figuras = rodillo.getFiguras();
			
			//recorremos las figuras
			for(int j=0; j<figuras.size(); j++) {
			
				Figura figura = figuras.get(j);
				arreglo[i][j] = figura.getId();
			}
		}
		
		return arreglo;
	}
	
	/**
	 * Devuelve la cantidad de figuras de determinado 
	 * tipo que hay en un rodillo dado su id 
	 * @return
	 */
	public int obtenerCantidadFigurasRodilloPorId(int rodilloId, int figuraId) {
		
		int cantidadFiguras = 0;
		
		for(Rodillo rodillo : rodillos) {
			if(rodillo.getId() == rodilloId) {
				
				//recorremos la lista de figuras
				//que componen el rodillo
				List<Figura> figurasRodillo = rodillo.getFiguras();
				for(Figura figura : figurasRodillo) {
					if(figura.getId() == figuraId) {
						cantidadFiguras++;
					}
				}
				
				//salimos del rodillo
				break;
			}
		}
		
		return cantidadFiguras;
	}	
	
	/**
	 * Funcion para calculo de probabilidad es 
	 * de la maquina y la configuracion elegida
	 * en particular
	 * 
	 * @return
	 */
	public void inicializarCalculoProbabilidades() {

		//obtenemos la referencia al 
		//modulo probabilistico
		ProbabilidadMaquina probabilidad = maquina.probabilidad;

		//obtengo la cantidad de rodillos
		int cantidadRodillos = rodillos.size();
		
		//obtengo la cantidad de figuras por rodillo
		int figurasPorRodillo = rodillos.get(0).getFiguras().size();
		
		//calculo el monto de apuestas con valor '1'
		//necesario para realizar todas las combinaciones
		//posibles de todas las figuras y todos los rodillos
		int montoCombinacionesTotales = (int)Math.pow(figurasPorRodillo, cantidadRodillos);
		
		//calculo el monto pago por todas las 
		//combinaciones ganadoras existentes
		int montoCombinacionesGanadoras = 0;
		
		//Idem. pero contiene el monto ganable
		//solamente de las jugadas de tipo NORMAL
		int montoCombinacionesGanadorasNormales = 0;
		
		//obtengo la figura wild
		Figura figuraWild = obtenerFiguraWild();
		
		//obtengo la figura bonus
		Figura figuraBonus = obtenerFiguraBonus();
		
		//obtengo la figura scatter
		Figura figuraScatter = obtenerFiguraScatter();

		//arreglo bidimensional que identifica
		//su primer dimension con el id de rodillo, y
		//su segunda dimension contiene los id de figura
		//que componen cada rodillo
		int[][] arregloRodillos = obtenerArregloRodillos();
		
		//lista de indices de figuras de los rodillos 
		ArrayList<Integer> array = new ArrayList<Integer>();
		for(int i=0; i<figurasPorRodillo; i++) {
			array.add(i);
		}
		
		//inicializamos la lista sobre la que se
		//analizaran cuales seran todas las 
		//posibles combinaciones
		CombinatoricsVector<Integer> initialVector = new CombinatoricsVector<Integer>(array);
		
		//create permutation with repetition generator, second parameter is a number of slots
		Generator<Integer> gen = new PermutationWithRepetitionGenerator<Integer>(initialVector, cantidadRodillos);

		//creamos iterador
		Iterator<CombinatoricsVector<Integer>> itr = gen.createIterator();

		//registramos en las probabilidades
		//la cantidad de combinaciones posibles
		probabilidad.combinaciones = gen.getNumberOfGeneratedObjects();
		
		//configuramos el maximo apostable 
		//como la cantidad de combinaciones
		//posibles de la maquina, de esta 
		//manera indicamos que se puede hacer
		//una apuesta por combinacion con valor
		//de una unidad por apuesta
		probabilidad.maximoApostable = probabilidad.combinaciones;
		
		
		//imprimimos la cantidad de permutaciones
//		System.out.println("Cantidad combinaciones: " + probabilidad.combinaciones);

		//guarda la combinacion actual de ids de figuras 
		int[] combinacionActual = new int[cantidadRodillos];
		
		//chequeo el tamano vertical de las lineas
		int tamanoY = maquina.lineas.get(0).getForma().length;
		
		//guarda la combinacion actual mas las 
		//dos siguientes lineas que conforman
		//lo visible de los rodillos en la pantalla
		int[][] combinacionGlobal = new int[cantidadRodillos][tamanoY];
		
		/**
		 * Iteramos sobre cada posible combinacion
		 * de figuras en los rodillos para chequear
		 * cuantas combinaciones ganadoras hay y
		 * que premio devuelve el conjunto de todos
		 * los resultados de las jugadas ganadoras
		 */
		while (itr.hasNext()) {
			
			//obtenemos la permutacion actual
			CombinatoricsVector<Integer> permutation = itr.next();
			
			//cargamos los id de figura correspondientes 
			//a los indices indicados por la permutacion
			for(int i=0; i<cantidadRodillos; i++) {
				combinacionActual[i] = arregloRodillos[i][permutation.getValue(i)];
			}
			
			//cargamos los id de figura correspondientes 
			//a los indices indicados por la permutacion
			//y tambien las dos 'lineas' de figuras 
			//visibles siguientes a la permutacion actual
			for(int i=0; i<cantidadRodillos; i++) {
				
				//obtenemos el indice de inicio
				//del rodillo actual
				int indiceInicio = permutation.getValue(i);
				
				//recorremos cada figura visible 
				//del rodillo actual
				for(int j=0; j<combinacionGlobal[0].length; j++) {
					
					if(j==0) {
						combinacionGlobal[i][j] = arregloRodillos[i][indiceInicio];
					}
					else {
						
						for(int k=1; k<combinacionGlobal[0].length; k++) {
							
							int indiceActual = indiceInicio + k;
							if(indiceActual > (figurasPorRodillo-1)) {
								indiceActual = indiceActual % figurasPorRodillo;
							}
							combinacionGlobal[i][k] = arregloRodillos[i][indiceActual];
						}
					}
				}
			}
			
			//flag para indicar que ya se ha encontrado
			//una jugada ganadora para la combinacion 
			//actual y que se puede seguir a la proxima
			//combinacion
			boolean esJugadaGanadora = false;
			
			//recorremos la lista de jugadas ganadoras de 
			//tipo 'NORMAL' para chequear si la combinacion  
			//actual tiene premio
			for(JugadaGanadora jugadaGanadoraNormal : jugadasGanadorasNormal) {
	
				//chequeamos que la combinacion actual 
				//sea una jugada ganadora valida
				if(jugadaGanadoraNormal.esJugadaNormalGanadora(combinacionActual, (figuraWild != null ? figuraWild.getId() : null) )) {
					
					//agregamos al monto de combinaciones
					//el multiplicador de la jugada ganadora
					montoCombinacionesGanadoras += jugadaGanadoraNormal.multiplicador;
					
					//incrementamos solo el valor de 
					//jugadas ganadoras normales
					montoCombinacionesGanadorasNormales += jugadaGanadoraNormal.multiplicador;
					
					//registramos la probabilidad de 
					//la jugada ganadora actual
					probabilidad.registrarJugadaGanadora(jugadaGanadoraNormal);
					
					//seteamos el flag para salir
					esJugadaGanadora = true;
				}
				
				//chequeamos si ya se asigno un
				//premio a la combinacion actual
				if(esJugadaGanadora) {
					break;
				}
			}
			
			//reinicializamos el flag para 
			//el recorrido de jugadas scatter
			esJugadaGanadora = false;
			
			//recorremos la lista de jugadas ganadoras de 
			//tipo 'SCATTER' para chequear si la combinacion  
			//actual tiene premio
			for(JugadaGanadora jugadaGanadoraScatter : jugadasGanadorasScatter) {
				
				//chequeamos que la combinacion actual 
				//sea una jugada ganadora valida
				if(jugadaGanadoraScatter.esJugadaScatterGanadora(combinacionGlobal, figuraScatter.getId())) {
					
					//agregamos al monto de combinaciones
					//el multiplicador de la jugada ganadora
					montoCombinacionesGanadoras += jugadaGanadoraScatter.multiplicador;
					
					//registramos la probabilidad de 
					//la jugada ganadora actual
					probabilidad.registrarJugadaGanadora(jugadaGanadoraScatter);

					//seteamos el flag para salir
					esJugadaGanadora = true;
				}
				
				//chequeamos si ya se asigno un
				//premio a la combinacion actual
				if(esJugadaGanadora) {
					break;
				}
			}
		}
		
		/**
		 * Recorremos la lista de objetos
		 * ProbabilidadJugadaGanadora para
		 * setearle a algunas variables 
		 * algunos calculos
		 */
		for(ProbabilidadJugadaGanadora probabilidadJugadaGanadora : probabilidad.probabilidadJugadasGanadoras) {
			
			long multiplicaciones = probabilidadJugadaGanadora.multiplicaciones;
			float probabilidadEntreGanadoras = (float)(multiplicaciones * 100f) / (float)montoCombinacionesGanadoras;
			
			//asignamos el valor al objeto
			probabilidadJugadaGanadora.porcentajePagoEntreGanadoras = probabilidadEntreGanadoras;
			
			/**
			 * Calculos solo para jugadas ganadoras
			 * de tipo NORMAL
			 */
			if(probabilidadJugadaGanadora.jugadaGanadora.tipo == JugadaTipo.NORMAL) {
				
				float probabilidadEntreGanadorasNormales = (float)(multiplicaciones * 100) / (float)montoCombinacionesGanadorasNormales;
				
				//asignamos el valor al objeto
				probabilidadJugadaGanadora.porcentajePagoEntreGanadorasNormales = probabilidadEntreGanadorasNormales;

			}
		}
		
		//configuramos el maximo de dinero
		//ganable en la variable de probabilidad
		probabilidad.maximoPagable = montoCombinacionesGanadoras;
		
		//calculamos la probabilidad 
		//de pago del jugador
		float probabilidadPagoJugador = ((float)montoCombinacionesGanadoras*100f)/(float)montoCombinacionesTotales;
		float probabilidadPagoMaquina = 100 - probabilidadPagoJugador;
		
		probabilidad.probabilidadJugador = probabilidadPagoJugador;
		probabilidad.probabilidadMaquina = probabilidadPagoMaquina;
		
		System.out.println("Tot: " + montoCombinacionesTotales);
		System.out.println("Gan: " + montoCombinacionesGanadoras);
		System.out.println("%: " + probabilidadPagoJugador);
		
	}
}