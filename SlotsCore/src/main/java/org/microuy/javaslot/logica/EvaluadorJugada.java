package org.microuy.javaslot.logica;

import java.util.List;

import org.microuy.javaslot.constante.Sistema;
import org.microuy.javaslot.dominio.JugadaLineaGanadora;
import org.microuy.javaslot.dominio.Maquina;

public class EvaluadorJugada {
	
	private Maquina maquina;
	
	public EvaluadorJugada(Maquina maquina) {
		this.maquina = maquina;
	}

	/**
	 * Evaluador de las jugadas ganadoras. 
	 * Encargado de chequear si las lineas
	 * ganadoras pueden ser pagadas por el
	 * sistema o no. Esta evaluacion esta 
	 * dada por la configuracion de premiacion
	 * de la maquina, o sea sus porcentajes
	 * de pago. Si la maquina esta pasada
	 * en pagos cancela lineas ganadoras, 
	 * si esta dentro los limites lo paga.
	 * 
	 * Devuelve TRUE si las jugadas  
	 * deben de ser canceladas
	 * 
	 * @param jugadasLineasGanadoras Las Jugadas a ser evaluadas
	 */
	public boolean evaluarCancelacionJugadasLineasGanadoras(List<JugadaLineaGanadora> jugadasLineasGanadoras) {
		
		boolean cancelaJugadas = false;
		
		//si hay jugadas ganadoras
		if(jugadasLineasGanadoras.size() > 0) {
			
			/**
			 * Si el sistema de control de pago se
			 * encuentra en baja en el momento en
			 * el que la apuesta es ejecutada, entonces
			 * cualquier premio generado por los rodillos
			 * es directamente cancelado
			 */
			if(maquina.logicaMaquina.sistemaControlPago.valorFuncionControlPago <= 0) {
				
				//damos por canceladas las jugadas,
				//por ende las borramos todas de la
				//lista de jugadas ganadoras
				jugadasLineasGanadoras.clear();
				
				//seteamos flag que indica
				//que se cancelaron jugadas
				cancelaJugadas = true;
				
			}
		}
		
		return cancelaJugadas;
	}
}