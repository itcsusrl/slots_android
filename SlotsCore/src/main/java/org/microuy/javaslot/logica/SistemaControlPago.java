package org.microuy.javaslot.logica;

import java.util.HashMap;
import java.util.Map;

import org.microuy.javaslot.constante.Sistema;
import org.microuy.javaslot.dominio.Maquina;
import org.microuy.javaslot.dominio.ResumenCobranza;

public class SistemaControlPago extends Thread {

	//referencia
	private LogicaMaquina logicaMaquina;
	
	/**
	 * Porcentaje de pago real actual
	 * de la maquina, basado en calculos
	 * hechos por el metodo
	 * 'calcularPorcentajePagoRealActualJugador'
	 */
	public float porcentajePagoRealActualJugador;
	
	/**
	 * Shifter usado por la funcion sinusoidal
	 * para correr el eje en Y en base al 
	 * porcentaje de pago actual de la maquina
	 */
	public int shifterY;
	
	/**
	 * El tiempo que lleva ejecutandose
	 * el sistema de control de pago.
	 * Esta variable se ve incrementada
	 * de a un segundo por esta clase
	 * que corre con un thread del 
	 * sistema
	 */
	private float tiempo;
	
	/**
	 * Valor calculado por la funcion 
	 * 'calcularFuncionControlPago'
	 *  
	 */
	public float valorFuncionControlPago;
	
	/**
	 * PERIODICIDAD JUGADOR REAL
	 * Define la periodicidad en segundos 
	 * de la funcion sinusoidal. 
	 * De pico a pico demora el valor 
	 * definido en esta constante
	 */
	public static long PERIODICIDAD_JUGADOR_REAL_SEG = 100;

	/**
	 * PERIODICIDAD JUGADOR BOT
	 * Define la periodicidad en milisegundos
	 * de la funcion sinusoidal
	 * De pico a pico demora el valor 
	 * definido en esta constante
	 */
	public static long PERIODICIDAD_JUGADOR_BOT_MS = 100;

	public SistemaControlPago(Maquina maquina) {
		
		//referenciamos variables
		logicaMaquina = maquina.logicaMaquina;
		
		//inicializa la tabla con las
		//constantes usadas por el 
		//shifter Y
		inicializarTablaShifterY();
		
		//comienza la ejecucion del hilo
		start();
	}
	
	/**
	 * Tabla contenedora de los valores usados
	 * para el desplazamiento en Y en la funcion
	 * sinusoidal en base al porcentaje de pago
	 * de la maquina, y al porcentaje actual de 
	 * pago de la maquina.
	 * 
	 * La key de la tabla principal es un byte que
	 * representa valores del 0 al 10, los cuales 
	 * indican el porcentaje de pago de la maquina
	 * que se desea configurar. 
	 * Por ejemplo, si la maquina se encuentra 
	 * configurada en 70% para el jugador, esta
	 * tabla debe definir los valores del Shifter
	 * en Y en la key con valor 7.
	 * El value de este ultimo valor es otra tabla,
	 * en la cual la key es el porcentaje de pago
	 * actual de la maquina (no el configurado), y
	 * el value es el valor de desplazamiento en Y
	 * (ShifterY)
	 */
	private Map<Integer, Map<Integer, Integer>> TABLA_SHIFTER_Y;
	
	/**
	 * Inicializa la tabla con los valores
	 * de corrimiento en Y. Esta tabla 
	 * actualmente esta hard-coded en el
	 * sistema, y presenta valores diferentes
	 * en base a los diferentes porcentajes
	 * de pago de la maquina
	 */
	private void inicializarTablaShifterY() {
		
		//instanciamos
		TABLA_SHIFTER_Y = new HashMap<Integer, Map<Integer,Integer>>();
		
		/**
		 * % PAGO -> 70 - 79
		 */
		Map<Integer, Integer> tabla7 = new HashMap<Integer, Integer>();
		tabla7.put(0, 35);
		tabla7.put(1, 30);
		tabla7.put(2, 25);
		tabla7.put(3, 20);
		tabla7.put(4, 15);
		tabla7.put(5, 10);
		tabla7.put(6, 5);
		tabla7.put(7, 0);
		tabla7.put(8, -20);
		tabla7.put(9, -45);
		TABLA_SHIFTER_Y.put(7, tabla7);
		
	}
	
	@Override
	public void run() {
		
		while(true) {
		
			try {

				//incrementamos el contador de tiempo
				tiempo += 1;

				//si es el jugado real
				if(!logicaMaquina.maquina.test) {
					//dormimos el thread por el periodo
					sleep(1000);
				}
				else {
					//es el jugador bot
					sleep(1);
				}
				
			} 
			catch (InterruptedException e) {
				e.printStackTrace();
			}			
		}	
	}
	
	/**
	 * Calcula cual es el porcentaje de pago 
	 * real actual al jugador por parte de 
	 * la maquina
	 * 
	 * @return
	 */
	private void calcularPorcentajePagoRealActualJugador() {
		
		//obtenemos el ultimo resumen
		ResumenCobranza resumenCobranza = logicaMaquina.obtenerUltimoResumen();
		
		/**
		 * Hasta que no haya un resumen de cobranza 
		 * disponible no podemos analizar nada 
		 * relativo a la entrada y salida de dinero
		 */
		if(resumenCobranza != null) {
			
			//monedas que ingresaron y 
			//salieron de la maquina  
			float dineroIngresado = (resumenCobranza.entradaMonedasPA + resumenCobranza.entradaMonedasPC) * Sistema.APUESTA_ADMITIDA;
			float dineroEgresado = (resumenCobranza.salidaMonedasPA + resumenCobranza.salidaMonedasPC) * Sistema.APUESTA_ADMITIDA;
			
			//monto ganado por el jugador
			//durante la sesion actual
			float montoPremiosSesionJugador = logicaMaquina.montoPremiosGanadosSesion;
			
			//monto apostado por el jugador
			//durante la sesion actual
			float montoApostadoSesionJugador = logicaMaquina.montoApostadoSesionJugador;
			
			/**
			 * ACUMULADO - Segun formula de Nico en donde:
			 * 
			 * 				A = I + M
			 * 
			 * Acumulado = Ingreso $ + Monto Actual
			 * 
			 */
			float acumulado = dineroIngresado + montoApostadoSesionJugador;
			
			/**
			 * PAGO TOTAL - Segun formula de Nico en donde:
			 * 
			 * 				P = S + Pj
			 * 
			 * Pago Total = Salida $ + Pago Jugador (actual)
			 * 
			 */
			float pagoTotal = dineroEgresado + montoPremiosSesionJugador;
			
			/**
			 * Calculamos el porcentaje de pago actual de la maquina
			 * 
			 * P / A = f(p)
			 * 
			 * Pago Total / Acumulado = f(p)
			 * 
			 */
			if(acumulado > 0) {
				porcentajePagoRealActualJugador = pagoTotal / acumulado * 100f;	
			}
		}
	}
	
	/**
	 * Recalcula el valor para la variable
	 * shifterY, encargada de desplazar la
	 * sinusoide a traves del eje Y en base
	 * al porcentaje de pago real actual de
	 * la maquina 
	 */
	private void calcularShifterY() {
		
		//obtenemos el primer digito
		//del porcentaje de pago 
		//configurado para el jugador
		int primerDigitoPorcentajeConfigurado = Integer.valueOf(String.valueOf(Sistema.PORCENTAJE_JUGADOR).substring(0, 1));
		
		//obtenemos el primer digito
		//del porcentaje de pago actual
		String porcentajePagoRealActualJugadorStr = String.valueOf((int)porcentajePagoRealActualJugador);
		int primerDigitoPorcentajeActual = 0;
		if(porcentajePagoRealActualJugadorStr.length() == 1) {
			primerDigitoPorcentajeActual = 0;	
		}
		else {
			primerDigitoPorcentajeActual = Integer.valueOf(porcentajePagoRealActualJugadorStr.substring(0, porcentajePagoRealActualJugadorStr.length()-1));
		}
		
		//ajuste porque no evaluamos segun la 
		//tabla de pagos ningun porcentaje 
		//mayor a 90 %
		if(primerDigitoPorcentajeActual > 9) {
			primerDigitoPorcentajeActual = 9;
		}
		
		//obtenemos el valor de desplazamiento  
		//en Y basandonos en el porcentaje de
		//pago al jugador configurado, y en el
		//porcentaje de pago actual
		shifterY = TABLA_SHIFTER_Y.get(primerDigitoPorcentajeConfigurado).get(primerDigitoPorcentajeActual);
		
		
	}
	
	/**
	 * Calcula el valor actual en Y de la funcion
	 * de control de pago en base a varios factores,
	 * entre ellos el porcentaje de pago actual de
	 * la maquina
	 */
	private void calcularFuncionControlPago() {
	
		//jugador real
		if(!logicaMaquina.maquina.test) {
			//funcion sinusoidal
			valorFuncionControlPago = 50f * (float)Math.sin((Math.PI * 2d * (1f / ((float)PERIODICIDAD_JUGADOR_REAL_SEG)) * (float)tiempo)) + shifterY;
		}
		else {
			//funcion sinusoidal
			valorFuncionControlPago = 50f * (float)Math.sin((Math.PI * 2d * (1f / ((float)PERIODICIDAD_JUGADOR_BOT_MS/1000f)) * ((float)tiempo / 1000f))) + shifterY;
		}
		
//		System.out.println(valorFuncionControlPago + "\t ____ \t " + (1d/((float)PERIODICIDADMS) / 1000f));

		
	}
	
	/**
	 * Ejecuta las rutinas que ajustan las 
	 * variables usadas en el sistema para
	 * verificar los pagos hechos hasta el
	 * momento por la maquina, y tomar acciones
	 * en base a eso
	 */
	public void ejecutarControlPago() {
		
		//calculamos el monto real de dinero   
		//disponible dentro de la maquina 
		calcularPorcentajePagoRealActualJugador();
		
		//calculamos el desplazamiento en Y 
		//de la onda sinusoidal de la funcion
		calcularShifterY();
		
		//ejecuta la funcion encargada de
		//calcular cual es la posibilidad 
		//del usuario de sacar un premio
		//en base al dinero real disponible
		//dentro de la maquina
		calcularFuncionControlPago();
		
		System.out.println("Valor funcion control pago: " + valorFuncionControlPago);
		System.out.println("Porcentaje pago actual maquina: " + porcentajePagoRealActualJugador);
		System.out.println("SHIFTER ACTUAL: " + shifterY);
		

	}
}