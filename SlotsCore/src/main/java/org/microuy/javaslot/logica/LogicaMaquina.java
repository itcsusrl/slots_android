package org.microuy.javaslot.logica;

import org.microuy.javaslot.comm.listener.ComandoEvent;
import org.microuy.javaslot.comm.listener.ComandoListener;
import org.microuy.javaslot.constante.Comando;
import org.microuy.javaslot.constante.FiguraTipo;
import org.microuy.javaslot.constante.JugadaTipo;
import org.microuy.javaslot.constante.ModoExpulsionMoneda;
import org.microuy.javaslot.constante.Sistema;
import org.microuy.javaslot.constante.SistemaFrontend;
import org.microuy.javaslot.dominio.Carta;
import org.microuy.javaslot.dominio.DistribucionPremio;
import org.microuy.javaslot.dominio.Figura;
import org.microuy.javaslot.dominio.JugadaGanadora;
import org.microuy.javaslot.dominio.JugadaGanadoraBonus;
import org.microuy.javaslot.dominio.JugadaLinea;
import org.microuy.javaslot.dominio.JugadaLineaGanadora;
import org.microuy.javaslot.dominio.Linea;
import org.microuy.javaslot.dominio.Maquina;
import org.microuy.javaslot.dominio.PagoExcedido;
import org.microuy.javaslot.dominio.ResumenCobranza;
import org.microuy.javaslot.dominio.Rodillo;
import org.microuy.javaslot.dominio.estadistica.EstadisticaMaquina;
import org.microuy.javaslot.dominio.estadistica.GanadorTipoJugadaComparator;
import org.microuy.javaslot.persistencia.dao.ConfiguracionDAO;
import org.microuy.javaslot.persistencia.dao.DistribuidorPremiosDAO;
import org.microuy.javaslot.persistencia.dao.EstadisticaDAO;
import org.microuy.javaslot.persistencia.dao.PagoExcedidoDAO;
import org.microuy.javaslot.persistencia.dao.ResumenCobranzaDAO;
import org.microuy.javaslot.util.MathUtil;
import org.microuy.javaslot.util.SlotsRandomGenerator;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

/**
 * Toda la logica correspondiente a la maquina tragamonedas, como
 * ejecutar una apuesta, calcular lineas ganadoras, montos, etc, se
 * realiza en esta clase.
 * 
 * @author Pablo Caviglia
 *
 */
public class LogicaMaquina implements ComandoListener {

	public Maquina maquina;
	
	//cantidad de tiradas gratis
	//generadas por el 'bonus'
	public int cantidadTiradasGratis;

	/**
	 * El monto total entregado a traves de
	 * tiradas gratuitas (bonus)
	 */
	public float montoEntragadoTiradasGratuitas;
	
	/**
	 * Representa una tirada en la maquina.
	 * El indice primario indica el reel y el 
	 * indice secundario indica el valor comenzando
	 * desde la primer linea horizontal
	 * 
	 */
	public int[][] tiradaActualValores;
	
	/**
	 * Representa una tirada en la maquina.
	 * El indice primario indica el rodillo y el 
	 * indice secundario indica el indice dentro
	 * desde la primer linea horizontal
	 * 
	 */	
	public int[] tiradaActualIndices;
	
	
	/**
	 * La tirada ficticia es aquella que fue
	 * modificada con valores especificos que
	 * no tienen porque respetar el orden de
	 * las figuras en los rodillos. Generalmente
	 * es usado cuando se cancelan jugadas porque
	 * se pasan del monto de pago maximo configurado
	 * en la maquina
	 */
	public int[][] tiradaFicticia;
	
	/**
	 * Una vez que se ejecuta una tirada, las posibles
	 * jugadas (ganadoras o no) son almacendas en esta
	 * lista
	 */
	public List<JugadaLinea> jugadasLineas = new ArrayList<JugadaLinea>();
	
	/**
	 * Una vez calculadas las jugadas, se guardan en
	 * esta coleccion la lista de jugadas ganadoras
	 * con su respectivo monto
	 */
	public List<JugadaLineaGanadora> jugadasLineasGanadoras = new ArrayList<JugadaLineaGanadora>();
	
	/**
	 * Valida las jugadas de linea y 
	 * devuelve las jugadas ganadoras
	 */
	public ValidadorJugada validadorJugada;
	
	/**
	 * Evaluador de las jugadas ganadoras. 
	 * Encargado de chequear si las lineas
	 * ganadoras pueden ser pagadas por el
	 * sistema o no. Esta evaluacion esta 
	 * dada por la configuracion de premiacion
	 * de la maquina, o sea sus porcentajes
	 * de pago. Si la maquina esta pasada
	 * en pagos cancela lineas ganadoras, 
	 * si esta dentro los limites lo paga.
	 */
	public EvaluadorJugada evaluadorJugada;
	
	/**
	 * Distribuye montos de premios cancelados
	 * entre las jugadas ganadoras disponibles
	 * en la configuracion usada actualmente
	 * por la maquina. La distribucion se hace
	 * respetando los porcentajes de pago que
	 * posee cada jugada ganadora individualmente.
	 * Ej.: Si una jugada ganadora significa el
	 * 28% de los premios entregados en total entre
	 * todas las jugadas ganadoras, significa que de
	 * 100$ se van a repartir 28$ para esa jugada
	 * ganadora en particular.
	 */
	public DistribuidorPremios distribuidorPremios;
	
	/**
	 * Modulo que corre como Thread, y se encarga
	 * de analizar permanentemente el dinero real
	 * disponible para el usuario en la maquina.
	 * En base a decisiones basadas en el dinero
	 * disponible, toma decisiones de liberar 
	 * premios indiscriminadamente, a pesar de que 
	 * el porcentaje de pago se encuentre en valores
	 * correctos.
	 * Esto debe hacerse debido a que a pesar de que 
	 * los porcentajes de pago internos de la maquina
	 * se encuentran ajustados en los valores correctos,
	 * puede ser que el porcentaje de pago real de dinero
	 * no se encuentre a favor del usuario. 
	 * Logrando detectar esas situaciones, este modulo
	 * decide cuando dar y cuando no nuevos premios
	 * generados por el distribuidor de premios.
	 */
	public SistemaControlPago sistemaControlPago;
	
	/**
	 * Indica si el jugador tiene un juego
	 * bonus 'mayor o menos' disponible para
	 * ser jugado
	 */
	public boolean bonusMenorMayorDisponible;
	
	/**
	 * Indica el monto con el que se
	 * ingresa al bonus menor mayor
	 */
	private float montoIngresoBonusMenorMayor;
	
	/**
	 * Seccion DAO's
	 * En esta seccion definimos la lista de DAO's
	 * usados para conectarnos a la base de datos
	 * para luego persistir datos que queremos que
	 * sean guardados permanentemente
	 */
	private ConfiguracionDAO configuracionDAO;
	private ResumenCobranzaDAO resumenCobranzaDAO;
	private PagoExcedidoDAO pagoExcedidoDAO;
	private DistribuidorPremiosDAO distribuidorPremiosDAO;
	private EstadisticaDAO estadisticaDAO;
	
	/**
	 * Cantidad real de dinero ingresado 
	 * y egresado al sistema por parte 
	 * del usuario
	 */
	public float dineroRealIngresadoSesion;
	public float dineroRealEgresadoSesion;
	
	/**
	 * Cantidad de dinero entregada
	 * EN PREMIOS (no en dinero salido de 
	 * la maquina), que fue entregado
	 * al usuario durante una sesion
	 */
	public float montoPremiosGanadosSesion;
	
	/**
	 * Cantidad de dinero apostado
	 * por el jugador durante una
	 * sesion de juego
	 */
	public float montoApostadoSesionJugador;
	
	/**
	 * Flag que indica si un nuevo
	 * resumen fue creado. Esto es
	 * usado para que cuando se pide
	 * el ultimo resumen se mantenga
	 * uno en memoria para no tener
	 * que ir a la base de datos a 
	 * buscarlo siendo siempre el mismo
	 * que se pide.
	 * Tiene que ser por defecto 'true'
	 * asi la primera vez que se entra
	 * al sistema se va a buscar
	 */
	private boolean resumenCreadoModificado = true;
	
	/**
	 * Ultimo resumen de cobranza mantenido
	 * en memoria para no tener que irlo
	 * a buscar a la base si no cambia
	 */
	public ResumenCobranza ultimoResumenCobranza;
	
	/**
	 * Constructor de clase
	 * @param maquina
	 */
	public LogicaMaquina(Maquina maquina) {
		
		//creo una maquina y le asigno la configuracion
		this.maquina = maquina;
		maquina.logicaMaquina = this;
		
		//creamos el evaluador de jugadas
		evaluadorJugada = new EvaluadorJugada(maquina);
		
		//creamos el validador de jugadas
		validadorJugada = new ValidadorJugada(maquina);
		
		//creamos el distribuidor de premios
		distribuidorPremios = new DistribuidorPremios(maquina);
		
		//creamos el modulo evaluador
		//de dinero real. Este modulo
		//corre como un Thread del sistema
		//debido a que realiza evaluaciones
		//del dinero real disponible a lo
		//largo del tiempo
		sistemaControlPago = new SistemaControlPago(maquina);
		
		//instaciamos los DAO's necesarios
		configuracionDAO = new ConfiguracionDAO();
		resumenCobranzaDAO = new ResumenCobranzaDAO();
		pagoExcedidoDAO = new PagoExcedidoDAO();
		distribuidorPremiosDAO = new DistribuidorPremiosDAO();
		estadisticaDAO = new EstadisticaDAO();
		
		if(!maquina.test) {
			//inicializamos la tabla y los
			//datos de distribucion de premios
			inicializarDatosDistribuidorPremios();
		}
		
		//inicializo la estructura 'tiradaActual'
		inicializarTiradaActual();
		
	}
	
	public void obtenerEstadoMotor() {
	}
	
	public void cambiarEstadoMotor(boolean prendido) {
	}
	
	/**
	 * Realiza las verificaciones pertinentes
	 * chequeando que la informacion de la base
	 * de datos coincida con la configuracion de
	 * la maquina en cuanto a los tipos de jugadas
	 * ganadoras existentes
	 */
	private void inicializarDatosDistribuidorPremios() {
		
		//obtenemos la lista de jugadas 
		//ganadoras cargada desde la 
		//configuracion de la maquina
		List<JugadaGanadora> jugadasGanadorasList = maquina.jugadasGanadoras;
		
        //obtenemos la lista de distribuciones
        //para cada jugada desde la persistencia
        List<DistribucionPremio> distribucionesPremios = distribuidorPremiosDAO.obtenerDistribuciones();

        //flag que indica si se debe de
        //reestablecer la data de la distribucion
        //de premios debido a un cambio en la
        //configuracion de las jugadas ganadoras
        //de la maquina (normalmente durante desarrollo)
        boolean reestablecerDataDistribucion = false;

        //si las dos listas tienen un tama�o
        //diferente significa que debemos de
        //reestablecer la data distributiva
        //debido a un cambio en la configuracion
        if(jugadasGanadorasList.size() != distribucionesPremios.size()) {
            reestablecerDataDistribucion = true;
        }
        else {

            //si el numero de registros coincide,
            //verificamos que cada registro coincida
            //en cuanto a su informacion
            for(DistribucionPremio distribucionPremioActual : distribucionesPremios) {

                //flag para chequer si existe una
                //jugada ganadora correspondiente
                //a la distribucion
                boolean existeFormaJugada = false;

                //recorremos la lista de memoria
                for(JugadaGanadora jugadaGanadoraActual : jugadasGanadorasList) {

                    //comparamos datos
                    if(distribucionPremioActual.formaJugada.equals(jugadaGanadoraActual.obtenerFormateado())) {

                        //existe un par
                        existeFormaJugada = true;

                        //no es necesario seguir recorriendo
                        //jugadas ganadoras, volvemos al for
                        //de distribuciones
                        break;
                    }
                }

                //si no existe una jugada que se
                //corresponda con la distribucion
                //actual, entonces tenemos diferencias
                if(!existeFormaJugada) {
                    //si es distinto, seteamos flag
                    reestablecerDataDistribucion = true;
                }
            }
        }

        //si el flag se encuentra seteado
        //debemos de reestablecer la data
        //distributiva
        if(reestablecerDataDistribucion) {

            //borramos el contenido integro
            //de la tabla distributiva
            borrarTodasDistribuciones();

            //insertamos las distribuciones
            //desde cero (sin acumulado)
            for(JugadaGanadora jugadaGanadoraActual : jugadasGanadorasList) {

                //lo insertamos
                distribuidorPremiosDAO.crearDistribucion(jugadaGanadoraActual.obtenerFormateado());
            }
        }
        else {

            //limpiamos los valores de premios
            //distribuidos de memoria, para ser
            //reemplazados por los valores obtenidos
            //desde la persistencia
            distribuidorPremios.premiosDistribuidos.clear();

            //si no se deben de reestablecer los
            //valores de distribucion, cargamos en
            //memoria los valores de la persistencia
            for(DistribucionPremio distribucionPremioActual : distribucionesPremios) {

                for(JugadaGanadora jugadaGanadoraActual : jugadasGanadorasList) {

                    //si se iguala el string de
                    //la forma de la jugada
                    if(distribucionPremioActual.formaJugada.equals(jugadaGanadoraActual.obtenerFormateado())) {

                        //seteamos el valor en memoria
                        distribuidorPremios.premiosDistribuidos.put(jugadaGanadoraActual, distribucionPremioActual.acumulado);

                        //no es necesario seguir recorriendo
                        //jugadas ganadoras, volvemos al for
                        //de distribuciones
                        break;
                    }
                }
            }
        }
	}
	
	/**
	 * Borra todas las distribuciones
	 * de premios en la persistencia
	 */
	public void borrarTodasDistribuciones() {
        //borramos los valores del
        //distribuidor en memoria
        Iterator<JugadaGanadora> jugadaGanadoraIt = distribuidorPremios.premiosDistribuidos.keySet().iterator();
        while(jugadaGanadoraIt.hasNext()) {

            JugadaGanadora jugadaGanadora = jugadaGanadoraIt.next();
            distribuidorPremios.premiosDistribuidos.put(jugadaGanadora, 0f);
        }

        //se borran todas las distribuciones
        //de premios hechas en la persistencia
        distribuidorPremiosDAO.borrarTodasDistribuciones();
	}
	
	/**
	 * Actualiza en la base de datos el monto
	 * acumulable de la jugada indicada 
	 * @param jugadaFormateada
	 * @param monto
	 */
	public void persistirDistribucionPremio(String jugadaFormateada, float monto) {
        //ejecuta el cambio en
        //la persistencia
        distribuidorPremiosDAO.actualizarAcumulable(jugadaFormateada, monto);
	}
	
	private void inicializarTiradaActual() {
		
		//inicializo el array de la tirada actual
		//con el tamano de una linea de ejemplo
		int tamanoX = maquina.lineas.get(0).getForma().length;
		int tamanoY = maquina.lineas.get(0).getForma()[0].length;
		
		//creo la estructura para guardar una tirada
		tiradaActualValores = new int[tamanoX][tamanoY];
		tiradaActualIndices = new int[maquina.rodillos.size()];

	}
	
	/**
	 * Ejecutor de la apuesta.
	 * Devuelve el monto del jugador luego
	 * de realizada la apuesta 
	 */
	public synchronized float apostarSlots(boolean demo) {

		//obtengo la cantidad de rodillos para 
		//analizar la cantidad de numeros aleatorios
		//a generar
		int cantRodillos = maquina.rodillos.size();
		
		//chequeo el tamano vertical de las lineas
		int tamanoY = maquina.lineas.get(0).getForma().length;
		
		//obtengo la cantidad de figuras por rodillo
		int cantFigurasRodillo = maquina.rodillos.get(0).getFiguras().size();
		
		//recorro la cantidad de rodillos para generar 
		//los numeros aleatorios necesarios
		for(int i=0; i<cantRodillos; i++) {
			
			//genero el numero aleatorio para el rodillo actual
			int numeroAleatorio = (int)(SlotsRandomGenerator.getInstance().nextFloat() * cantFigurasRodillo)+1;
			
			//obtengo el rodillo actual
			Rodillo rodilloActual = maquina.rodillos.get(i);
			List<Figura> figurasRodillo = rodilloActual.getFiguras();
			
			//cargo la linea superior de la tirada con 
			//los id y los indices de las figuras
			tiradaActualValores[0][i] = figurasRodillo.get(numeroAleatorio-1).getId();
			tiradaActualIndices[i] = numeroAleatorio;
			
			//obtengo el numero correspondiente a la
			//siguiente figura dentro del mismo rodillo
			int siguienteFiguraIndex = numeroAleatorio - 1;
			
			//recorro verticalmente cada rodillo
			for(int j=1; j<tamanoY; j++) {
				
				//incremento el id para la siguiente figura
				siguienteFiguraIndex++;
				
				//chequeo si la siguiente figura es mayor
				//al tamano de figuras dentro del rodillo.
				//En caso de que sea mayor comienza en 0 de nuevo
				if(siguienteFiguraIndex > (cantFigurasRodillo-1)) {
					siguienteFiguraIndex = 0;
				}
				
				//cargo la linea siguiente a la superior (siguienteFiguraIndex++)
				tiradaActualValores[j][i] = figurasRodillo.get(siguienteFiguraIndex).getId();
				
			}
		}

		if(demo) {

			//configuramos el maximo de lineas
			//y el maximo apostable por linea
			maquina.lineasApostadas = (short)maquina.lineas.size();
			maquina.dineroApostadoPorLinea = Sistema.APUESTA_MAXIMA_POR_LINEA;
			
			//limpio la jugada anterior
			jugadasLineas.clear();
			
			//generamos un premio en modo demo
			distribuidorPremios.generarJugadaGanadoraDemo();
			
			//buscamos la posicion adecuada para dar 
			//solamente el premio que nos devuelve en 
			//una sola linea
			tiradaFicticia = distribuidorPremios.tiradaFicticia;
			
			//agregamos la jugada que fue generada  
			//a la lista de jugadas ganadoras
			List<JugadaLineaGanadora> jugadas = new ArrayList<JugadaLineaGanadora>();
			jugadas.add(distribuidorPremios.premioGenerado);
	
			//copiamos la lista 
			//de jugadas ganadoras
			jugadasLineasGanadoras = new ArrayList<JugadaLineaGanadora>(jugadas);
			
			return 0f;
		}
		else {

			//flag que indica si la 
			//apuesta actual es gratuita
			boolean apuestaGratuita = false;
			
			//si la tirada actual es gratuita o
			//'bonus', no decrementamos el monto
			//de apuesta del jugador
			if(cantidadTiradasGratis == 0) {
				
				//calculamos cual es el 
				//monto apostado
				float apostado = maquina.lineasApostadas * maquina.dineroApostadoPorLinea;
				
				//decremento el monto apostado
				decrementarMontoJugador(apostado);

				//incrementamos el monto apostado
				//durante la sesion del jugador
				montoApostadoSesionJugador += apostado;

				//actualizamos el valor de configuracion
				actualizarValorConfiguracion(ConfiguracionDAO.MONTO_APOSTADO_SESION_JUGADOR, ""+montoApostadoSesionJugador);

			}
			else {
				
				/**
				 * DESHABILITAMOS ESTO PARA QUE EL
				 * MONTO DE APUESTA PARA TIRADAS
				 * GRATUITAS SEA BASADO EN EL MONTO
				 * APOSTADO POR EL JUGADOR EN LA 
				 * ULTIMA TIRADA
				 */
//				//configuro los nuevos valores 
//				//de apuesta debido a que es bonus
//				maquina.lineasApostadas = Sistema.CANTIDAD_LINEAS_APUESTA_GRATUITA;
//				maquina.setDineroApostadoPorLinea(Sistema.MONTO_APUESTA_GRATUITA);

				//configuramos el flag
				apuestaGratuita = true;
				
				//decrementamos la cantidad
				//de tiradas gratuitas
				cantidadTiradasGratis--;
			}
			
			//borramos la tirada ficticia
			tiradaFicticia = null;
			distribuidorPremios.tiradaFicticia = null;
			distribuidorPremios.premioGenerado = null;
			
			//si se ejecuto una apuesta se
			//asume que la jugada bonus 'mayor
			//o menor' ya fue jugada
			bonusMenorMayorDisponible = false;
			
			//limpio la jugada anterior
			jugadasLineas.clear();
			
			//obtengo las jugadas de la tirada actual
			obtenerJugadasLineas();
			
			//obtengo la jugada scatter de la tirada actual
			obtenerJugadaScatter();
			
			//obtengo la jugada bonus de la tirada actual
			obtenerJugadaBonus();
			
			//obtengo las jugadas en lineas ganadoras
			List<JugadaLineaGanadora> jugadas = validadorJugada.obtenerJugadasLineasGanadoras(jugadasLineas);

			/**
			 * Distribuimos un porcentaje dado
			 * del monto de la apuesta para el
			 * pozo del minijuego 'menor o mayor'
			 */
			distribuidorPremios.distribuirMontoPremioMenorMayor();
			
			/**
			 * Distribuimos un porcentaje dado
			 * del monto de la apuesta para el
			 * pozo JACKPOT
			 */
			distribuidorPremios.distribuirMontoPremioJackpot();

			/**
			 * Procedemos a realizar la evaluacion 
			 * de la apuesta para ver si es necesario
			 * cancelar alguna debido a que se pasaron
			 * los pagos reales de la maquina
			 */
			boolean cancelaJugadas = evaluadorJugada.evaluarCancelacionJugadasLineasGanadoras(jugadas);
			
			/**
			 * Si no existen jugadas ganadoras
			 * en la apuesta actual, entonces
			 * pasamos a analizar si existe 
			 * alguna jugada disponible a traves
			 * del distribuidor de premios
			 */
			if(jugadas.size() == 0) {
				
				/**
				 * Chequeamos que el distribuidor de premios 
				 * tenga algun premio disponible para dar
				 */
				boolean tienePremio = distribuidorPremios.generarPremio();
				
				if(tienePremio) {
					
					/**
					 * Si tiene premio buscamos la posicion
					 * adecuada para dar solamente el premio
					 * que nos devuelve en una sola linea
					 */
					tiradaFicticia = distribuidorPremios.tiradaFicticia;
					
					//agregamos la jugada que fue generada  
					//a la lista de jugadas ganadoras
					jugadas.add(distribuidorPremios.premioGenerado);
					
				}
				else if(cancelaJugadas) {
					
					/**
					 * Si no tiene premio debido a que la
					 * jugada fue cancelada buscamos una 
					 * combinacion que no contega premio
					 * alguno y configuramos esa posicion
					 */
					tiradaFicticia = distribuidorPremios.tiradaFicticia;
					
				}
			}
			
			//lo q dice
			//mostrarTiradaActualConsola();
			
			//calculo y pago el dinero que se ha  
			//ganado en la ultima tirada ejecutada
			pagoJugadasLineasGanadoras(apuestaGratuita, jugadas);

			/**
			 * Ejecuta las rutinas que ajustan las 
			 * variables usadas en el sistema para
			 * verificar los pagos hechos hasta el
			 * momento por la maquina, y tomar acciones
			 * en base a eso
			 */		
			sistemaControlPago.ejecutarControlPago();

			//asignamos las tiradas gratuitas
			//generados por jugadas bonus
			asignarTiradasGratuitas(jugadas, apuestaGratuita);

			//asignamos el juego bonus menor
			//mayor en caso de ser necesario
			asignarBonusMenorMayor(jugadas);
			
			//reconfiguro los valores posibles
			//de apuestas despues de haber
			//ejecutado esta apuesta totalmente
			reconfigurarPosiblesApuestas();
			
			//registramos las estadisticas
			//de la apuesta
			registrarEstadisticasApuesta(jugadas, apuestaGratuita);
			
			//copiamos la lista
			//de jugadas ganadoras
			jugadasLineasGanadoras = new ArrayList<JugadaLineaGanadora>(jugadas);
			
			//si el jugador ya no cuenta
			//con mas creditos, finalizamos
			//el juego
			if(maquina.montoJugador <= 0) {
				finalizaSesionJuego();
			}
				
			//devolvemos el monto 
			//actual del jugador
			return maquina.montoJugador;
		}
	}
	
	/**
	 * Contabiliza la mano del  
	 * juego menor o mayor
	 */
	public int manoMenorMayor;
	public float montoActualMenorMayor;
	public float montoGanadoMenorMayor;
	public int porcentajeEntregaMenorMayor;
	public int cartaMaquinaMenorMayor;
	public int cartaJugadorMenorMayor;
	
	/**
	 * Obtiene la carta correspondiente
	 * a la maquina en el minijuego 
	 * 'menor o mayor'
	 * @return
	 */
	public int generarCartaMaquinaMinijuegoMenorMayor() {
		
		//id de carta final que debe
		//de ser asignado
		int cartaId = 0;
		
		/**
		 * Comienza la seccion de probabilidades
		 * de la apuesta, en base principalmente 
		 * al numero de tirada actual en la que
		 * se encuentra el jugador
		 */
		switch (manoMenorMayor) {
		case 0:
			porcentajeEntregaMenorMayor = 80;
			break;
		case 1:
			porcentajeEntregaMenorMayor = 70;
			break;
		case 2:
			porcentajeEntregaMenorMayor = 55;
			break;
		case 3:
			porcentajeEntregaMenorMayor = 40;
			break;
		case 4:
			porcentajeEntregaMenorMayor = 25;
			break;
		default:
			porcentajeEntregaMenorMayor = 0;
			break;
		}
		
		//lista con cartas que 
		//van a ser admitidas como
		//posibles cartas de la maquina
		int[] cartasAdmitidas = new int[]{2,3,4,5,6,7,8,9,10,11};
		
		//elegimos una posicion aleatorio
		//dentro de la lista de cartas 
		//admitidas
		int posicionAleatoria = (int)(SlotsRandomGenerator.getInstance().nextFloat() * ((float)cartasAdmitidas.length));
		cartaId = cartasAdmitidas[posicionAleatoria];
		
		//dejamos una referencia 
		//a la carta de la maquina
		cartaMaquinaMenorMayor = cartaId;
		
		return cartaId;
	}
	
	/**
	 * Da por finalizada la partida 
	 * del minijuego menor mayor
	 */
	public float finalizarMinijuegoMenorMayor() {
		
		//incrementamos al monto del
		//jugador el monto ganado en
		//el minijuego
		maquina.montoJugador = (float)MathUtil.roundTwoDecimals(maquina.montoJugador + montoGanadoMenorMayor);
		
		//reconfiguramos el monto en el
		//distribuidor de premios del pozo
		//por el minijuego menor mayor
		distribuidorPremios.montoDistribuidoMenorMayor = (float)MathUtil.roundTwoDecimals(distribuidorPremios.montoDistribuidoMenorMayor - montoGanadoMenorMayor);
		
		//El monto del JACKPOT comienza
		//nuevamente desde cero
		actualizarValorConfiguracion(ConfiguracionDAO.MENOR_MAYOR_MONTO_ACTUAL, String.valueOf(distribuidorPremios.montoDistribuidoMenorMayor));

		//el dinero apostado es el
		//valor con el que se entra
		//al minijuego
		float dineroApostado = (float)MathUtil.roundTwoDecimals((((float)maquina.lineasApostadas) * maquina.dineroApostadoPorLinea));
		
		//actualizamos el resumen de cobranza
		actualizarUltimoResumenCobranzaEntradaSalida(dineroApostado, montoGanadoMenorMayor);
		
		//seteamos valor a otra
		//variable antes de 
		//resetear la misma
		float montoGanadorAntesReset = (float)MathUtil.roundTwoDecimals(montoGanadoMenorMayor);
		
		//reconfigura variables
		reconfigurarVariablesMenorMayor();
		
		return montoGanadorAntesReset;
	}
	
	public void reconfigurarVariablesMenorMayor() {
		
		montoActualMenorMayor = 0f;
		montoGanadoMenorMayor = 0f;
		manoMenorMayor = 0;
		porcentajeEntregaMenorMayor = 0;
		cartaMaquinaMenorMayor = 0;
		cartaJugadorMenorMayor = 0;

	}
	
	public void inicializarMinijuegoMenorMayor() {
		
		//si es la primer mano
		//el valor inicial ganado
		//es el monto de entrada
		montoActualMenorMayor = montoIngresoBonusMenorMayor;

	}
	
	/**
	 * Ejecuta la apuesta del minijuego
	 * menor o mayor (doble o nada)
	 */
	public boolean apostarMinijuegoMenorMayor(boolean mayor) {
		
		//flag que indica si esta
		//ejecucion de un resultado
		//favorable para el jugador
		boolean ganaMinijuego = false;
		
		//antes que nada debemos verificar
		//si la apuesta actual es pagable
		if(distribuidorPremios.montoDistribuidoMenorMayor > (MathUtil.roundTwoDecimals(montoActualMenorMayor * 2f))) {
			//configura el flag ejecutando
			//una apuesta con el porcentaje
			//de entrega actual
			ganaMinijuego = SlotsRandomGenerator.ejecutarPorcentajeEntrega(porcentajeEntregaMenorMayor);
		}
		else {
			//para asegurarnos de 
			//que no pase nada raro
			porcentajeEntregaMenorMayor = 0;
		}
		
		//calculamos cual puede ser
		//la carta del jugador en base
		//a los resultados de la apuesta
		if(ganaMinijuego) {
			
			if(manoMenorMayor == 0) {
				//asignamos el monto  anterior
				//del minijuego a lo ya ganado
				montoGanadoMenorMayor = montoActualMenorMayor;
			}
			else {
				montoGanadoMenorMayor = (float)MathUtil.roundTwoDecimals(montoGanadoMenorMayor * 2f);
			}
			
			//primer mano, entra con el
			//monto apostado en los rodillos
			if(manoMenorMayor > 0) {

				//si es superior a 
				//la primer mano duplica
				montoActualMenorMayor = (float)MathUtil.roundTwoDecimals((montoActualMenorMayor * 2f));
			}
			
			//obtiene la carta adecuada
			//para que el jugador gane
			cartaJugadorMenorMayor = obtenerCartaIdMinijuego(true, mayor);
			
		}
		else {
			
			//si no se gana el minijuego
			//se reconfiguran variables
			manoMenorMayor = 0;
			montoActualMenorMayor = 0f;
			montoGanadoMenorMayor = 0f;
			porcentajeEntregaMenorMayor = 0;

			//obtiene la carta adecuada
			//para que el jugador pierda
			cartaJugadorMenorMayor = obtenerCartaIdMinijuego(false, mayor);
			
		}
		
		System.out.println("---> MONTO ASEGURADO MENOR MAYOR: " + montoGanadoMenorMayor);
		
		//incrementamos el numero
		//de mano del jugador
		manoMenorMayor++;
		
		return ganaMinijuego;
	}
	
	/**
	 * En base al parametro de si el minijuego
	 * se debe dar como ganado, y a la seleccion
	 * de carta 'menor o mayor' dada por el usuario,
	 * se obtiene la carta que satisfaga esas
	 * caracteristicas
	 * 
	 * @param gana
	 * @param mayor
	 * @return
	 */
	private int obtenerCartaIdMinijuego(boolean gana, boolean mayor) {
		
		int cartaId = 0;
		
		//lista contenedora 
		//de numeros validos
		List<Integer> numerosValidos = new ArrayList<Integer>();
		
		//obtenemos las cartas
		List<Carta> cartas = maquina.configuracion.getCartas();
		
		//recorremos la lista de cartas
		for(Carta carta : cartas) {
			
			if(gana) {
				if(mayor) {
					if(carta.getId() > cartaMaquinaMenorMayor) {
						numerosValidos.add(carta.getId());
					}
				}
				else {
					if(carta.getId() < cartaMaquinaMenorMayor) {
						numerosValidos.add(carta.getId());
					}
				}
			}
			else {
				if(mayor) {
					if(carta.getId() < cartaMaquinaMenorMayor) {
						numerosValidos.add(carta.getId());
					}
				}
				else {
					if(carta.getId() > cartaMaquinaMenorMayor) {
						numerosValidos.add(carta.getId());
					}
				}
			}
		}

		if(numerosValidos != null && numerosValidos.size() > 0) {
			//obtenemos una figura aleatoria
			//de la lista de numeros validos
			cartaId = numerosValidos.get(((int)(SlotsRandomGenerator.getInstance().nextFloat() * (float)numerosValidos.size())));
		}
		
		return cartaId;
	}
	
	
	/**
	 * Graba estadisticas relacionadas
	 * a la ejecucion de la ultima 
	 * apuesta
	 */
	private void registrarEstadisticasApuesta(List<JugadaLineaGanadora> jugadas, boolean apuestaGratuita) {
		
		EstadisticaMaquina estadisticaMaquina = maquina.estadisticaMaquina;
		
		/**
		 * Solo si la apuesta no es 
		 * gratuita registramos como
		 * que fue dinero apostado 
		 * por el jugador
		 */
		if(!apuestaGratuita) {

			//incremento la cantidad de  jugadas 
			//totales hechas (una por linea)
			estadisticaMaquina.cantidadApuestas += maquina.lineasApostadas;
			
			//incremento la cantidad de dinero
			//apostado entre todas las lineas
			estadisticaMaquina.dineroApostado += (((float)maquina.lineasApostadas) * maquina.dineroApostadoPorLinea);
		}
		
		//agrego a las estadisticas las jugadas ganadoras
		for(JugadaLineaGanadora jlg : jugadas) {

			int tiradasGratuitas = 0;
			
			//asigno los premio de bonus
			if(jlg.getTipo() == JugadaTipo.BONUS) {

				JugadaGanadoraBonus jugadaGanadoraBonus = ((JugadaGanadoraBonus)jlg.getJugadaGanadora());
				tiradasGratuitas = jugadaGanadoraBonus.getTiradasGratis();
				estadisticaMaquina.cantidadApuestasBonus += tiradasGratuitas;
				
			}
			else if(jlg.getTipo() == JugadaTipo.JACKPOT) {
				
				/**
				 * 		  !!! IMPORTANTE !!!
				 * 		
				 * No registramos el monto ganado con el
				 * JACKPOT ya que ese monto ya se asigna
				 * como ganado cada vez que se realiza una
				 * apuesta maxima y se retira dinero para
				 * este pozo.
				 *  
				 */
				
				//incremento en uno la cantidad de 
				//jugadas totales ganadas
				estadisticaMaquina.cantidadApuestasGanadas++;
				
			}
			else {
				
				/**
				 * Solo si la jugada no fue generada por el 
				 * distribuidor de premios se suma el dinero
				 * ganado al modulo estadistico. Esto se hace
				 * debido a que esa suma se va realizando 
				 * gradualmente por el modulo EvaluadorJugada
				 * cuando las jugadas son canceladas y el monto
				 * restante es distribuido. Con esto logramos  
				 * que los los valores estadisticos no se 
				 * disparen si sale un premio grande generado
				 * por el distribuidor de premios. 
				 */
				if(!jlg.isGenerado()) {
					
					//calculamos el monto ganado
					float dineroGanado = (((float)jlg.getJugadaGanadora().multiplicador) * maquina.dineroApostadoPorLinea);
					
					//incremento la cantidad de dinero ganado en esta apuesta
					estadisticaMaquina.dineroGanado += dineroGanado;
				}
				
				//incremento en uno la cantidad de 
				//jugadas totales ganadas
				estadisticaMaquina.cantidadApuestasGanadas++;

			}
			
			//registro el tipo de jugada ganada
			estadisticaMaquina.registrarGanadorTipoJugada(jlg.getJugadaGanadora().obtenerFormateado(), jlg.getTipo(), jlg.getJugadaGanadora().multiplicador, tiradasGratuitas);
				
		}

		//ordeno la lista de tipo de jugadas ganadoras
		Collections.sort(estadisticaMaquina.ganadoresTipoJugada, new GanadorTipoJugadaComparator());
		
		//actualizamos los valores
		//estadisticos generales
		estadisticaMaquina.actualizarValoresEstadisticos();
		
	}
	
	/**
	 * Si se ha generado un bonus menor mayor
	 * debemos configurar las variables 
	 * necesarias para dejar pronta la logica
	 * para jugar
	 */
	private void asignarBonusMenorMayor(List<JugadaLineaGanadora> jugadas) {
		
		//recorro la lista de jugadas ganadoras 
		for(JugadaLineaGanadora jugadaLineaGanadora : jugadas) {

			//si la linea ganadora es de tipo bonus
			if(jugadaLineaGanadora.getTipo() == JugadaTipo.BONUS_MENOR_MAYOR) {
				
				//calculamos el monto con el
				//que entra el jugador al
				//bonus menor mayor
				montoIngresoBonusMenorMayor = (float)MathUtil.roundTwoDecimals((((float)maquina.lineasApostadas) * maquina.dineroApostadoPorLinea));
				
				//configuramos el flag que
				//indica que el jugador tiene
				//un minijuego disponible
				bonusMenorMayorDisponible = true;
				
				//salimos
				break;
			}
		}
	}
	
	/**
	 * Asigna las tiradas gratuitas generadas
	 * por las jugadas ganadoras de tipo bonus
	 */
	private void asignarTiradasGratuitas(List<JugadaLineaGanadora> jugadas, boolean tiradaGratuita) {
		
		//creamos un iterador de las 
		//jugadas ganadoras
		Iterator<JugadaLineaGanadora> jugadasIt = jugadas.iterator();
		
		//recorro la lista de jugadas ganadoras 
		while(jugadasIt.hasNext()) {
			
			//obtenemos la proxima 
			//jugada ganadora
			JugadaLineaGanadora jugadaLineaGanadora = jugadasIt.next();
			
			//si la linea ganadora es de tipo bonus
			if(jugadaLineaGanadora.getTipo() == JugadaTipo.BONUS) {

				//si la tirada es gratuita ya
				//no consideramos la jugada bonus
				//actual y la borramos de la lista
				//de jugadas ganadoras
				if(tiradaGratuita) {
					
					//removemos el elemento 
					//de la lista
					jugadasIt.remove();
					
				}
				else {
					
					//casteo a bonus
					JugadaGanadoraBonus jugadaGanadoraBonus = (JugadaGanadoraBonus)jugadaLineaGanadora.getJugadaGanadora();
					
					//incremento la cantidad de tiradas 
					//gratuitas generadas por el bonus
					cantidadTiradasGratis += jugadaGanadoraBonus.getTiradasGratis();

				}
			}
		}
	}
	
	/**
	 * Calcula y paga al jugador las jugadas
	 * ganadoras de la ultima tirada ejecutada
	 */
	private void pagoJugadasLineasGanadoras(boolean actualizarMontoJugadaGratuita, List<JugadaLineaGanadora> jugadas) {
		
		float totalAPagar = 0f;
		
		//recorro la lista de jugadas ganadoras 
		for(JugadaLineaGanadora jugadaLineaGanadora : jugadas) {

			//si la linea ganadora no es bonus
			if(jugadaLineaGanadora.getTipo() != JugadaTipo.BONUS && jugadaLineaGanadora.getTipo() != JugadaTipo.BONUS_MENOR_MAYOR) {
				
				//variable auxiliar
				float montoGanadoJugada = 0;
				
				/**
				 * Si la jugada es NORMAL o SCATTER
				 */
				if(jugadaLineaGanadora.getTipo() == JugadaTipo.NORMAL || jugadaLineaGanadora.getTipo() == JugadaTipo.SCATTER || jugadaLineaGanadora.getTipo() == JugadaTipo.WILD) {
					
					//obtengo el multiplicador de premio
					int multiplicador = jugadaLineaGanadora.getJugadaGanadora().multiplicador;
					
					//calculamos el monto ganado
					//por la jugada actual
					montoGanadoJugada = multiplicador * maquina.dineroApostadoPorLinea;
					
					//incremento al total a pagar el multiplicador
					//de esta jugada por el monto apostado por linea
					totalAPagar += montoGanadoJugada;
				}
				else if(jugadaLineaGanadora.getTipo() == JugadaTipo.JACKPOT) { //SI LA JUGADA ES JACKPOT
					
					//incrementamos al monto de pago 
					//total el monto del JACKPOT
					montoGanadoJugada = distribuidorPremios.montoDistribuidoJackpot;
					totalAPagar += distribuidorPremios.montoDistribuidoJackpot;
					
					//reseteamos a cero el monto
					//del JACKPOT en el distribuidor
					distribuidorPremios.montoDistribuidoJackpot = 0;
					
					//El monto del JACKPOT comienza
					//nuevamente desde cero
					actualizarValorConfiguracion(ConfiguracionDAO.JACKPOT_MONTO_ACTUAL, String.valueOf(0));
					
				}
				
				//si la apuesta actual fue gratuita 
				//actualizamos el monto de gratuitas
				if(actualizarMontoJugadaGratuita) {
					
					montoEntragadoTiradasGratuitas += montoGanadoJugada;
					
					//incrementamos la variable que guarda la 
					//cantidad de dinero ganada por jugadas 
					//generadas durante tiradas gratuitas
					maquina.estadisticaMaquina.dineroGanadoBonus += montoGanadoJugada;
				}
				else {
					
					//incremento la cantidad de dinero ganada en este bonus 
					//pero discriminado ese dinero solo por premios scatter
					if(jugadaLineaGanadora.getTipo() == JugadaTipo.SCATTER) {
						maquina.estadisticaMaquina.dineroGanadoScatter += montoGanadoJugada;	
					}
					else if(jugadaLineaGanadora.getTipo() == JugadaTipo.NORMAL || jugadaLineaGanadora.getTipo() == JugadaTipo.WILD) {
						maquina.estadisticaMaquina.dineroGanadoNormal += montoGanadoJugada;	
					}
					else if(jugadaLineaGanadora.getTipo() == JugadaTipo.JACKPOT) {
						maquina.estadisticaMaquina.dineroGanadoJackpot += montoGanadoJugada;	
					}
				}
			}
		}

		/**
		 * Actualizamos en el resumen de pago la 
		 * informacion de los pagos y cobros de
		 * esta apuesta
		 */
		//si el flag es verdadero significa que
		//la tirada fue gratuita, sino calculamos
		//cuanto fue lo apostado
		float dineroApostado = actualizarMontoJugadaGratuita ? 0 : (((float)maquina.lineasApostadas) * maquina.dineroApostadoPorLinea);
		float dineroGanado = totalAPagar;
		
		//actualizamos el resumen de cobranza
		actualizarUltimoResumenCobranzaEntradaSalida(dineroApostado, dineroGanado);
		
		//incremento el valor al monto
		maquina.incrementarMontoJugador(totalAPagar);
		
		//incrementamos la variable que indica
		//cuanto fue pagado en premios al usuario
		//durante la sesion (sin contabilizar dinero
		//salido, solo premios)
		montoPremiosGanadosSesion += totalAPagar;
		
		//actualizamos el valor en 
		//la persistencia
		actualizarValorConfiguracion(ConfiguracionDAO.MONTO_PREMIOS_SESION_JUGADOR, ""+montoPremiosGanadosSesion);

	}
	
	/**
	 * Decrementa el monto total de dinero del jugador
	 * @param valor
	 */
	private void decrementarMontoJugador(float valor) {
		
		//decremento el valor al monto
		maquina.incrementarMontoJugador(-valor);
		
		//hago un chequeo en caso de que
		//haya un bug y quede el valor
		//en negativo
		if(maquina.montoJugador < 0) {
			throw new RuntimeException("El monto del jugador nunca puede ser menor que cero!");
		}
	}
	
	private void obtenerJugadaScatter() {
		
		//modelo de figura scatter 
		//(si fue configurado por el usuario)
		Figura figuraScatter = null;
		
		//coleccion con figuras donde sera 
		//buscada la figura scatter
		List<Figura> figuras = maquina.configuracion.getFiguras();
		
		for(Figura figuraActual : figuras) {
			if(figuraActual.getTipo() == FiguraTipo.SCATTER) {
				figuraScatter = figuraActual;
				break;
			}
		}
		
		if(figuraScatter != null) {
			
			JugadaLinea jugadaLineaScatter = new JugadaLinea();
			jugadaLineaScatter.setTipo(JugadaTipo.SCATTER);
			
			//recorro la tirada para compararla 
			//con la forma de la linea
			for(int j=0; j<tiradaActualValores[0].length; j++) {
				for(int k=0; k<tiradaActualValores.length; k++) {
					
					if(tiradaActualValores[k][j] == figuraScatter.getId()) {
						
						//la agrego al objeto jugadaLinea
						jugadaLineaScatter.getFiguras().add(figuraScatter);
					}
				}
			}
			
			if(jugadaLineaScatter.getFiguras().size() > 0) {
				jugadasLineas.add(jugadaLineaScatter);	
			}
		}
	}
	
	private void obtenerJugadaBonus() {
		
		//modelo de figura bonus 
		//(si fue configurado por el usuario)
		Figura figuraBonus = null;
		
		//coleccion con figuras donde sera 
		//buscada la figura scatter
		List<Figura> figuras = maquina.configuracion.getFiguras();
		
		for(Figura figuraActual : figuras) {
			if(figuraActual.getTipo() == FiguraTipo.BONUS) {
				figuraBonus = figuraActual;
				break;
			}
		}
		
		if(figuraBonus != null) {
			
			JugadaLinea jugadaLineaBonus = new JugadaLinea();
			jugadaLineaBonus.setTipo(JugadaTipo.BONUS);
			
			for(int j=0; j<tiradaActualValores[0].length; j++) {
				for(int k=0; k<tiradaActualValores.length; k++) {

					if(tiradaActualValores[k][j] == figuraBonus.getId()) {
						
						//la agrego al objeto jugadaLinea
						jugadaLineaBonus.getFiguras().add(figuraBonus);
					}
				}
			}
			
			if(jugadaLineaBonus.getFiguras().size() > 0) {
				jugadasLineas.add(jugadaLineaBonus);	
			}
		}
	}
	
	private void obtenerJugadasLineas() {
		
		//recorro las lineas apostadas
		for(int i=0; i<maquina.lineasApostadas; i++) {
			
			//chequeo sencillo debido a la aparicion de un bug
			//relacionado a que se pasa el limite de lineas apostadas
			//cuando se hacen muchas jugadas seguidas en el sistema
			//estadistico. TODO Revisar esto...
			if(maquina.lineasApostadas <= maquina.lineas.size()) {
				
				//obtengo la forma de linea actual
				Linea linea = maquina.lineas.get(i);
				int[][] formaLinea = linea.getForma();
				
				//creo la jugada de linea para guardar datos
				JugadaLinea jugadaLinea = new JugadaLinea();
				jugadaLinea.setTipo(JugadaTipo.NORMAL);
				jugadaLinea.setLinea(linea);
				
				//agrego la jugada linea a la coleccion
				jugadasLineas.add(jugadaLinea);
				
				//recorro la tirada para compararla 
				//con la forma de la linea
				for(int j=0; j<tiradaActualValores[0].length; j++) {
					
					for(int k=0; k<tiradaActualValores.length; k++) {
						
						if(formaLinea[k][j] == 1) {

							//obtengo la figura correspondinete
							Figura figura = maquina.configuracion.obtenerFiguraPorId(tiradaActualValores[k][j]);
							
							//la agrego al objeto jugadaLinea
							jugadaLinea.getFiguras().add(figura);
						}
					}
				}
				
				//cantida de figuras
				//wild en la linea
				int cantidadWild = 0;
				Figura figuraWild = maquina.configuracion.obtenerFiguraWild();
				
				/**
				 * Chequeamos si la jugada 
				 * es una jugada de tipo wild
				 */
				for(int k=0; k<jugadaLinea.getFiguras().size(); k++) {
					
					if(jugadaLinea.getFiguras().get(k).getId() == figuraWild.getId()) {
						cantidadWild++;
					}
				}
				
				//si la cantidad de figuras wild
				//es igual a la cantidad de rodillos
				//entonces es una jugada de tipo wild
				if(cantidadWild == maquina.rodillos.size()) {
					jugadaLinea.setTipo(JugadaTipo.WILD);
				}
			}
		}
	}
	
	/**
	 * Actualiza en el ultimo resumen de cobranza 
	 * los valores de entrada y salida
	 * @param entrada
	 * @param salida
	 */
	public void actualizarUltimoResumenCobranzaEntradaSalida(float entrada, float salida) {
		
        //obtenemos los datos del
        //ultimo resumen de cobranza
        ResumenCobranza resumenCobranza = obtenerUltimoResumen();

        //recalculamos los nuevos valores
        float nuevaEntrada = resumenCobranza.entrada + entrada;
        float nuevaSalida = resumenCobranza.salida + salida;
        float nuevoSaldo = nuevaEntrada-nuevaSalida;

        //maquina real
        if(!maquina.test) {
            //actualizamos los datos del
            //resumen en la persistencia
            resumenCobranzaDAO.actualizarResumen(resumenCobranza.id, resumenCobranza.base, nuevaEntrada, nuevaSalida, nuevoSaldo, resumenCobranza.entradaMonedasPA, resumenCobranza.entradaMonedasPC, resumenCobranza.salidaMonedasPA, resumenCobranza.salidaMonedasPC);
        }
        else {
            //maquina test
            ultimoResumenCobranza.entrada = ultimoResumenCobranza.entrada + entrada;
            ultimoResumenCobranza.entrada = ultimoResumenCobranza.salida + salida;
            ultimoResumenCobranza.saldo = ultimoResumenCobranza.entrada - ultimoResumenCobranza.salida;
        }
	}
	
	/**
	 * Actualiza en el ultimo resumen de
	 * cobranza el monto base del mismo
	 * @param base
	 */
	public void actualizarUltimoResumenCobranzaBase(float base) {
		//solo accedemos a la persistencia
		//si es la maquina real y no una
		//de testeo
		if(!maquina.test) {
            //obtenemos los datos del
            //ultimo resumen de cobranza
            ResumenCobranza resumenCobranza = obtenerUltimoResumen();

            //actualizamos los datos del
            //resumen en la persistencia
            resumenCobranzaDAO.actualizarResumen(resumenCobranza.id, base, resumenCobranza.entrada, resumenCobranza.salida, resumenCobranza.saldo, resumenCobranza.entradaMonedasPA, resumenCobranza.entradaMonedasPC, resumenCobranza.salidaMonedasPA, resumenCobranza.salidaMonedasPC);
		}
	}
	
	/**
	 * Actualiza un valor estadistico 
	 * en la persistencia
	 */
	public void actualizarValorEstadistica(String nombre, float nuevoValor) {
		if(!maquina.test) {
            //persistimos
            estadisticaDAO.actualizarValor(nombre, nuevoValor);
		}
	}
	
	public void actualizarValoresEstadisticos(float dineroApostado, float dineroGanado, float dineroGanadoNormal, float dineroGanadoScatter, float dineroGanadoBonus, float dineroGanadoJackpot, long cantidadApuestas, long cantidadApuestasGanadas, long cantidadApuestasBonus) {
		if(!maquina.test) {
            //persistimos
            estadisticaDAO.actualizarValoresEstadisticos(dineroApostado, dineroGanado, dineroGanadoNormal, dineroGanadoScatter, dineroGanadoBonus, dineroGanadoJackpot, cantidadApuestas, cantidadApuestasGanadas, cantidadApuestasBonus);
		}
	}
	
	/**
	 * Devuelve todos los valores estadisticos
	 * registrados en la persistencia
	 * @return
	 */
	public Map<String, Float> obtenerValoresEstadistica() {
		Map<String, Float> valores = new HashMap<String, Float>();
		if(!maquina.test) {
			//obtenemos los valores desde
			//persistencia
    		valores = estadisticaDAO.obtenerValores();
		}
		return valores;
	}
	
	/**
	 * Resetea los valores estadisticos de 
	 * la maquina
	 */
	public void restablecerValoresEstadisticos() {

		maquina.estadisticaMaquina.dineroApostado = 0f; 
		maquina.estadisticaMaquina.dineroGanado = 0f; 
		maquina.estadisticaMaquina.dineroGanadoNormal = 0f; 
		maquina.estadisticaMaquina.dineroGanadoScatter = 0f; 
		maquina.estadisticaMaquina.dineroGanadoBonus = 0f; 
		maquina.estadisticaMaquina.dineroGanadoJackpot = 0f; 
		maquina.estadisticaMaquina.cantidadApuestas = 0; 
		maquina.estadisticaMaquina.cantidadApuestasGanadas = 0; 
		maquina.estadisticaMaquina.cantidadApuestasBonus = 0;
		
		//tambien reseteamos los valores 
		//del distribuidor de premios
		borrarTodasDistribuciones();
		
		//forzamos la escritura de los nuevos valores
		maquina.estadisticaMaquina.actualizarValoresEstadisticos();
		
	}
	
	/**
	 * Obtiene la lista con los ultimos resumenes
	 * recuperables desde la persistencia
	 * @return
	 */
	public List<ResumenCobranza> obtenerUltimosResumenes() {
		List<ResumenCobranza> lista = new ArrayList<ResumenCobranza>();
		if(!maquina.test) {
            //obtenemos la lista con los
            //resumenes de cobranza
            lista = resumenCobranzaDAO.obtenerUltimosResumenes(Sistema.RESUMENES_COBRANZA_LIMITE_OBTENCION);
        }
		return lista;
	}
	
	/**
	 * Obtiene la lista con los ultimos pagos
	 * excedidos recuperables desde la persistencia
	 * @return
	 */
	public List<PagoExcedido> obtenerUltimosPagosExcedidos() {
		List<PagoExcedido> lista = new ArrayList<PagoExcedido>();
		if(!maquina.test) {
            //obtenemos la lista con los
            //resumenes de cobranza
            lista = pagoExcedidoDAO.obtenerUltimosPagosExcedidos();
		}
		
		return lista;
	}
	
	/**
	 * Obtiene la lista con los ultimos resumenes
	 * recuperables desde la persistencia
	 * @return
	 */
	public ResumenCobranza obtenerUltimoResumen() {
        if(resumenCreadoModificado) {
            if(!maquina.test) {
                if(resumenCobranzaDAO != null) {
                    //obtenemos el ultimo
                    //resumen de cobranza
                    ultimoResumenCobranza = resumenCobranzaDAO.obtenerUltimoResumen();
                    //cambiamos el flag para
                    //no entrar nuevamente hasta
                    //que no se cree un nuevo
                    //resumen
                    resumenCreadoModificado = false;
                }
            }
        }
		return ultimoResumenCobranza;
	}
	
	/**
	 * Finaliza el resumen de cobranza actual, y
	 * crea uno nuevo pronto para ser usado
	 */
	public void finalizarResumenActual() {
		if(!maquina.test) {
            //obtenemos el ultimo resumen
            ResumenCobranza resumenCobranzaActual = obtenerUltimoResumen();
            //fecha actual
            Timestamp fechaActual = new Timestamp(System.currentTimeMillis());
            //actualizamos la fecha hasta
            //para darlo por finalizado
            resumenCobranzaDAO.actualizarResumenFechaHasta(resumenCobranzaActual.id, fechaActual);
            //creamos un nuevo resumen
            crearResumenCobranza();
            //actualizamos los valores
            //de configuracion
            actualizarValorConfiguracion(ConfiguracionDAO.MONTO_APOSTADO_SESION_JUGADOR, "0");
            actualizarValorConfiguracion(ConfiguracionDAO.MONTO_PREMIOS_SESION_JUGADOR, "0");
            //reseteamos los valores estadisticos
            restablecerValoresEstadisticos();
		}
	}
	
	/**
	 * Crea un nuevo resumen de cobranza
	 */
	public void crearResumenCobranza() {
        if(!maquina.test) {
            //maquina real
            resumenCobranzaDAO.crearResumen(0f, 0f, 0f, 0f, 0, 0, 0, 0, new Timestamp(System.currentTimeMillis()), null);
        }
        else {
            //maquina test
            ultimoResumenCobranza = new ResumenCobranza(0, 50f, 0f, 0f, 0f, 0, 0, 0, 0, null, null);
        }
        //cambiamos flag para que
        //se cargue nuevamente
        resumenCreadoModificado = true;
	}
	
	/**
	 * Actualiza el valor de un registro de la
	 * tabla 'configuracion'
	 * @param codigo
	 * @param nuevoValor
	 */
	public void actualizarValorConfiguracion(String codigo, String nuevoValor) {
		//solo accedemos a la persistencia
		//si es la maquina real y no una
		//de testeo
		if(!maquina.test) {
            //ejecutamos el cambio en la persistencia
            configuracionDAO.actualizarValor(codigo, nuevoValor);
            //volvemos a cargar los valores de configuracion
            //desde la persistencia para ver los cambios en
            //memoria inmediatamente
            cargarValoresConfiguracion();
            //si el valor de configuracion a ser
            //cambiado es el porcentaje del jugador,
            //reseteamos los valores estadisticos de
            //la maquina
            if(codigo.equals(ConfiguracionDAO.PORCENTAJE_JUGADOR)) {
                restablecerValoresEstadisticos();
            }
		}
	}
	
	/**
	 * Obtiene la descripcion de un
	 * valor de configuracion desde
	 * la base de datos
	 * 
	 * @param codigo
	 * @return
	 */
	public String obtenerDescripcionConfiguracion(String codigo) {
		return configuracionDAO.obtenerDescripcion(codigo);
	}
	
	/**
	 * Obtiene el valor del registro asociado
	 * al codigo recibido por parametro de la
	 * tabla 'configuracion'
	 * @param codigo
	 * @return
	 */
	public String obtenerValorConfiguracion(String codigo) {
		return configuracionDAO.obtenerValor(codigo);
	}
	
	public Map<String, String> obtenerValoresConfiguracion() {
	    return configuracionDAO.obtenerValores();
	}
	
	/**
	 * Carga desde la persistencia los
	 * valores de configuracion de la
	 * maquina
	 */
	public void cargarValoresConfiguracion() {
		
		//obtenemos todos los valores de 
		//configuracion desde la persistencia
		Map<String, String> valoresConfiguracion = obtenerValoresConfiguracion();
		
		//obtenemos el valor de cada uno de
		//los campos que precisemos configurar
		String apuestaAdmitidaStr = valoresConfiguracion.get(ConfiguracionDAO.APUESTA_ADMITIDA);
		String apuestaMaximaPorLineaStr = valoresConfiguracion.get(ConfiguracionDAO.APUESTA_MAXIMA_POR_LINEA);
		String audioStr = valoresConfiguracion.get(ConfiguracionDAO.AUDIO);
		String cantidadLineasBonusStr = valoresConfiguracion.get(ConfiguracionDAO.CANTIDAD_LINEAS_APUESTA_GRATUITA);
		String factorConversionMonedaStr = valoresConfiguracion.get(ConfiguracionDAO.FACTOR_CONVERSION_MONEDA);
		String incrementoApuestaStr = valoresConfiguracion.get(ConfiguracionDAO.INCREMENTO_APUESTA);
		String montoApuestaGratuitaStr = valoresConfiguracion.get(ConfiguracionDAO.MONTO_APUESTA_GRATUITA);
		String apuestaWildStr = valoresConfiguracion.get(ConfiguracionDAO.APUESTA_WILD);
		String porcentajeJugadorStr = valoresConfiguracion.get(ConfiguracionDAO.PORCENTAJE_JUGADOR);
		
		String jackpotMontoMinimoStr = valoresConfiguracion.get(ConfiguracionDAO.JACKPOT_MONTO_MINIMO);
		String jackpotPorcentajeExtraidoApuestaStr = valoresConfiguracion.get(ConfiguracionDAO.JACKPOT_PORCENTAJE_EXTRAIDO_APUESTA);
		String jackpotProbabilidadesJugadorStr = valoresConfiguracion.get(ConfiguracionDAO.JACKPOT_PROBABILIDADES_JUGADOR);
		String jackpotAperturaPozoStr = valoresConfiguracion.get(ConfiguracionDAO.JACKPOT_APERTURA_POZO);
		String jackpotMontoActualStr = valoresConfiguracion.get(ConfiguracionDAO.JACKPOT_MONTO_ACTUAL);
		
		String menorMayorMontoActualStr = valoresConfiguracion.get(ConfiguracionDAO.MENOR_MAYOR_MONTO_ACTUAL);
		String menorMayorPorcentajeExtraidoStr = valoresConfiguracion.get(ConfiguracionDAO.MENOR_MAYOR_PORCENTAJE_EXTRAIDO_APUESTA);

		//sesion jugador
		String montoPremiosSesionJugadorStr = valoresConfiguracion.get(ConfiguracionDAO.MONTO_PREMIOS_SESION_JUGADOR);
		String montoApostadoSesionJugadorStr = valoresConfiguracion.get(ConfiguracionDAO.MONTO_APOSTADO_SESION_JUGADOR);

		//parseamos para dejarlo en memoria
		montoPremiosGanadosSesion = Float.parseFloat(montoPremiosSesionJugadorStr);
		montoApostadoSesionJugador = Float.parseFloat(montoApostadoSesionJugadorStr);
		
		//finalmente los seteamos en memoria
		Sistema.APUESTA_ADMITIDA = Integer.parseInt(apuestaAdmitidaStr);
		Sistema.APUESTA_MAXIMA_POR_LINEA = Float.parseFloat(apuestaMaximaPorLineaStr);
		SistemaFrontend.AUDIO_FX_ENABLED = Boolean.parseBoolean(audioStr);
		Sistema.CANTIDAD_LINEAS_APUESTA_GRATUITA = Short.parseShort(cantidadLineasBonusStr);
		Sistema.FACTOR_CONVERSION_MONEDA = Integer.parseInt(factorConversionMonedaStr);
		Sistema.INCREMENTO_APUESTA = Float.parseFloat(incrementoApuestaStr);
		Sistema.MONTO_APUESTA_GRATUITA = Float.parseFloat(montoApuestaGratuitaStr);
		Sistema.APUESTA_WILD = Byte.parseByte(apuestaWildStr);
		Sistema.PORCENTAJE_JUGADOR = (int)Float.parseFloat(porcentajeJugadorStr);
		Sistema.JACKPOT_MONTO_MINIMO = Integer.parseInt(jackpotMontoMinimoStr);
		Sistema.JACKPOT_PORCENTAJE_EXTRAIDO_APUESTA = Float.parseFloat(jackpotPorcentajeExtraidoApuestaStr);
		Sistema.JACKPOT_PROBABILIDADES_JUGADOR = Float.parseFloat(jackpotProbabilidadesJugadorStr);
		Sistema.JACKPOT_APERTURA_POZO = Float.parseFloat(jackpotAperturaPozoStr);
		Sistema.MENOR_MAYOR_PORCENTAJE_EXTRAIDO_APUESTA = Float.parseFloat(menorMayorPorcentajeExtraidoStr);
		distribuidorPremios.montoDistribuidoJackpot = Float.parseFloat(jackpotMontoActualStr);
		distribuidorPremios.montoDistribuidoMenorMayor = Float.parseFloat(menorMayorMontoActualStr);
		
	}
	
	public void mostrarTiradaActualConsola() {
		int[][] tiradaGenerica = null;
		if(tiradaFicticia != null) {
			
			//adaptamos la forma del array
			//ficticio para mostrarlo
			tiradaGenerica = new int[tiradaFicticia[0].length][tiradaFicticia.length];
			
			for(int i=0; i<tiradaGenerica.length; i++) {
				for(int j=0; j<tiradaGenerica[0].length; j++) {
					tiradaGenerica[i][j] = tiradaFicticia[j][i];
				}
			}
		}
		else {
			tiradaGenerica = tiradaActualValores;
		}
		
		for(int i=0; i<tiradaGenerica.length; i++) {
			
			for(int j=0; j<tiradaGenerica[0].length; j++) {
				
				System.out.print(tiradaGenerica[i][j]+"\t");
			}
			
			System.out.println();
		}
		
		System.out.println();
	}
	
	public void mostrarTiradaIndicesActualConsola() {
		
		for(int i=0; i<tiradaActualIndices.length; i++) {
			
			System.out.print(tiradaActualIndices[i]+"\t");
		}
		System.out.println();
	}

	public void mostrarJugadasLineasConsola() {
		
		for(JugadaLinea jugadaLinea : jugadasLineas) {
			
			for(Figura figura : jugadaLinea.getFiguras()) {
				
				System.out.print("->" + figura.getId() + " \t");
			}
			
			System.out.println();
		}
	}
	
	/**
	 * Incrementa la cantidad de lineas 
	 * para apostar
	 */
	public void incrementarLineas() {
		
		//obtengo la cantidad actual de 
		//lineas apostadas e incremento uno
		int nuevasLineasApostadas = maquina.lineasApostadas + 1;
		
		//calculo el monto necesario para realizar esta apuesta
		float montoNecesario = (float)MathUtil.roundTwoDecimals((nuevasLineasApostadas) * maquina.dineroApostadoPorLinea);
		
		//si el monto necesario para realiazr la apuesta
		//es mayor a la que posee el jugador comenzamos
		//desde 1 linea apostada
		if(montoNecesario > maquina.montoJugador || nuevasLineasApostadas > maquina.lineas.size()) {
			maquina.lineasApostadas = (short)1;
		}
		else {
			maquina.lineasApostadas = (short)nuevasLineasApostadas;
		}
	}
	
	/**
	 * Incrementa la cantidad de dinero 
	 * apostado por linea. El incremento
	 * esta definido por la constante 
	 * Sistema.INCREMENTO_APUESTA
	 */
	public void incrementarApuesta() {

		//calculo el nuevo monto
		float nuevaApuesta = (float)MathUtil.roundTwoDecimals(maquina.dineroApostadoPorLinea + Sistema.INCREMENTO_APUESTA);
		
		//calculo el total de dinero necesario 
		//para poder cubrir la apuesta
		float nuevoMonto = (float)MathUtil.roundTwoDecimals(nuevaApuesta * maquina.lineasApostadas);
		
		//si el nuevo monto es mayor a lo que
		//puede apostar el jugador reinicio
		//al minimo el incremento de la apuesta
		if(nuevoMonto > maquina.montoJugador || nuevaApuesta > Sistema.APUESTA_MAXIMA_POR_LINEA) {
			maquina.setDineroApostadoPorLinea(Sistema.INCREMENTO_APUESTA);
		}
		else {
			maquina.setDineroApostadoPorLinea(nuevaApuesta);
		}
	}
	
	/**
	 * Reconfiguro los limites de apuestas posibles
	 * en base al monto que posee el jugador
	 */
	public void reconfigurarPosiblesApuestas() {
		
		//si el monto del jugador es cero  
		//se termino la partida actual
		if(maquina.montoJugador == 0f) {
			
			maquina.setDineroApostadoPorLinea(0f);
			maquina.lineasApostadas = (short)0;
			
		}
		else { //evaluamos una sub apuesta del monto actual del jugador
			
			//calculo el maximo apostable
			float costoApuestaActual = maquina.dineroApostadoPorLinea * ((float)maquina.lineasApostadas);
			
			//si el monto del jugador es menor al maximo apostable
			//tenemos que reevaluar las posibilidades de apuestas
			//del jugador
			if(maquina.montoJugador < costoApuestaActual) {
			
				boolean montoEncontrado = false;
				
				//recorremos la las lineas
				//de adelante hacia atras
				for(int i=maquina.lineas.size(); i>0; i--) {
				
					//recorremos el monto saltando
					//entre vuelta por apuesta minima
					int cantidadIteraciones = (int)(Sistema.APUESTA_MAXIMA_POR_LINEA / Sistema.INCREMENTO_APUESTA);
					for(int j=cantidadIteraciones; j>0; j--) {

						/**
						 * Calculamos el monto actual en base
						 * a la cantidad de lineas actuales (i) 
						 * y al monto por linea actual 
						 * (j * Sistema.INCREMENTO_APUESTA)
						 */
						float montoActual = (((float)j) * Sistema.INCREMENTO_APUESTA) * ((float)i);
						
						//chequeamos si ya satisfacemos 
						//las necesidas del jugador
						if(montoActual <= maquina.montoJugador) {
							
							maquina.lineasApostadas = (short)i;
							float dineroApostadoPorLineaTmp = (float)MathUtil.roundTwoDecimals(((float)j) * Sistema.INCREMENTO_APUESTA);
							maquina.setDineroApostadoPorLinea(dineroApostadoPorLineaTmp);
							montoEncontrado = true;
							break;
						}
					}
					
					if(montoEncontrado) {
						break;
					}
				}
			}			
		}
	}
	
	/**
	 * En base al dinero real ingresado
	 * calcula del total recolectado cual
	 * porcentaje debe de ser devuelto al
	 * distribuidor de premios. 
	 * Esto se hace debido a que si el usuario
	 * de la sesion actual se gasta todo el 
	 * dinero, se debe de volcar el porcentaje
	 * de pago al jugador en base al dinero 
	 * real ingresado para prolongar futuras
	 * sesiones de juego con pagos mas grandes
	 */
	public void finalizaSesionJuego() {
		
		/**
		 * Reseteamos el valor para 
		 * ajustar la futura sesion
		 * a cero nuevamente
		 */
		dineroRealIngresadoSesion = 0;
		dineroRealEgresadoSesion = 0;
		montoPremiosGanadosSesion = 0;
		montoApostadoSesionJugador = 0;
		
		//actualizamos los valores 
		//de configuracion
		actualizarValorConfiguracion(ConfiguracionDAO.MONTO_APOSTADO_SESION_JUGADOR, "0");
		actualizarValorConfiguracion(ConfiguracionDAO.MONTO_PREMIOS_SESION_JUGADOR, "0");
		
		System.out.println("---> FINALIZA SESION JUEGO");
		
	}

	public void procesarEventoComando(ComandoEvent comandoEvent) {
		
		if(comandoEvent.getComandoId() == Comando.COMANDO_VERIFICAR_PAGO_MONEDA) {
			
			//obtenemos el modo de expulsion de 
			//moneda que genera este comando
			byte modoExpulsionMoneda = (Byte)comandoEvent.getGenerico();
			
			if(modoExpulsionMoneda == ModoExpulsionMoneda.MODO_EXPULSION_MONEDA_PAGO_JUGADOR) {
				
				//registramos el egreso de la moneda
				incrementarSalidaMoneda((Byte)comandoEvent.getGenericoHelper());
				
				/**
				 * Increntamos la variable de usuario que
				 * registra la cantidad de dinero real 
				 * egresado de la maquina
				 */
				dineroRealEgresadoSesion += Sistema.APUESTA_ADMITIDA; 
				
				//solo decrementamos si el monto 
				//final del jugador es mayor o igual
				//a cero
				if((maquina.montoJugador - Sistema.APUESTA_ADMITIDA) >= 0) {
					//decremento el valor al monto
					maquina.incrementarMontoJugador((float)-Sistema.APUESTA_ADMITIDA);
				}
				
				//si el jugador ya no cuenta
				//con mas creditos, finalizamos
				//el juego
				if(maquina.montoJugador <= 0) {
					finalizaSesionJuego();
				}
			}
		}
		else if(comandoEvent.getComandoId() == Comando.COMANDO_DINERO_INGRESADO) {

			//incremento el valor al monto
			maquina.incrementarMontoJugador((float)Sistema.APUESTA_ADMITIDA);
			
			//incrementamos el incremento
			//a la variable que contabiliza el
			//dinero real ingresado
			dineroRealIngresadoSesion += Sistema.APUESTA_ADMITIDA;
			
			//registramos la salida de la moneda
			byte estadoPuerta = (Byte)comandoEvent.getGenerico();
			incrementarEntradaMoneda(estadoPuerta);
			
		}
		else if(comandoEvent.getComandoId() == Comando.COMANDO_CAPACIDAD_PAGO_EXCEDIDA) {
			
			//guardamos registro del monto
			//excedido por la capacidad de
			//pago de la maquina
			guardarRegistroCapacidadPagoExcedido(maquina.montoJugador);
			
			//escribimos en la persistencia
			//el monto que no fue posible 
			//pagar
			actualizarValorConfiguracion(ConfiguracionDAO.CAPACIDAD_PAGO_EXCEDIDA, String.valueOf(maquina.montoJugador));

			//el monto actual del jugador
			//ya lo dejamos a cero, en caso
			//de que apaguen la maquina y no
			//queden incongruencias en el 
			//sistema
			actualizarValorConfiguracion(ConfiguracionDAO.MONTO_JUGADOR, String.valueOf("0"));

		}
	}
	
	/**
	 * Cambia el estado de 'pagado' en 
	 * el ultimo registro de capacidad
	 * de pago excedida que posea la 
	 * maquina
	 */
	public void cambiarEstadoUltimoPagoExcedido(boolean estado) {
        int ultimoPagoExcedidoId = obtenerUltimoPagoExcedido().id;
        pagoExcedidoDAO.cambiarEstadoPagoExcedido(ultimoPagoExcedidoId, estado);
	}
	
	public PagoExcedido obtenerUltimoPagoExcedido() {
		return pagoExcedidoDAO.obtenerUltimoResumen();
	}
	
	/**
	 * Guarda un registro conteniendo informacion
	 * acerca de la fecha y el monto que no fue
	 * posible pagar por la maquina 
	 */
	private void guardarRegistroCapacidadPagoExcedido(float monto) {
		//creamos el registro
		//en la persistencia
		pagoExcedidoDAO.crearPagoExcedido(monto, new Timestamp(System.currentTimeMillis()), false);
	}
	
	private void incrementarSalidaMoneda(byte estadoPuerta) {
        //maquina real
        if(!maquina.test) {
            int ultimoResumenId = obtenerUltimoResumen().id;
            resumenCobranzaDAO.incrementarSalidaMoneda(ultimoResumenId, estadoPuerta == 1);
        }
        else {
            //maquina test
            ultimoResumenCobranza.salidaMonedasPC += 1;
        }
        //cambiamos flag para que
        //se cargue nuevamente
        resumenCreadoModificado = true;
	}
	
	private void incrementarEntradaMoneda(byte estadoPuerta) {
        //maquina real
        if(!maquina.test) {
            int ultimoResumenId = obtenerUltimoResumen().id;
            resumenCobranzaDAO.incrementarEntradaMoneda(ultimoResumenId, estadoPuerta == 1);
        }
        else {
            ultimoResumenCobranza.entradaMonedasPC += 1;
        }
        //cambiamos flag para que
        //se cargue nuevamente
        resumenCreadoModificado = true;
	}
}