package org.microuy.javaslot.constante;

public class EstadoMaquina {

	//Estado Standby. Se refiere a cuando la
	//maquina tiene la variable 'montoJugador'
	//en valor cero, por lo tanto cuando el 
	//jugador no tiene mas dinero asociado a
	//la maquina
	public static final byte EN_ESPERA = 0;
	
	//Estado Jugando. A este estado se llega
	//cuando la variable 'montoJugador' iguala
	//un valor mayor a cero, por lo tanto cuando
	//el jugador tiene dinero disponible para 
	//seguir jugando en la maquina
	public static final byte JUGANDO = 1;
	
}
