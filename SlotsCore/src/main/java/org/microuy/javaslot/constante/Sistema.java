package org.microuy.javaslot.constante;

import org.microuy.javaslot.logica.configuracion.ConfiguracionSlotsPremiosChicos;

public class Sistema {

	/**
	 * Representa la configuracion de maquina a usarse para el juego, por ende
	 * para las apuestas reales
	 */
	public static Class configuracionMaquina = ConfiguracionSlotsPremiosChicos.class;
	
	/**
	 * Representa la configuracion usada en todo lo que represente testeos sobre
	 * la maquina. No deben de considerarse como valores generados por apuestas
	 * reales ya que no se debe de usar para eso
	 */
	public static Class configuracionTest = ConfiguracionSlotsPremiosChicos.class;
	
	/**
	 * El monto de la apuesta admitida en el juego. Esto actualmente limita el
	 * sistema a que solo pueda haber un tipo de apuesta. Lo que realmente me
	 * importa un rabano porque si manana se puede apostar otro valor es que
	 * todo anduvo bien y estamos haciendo guita!
	 */
	public static int APUESTA_ADMITIDA = 10;

	/**
	 * El incremento de la apuesta es de a esta cantidad
	 */
	public static float INCREMENTO_APUESTA = 0.2f;

	/**
	 * La cantidad maxima apostable por linea es el valor asignado aqui
	 */
	public static float APUESTA_MAXIMA_POR_LINEA = 1f;

	/**
	 * Factor multiplicador o divisor de moneda. Usado para modificar el tipo de
	 * moneda en juego y usar valores mas altos o mas bajos dependiendo de lo
	 * deseado
	 */
	public static int FACTOR_CONVERSION_MONEDA = 10;

	/**
	 * El monto apostado durante la ejecucion de una tirada gratuita generada
	 * por el bonus
	 */
	public static float MONTO_APUESTA_GRATUITA = 0.2f;

	/**
	 * Indica si el sistema hara uso o no
	 * de las jugadas ganadoras conformadas
	 * totalmente por figuras wild
	 * 
	 */
	public static byte APUESTA_WILD = 1;

	/**
	 * Las lineas apostadas durante la ejecucion de una tirada gratuita generada
	 * por el bonus
	 */
	public static short CANTIDAD_LINEAS_APUESTA_GRATUITA = 10;

	/**
	 * Define el porcentaje de pago de la maquina a el jugador. Ej.: De una
	 * apuesta de 100 pesos, 30 son devueltos al jugador, y la casa se queda con
	 * los 70 restantes. Esto no se cumple a rajatabla, sino no seria un juego
	 * muy divertido, sino que se va dando con el correr de las apuestas.
	 */
	public static int PORCENTAJE_JUGADOR = 70;

	/**
	 * El pozo puede tener un monto minimo al cual se precisa llegar antes de
	 * admitir la premiacion con el Jackpot
	 */
	public static int JACKPOT_MONTO_MINIMO = 2000;

	/**
	 * De cada apuesta que pueda participar del jackpot se retira un porcentaje
	 * para aportar al pozo
	 */
	public static float JACKPOT_PORCENTAJE_EXTRAIDO_APUESTA = 2f;

	/**
	 * Una vez que se llega al monto minimo es necesario por parte del jugador
	 * estar dentro del porcentaje necesario para acceder luego definitivamente
	 * al JACKPOT. El rango de valores puede ser el rango de 0 a 100. Para
	 * ejemplificar, 0 significaria que el usuario no saca nunca el pozo, y 100
	 * que lo saca siempre que se llegue al monto minimo del JACKPOT
	 */
	public static float JACKPOT_PROBABILIDADES_JUGADOR = 0.1f; // 0.1 en un
																// rango de
																// 1...100
																// equivale
																// a una
																// probabilidad
																// de 1 en 100

	/**
	 * Cuando se llega al monto indicado en este campo se realiza la apertura
	 * del pozo para empezar a participar por el tras cada tirada.
	 */
	public static float JACKPOT_APERTURA_POZO = 0f;

	/**
	 * De todas las apuestas se toma un porcentaje para el modulo 'menor o
	 * mayor'
	 */
	public static float MENOR_MAYOR_PORCENTAJE_EXTRAIDO_APUESTA = 1f;

	/**
	 * Cuantos resumenes de cobranza se obtienen desde la persistencia
	 */
	public static int RESUMENES_COBRANZA_LIMITE_OBTENCION = 100;
	
}