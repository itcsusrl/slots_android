package com.gmi.slotscore;

import org.microuy.javaslot.constante.Sistema;
import org.microuy.javaslot.dominio.Maquina;
import org.microuy.javaslot.persistencia.DBManager;
import org.microuy.javaslot.presentacion.Principal;
import org.microuy.javaslot.util.MaquinaPoolManager;
import org.newdawn.slick.SlickActivity;


public class SlotsActivity extends SlickActivity {

    public DBManager dbManager;

    @Override
    protected void onResume() {
        super.onResume();

        //init the database
        dbManager = new DBManager(getApplicationContext());

        //creamos una maquina con una configuracion
        Maquina maquina = MaquinaPoolManager.obtenerInstanciaMaquinaJuego(Sistema.configuracionMaquina);
        start(new Principal(maquina));
    }
}
